\label{sec:ClassificationAccuracy}

In diesem Abschnitt soll die Klassifikationsqualität, gemessen durch die 
Genauigkeit und weitere in \autoref{sec:Metriken} und \autoref{subsubsec:alpha} 
vorgestellten Metriken, besprochen werden. Die Qualität kann dabei in 
Abhängigkeit der verschiedenen Systemparameter, wie z.B. die Stapelgröße oder 
der genutzte Optimierungsalgorithmus, gesetzt werden.

\subsection{Erfassen der Ergebnisse}
\label{subsec:ErfassenErgebnisse}

Die Anzahl der Systemparameter alleine ist schon sehr groß. Eine 
vollständige Abarbeitung aller Kombinationen wird dadurch nahezu unmöglich. 
Jedoch ist es in diesem Fall nicht notwendig, alle möglichen Kombinationen 
einzeln aufzuführen, da mit keiner getesteten Kombination eine 
Klassifikationsgenauigkeit erreicht wurde, die höher ist als das Ergebnis eines 
ZeroR-Klassifikators, der in jedem Fall die neutrale Klasse des Korpus von Narr 
et~al. wählt (Baseline).

\smallskip
Deswegen sollen an dieser Stelle zunächst ein beispielhafter Trainingslauf und 
seine Parameter besprochen werden. Anschließend werden einige der 
Parameter-Va\-ri\-a\-ti\-on\-en dargestellt. Die folgenden Ausführungen 
beziehen sich auf den Trainingslauf \texttt{2017-""11-""07T""16:00:27.043856}. 
Dieser nutzt die Parameter, die in \texttt{main.py} als \QuoteM{default} 
angegeben sind: 

\begin{itemize}
	\setlength\itemsep{\Itemizespace}
	\item Kodierung auf Zeichenebene.
	\item Der Korpus von Narr et~al. wird für den Analysator genutzt.
	\item Der Korpus von Mozeti{\v{c}} et~al. wird für den Diskriminator 
	genutzt.
	\item 10~\% des Korpus wird zum Testen genutzt.
	\item Die Stapelgröße liegt bei 300 Tweets.
	\item Es werden maximal $1\,000$ Epochen trainiert, wobei in diesem Fall 
	schon früher abgebrochen wurde.
	\item Alle 10 Epochen wird das System getestet.
	\item Es wird nur das aktuellste Modell gespeichert.
	\item Die Lernrate liegt bei 0,001.
	\item Das \ac{DAN}-System wird als Einheit trainiert.
\end{itemize}

Weiterhin kommt das Programm unmodifiziert, so wie es sich im Anhang (siehe 
\autoref{sec:app:Inhaltsliste}) zu dieser Arbeit finden lässt, zum Einsatz. Das 
heißt, dass die klassische Fehlerfunktion ohne Feature Matching genutzt wird. 
Weiterhin wird der Dropout im Diskriminator deaktiviert.

\begin{figure}[htb]
	\centering
	{
		\graphicspath{
		    {Content/60_EvaluationValidation/01_ClassificationAccuracy/figures/}
		}
		\resizebox{0.9\textwidth}{!}{
			\input{Content/60_EvaluationValidation/01_ClassificationAccuracy/figures/TrainTestAccuracy}
		}
	}
	\caption[Entwicklung der Klassifikationsgenauigkeit über die Epochen]
	{Entwicklung der Klassifikationsgenauigkeit über die Trainingsepochen des 
	Trainingslaufes \texttt{2017-11-07T16:00:27.043856}}
	\label{fig:TrainTestAccuracy}
\end{figure}

\medskip
In \autoref{fig:TrainTestAccuracy} wird die Entwicklung der Genauigkeit über 
die einzelnen Epochen dargestellt. Dabei wird die Genauigkeit für jede 
Trainingsepoche durch die blaue Linie symbolisiert. Alle zehn Trainingsepochen 
wird das \ac{DAN}-System mit unbekannten Daten getestet, die Ergebnisse dieses 
Tests sind in rot dargestellt.

\bigskip
Trotz der in \autoref{sec:Metriken} beschriebenen Vorbehalte stellt 
\autoref{fig:TrainTestAccuracy} die Klassifikationsgenauigkeit dar. An dieser 
Stelle wird die Klassifikationsgenauigkeit gewählt, da sie die einzige Metrik 
ist, die kontinuierlich auch während des Trainings ermittelt wird, sodass sich 
Rückschlüsse auf mögliche Überanpassungen ziehen lassen. In diesem Fall werden 
die Ergebnisse auf Basis des selben Korpus berechnet, sodass die Werte dennoch 
vergleichbar sind. Zusätzlich werden im Folgenden weitere Metriken, die ein 
genaueres Bild zulassen, dargestellt.

\subsection{Auswertung der Ergebnisse}
\label{subsec:AuswertungErgebnisse}

In \autoref{fig:TrainTestAccuracy} lässt sich gut erkennen, dass nach wenigen 
(ca. 14) Trainingsepochen bereits die Baseline erreicht ist und sich die 
Klassifikationsqualität danach nicht grundlegend ändert. Hier bestätigt sich 
auch die in \autoref{sec:DAN} aufgestellte These, dass das \ac{DAN}-System 
Überanpassungen vermeidet, da die Genauigkeit, unabhängig davon ob die Daten 
bekannt sind oder nicht, im gleichen Bereich liegt.

\medskip
Obwohl die Klassifikationsgenauigkeit nicht an das Niveau anderer Verfahren, 
beispielsweise die Arbeit von Flender et~al., die auf dem Korpus von Narr 
et~al. maximal 84,5~\% erreicht hat 
\QuoteIndirectNoPage{DBLP:conf/lwa/FlenderG17}, heranreicht, erfüllt der 
Prototyp doch seine Aufgabe, da er die generelle Durchführbarkeit demonstriert. 
Der Anstieg der Klassifikationsgenauigkeit innerhalb der ersten 14 Epochen 
zeigt, dass das \ac{DAN}-System (bis zu einem gewissen Punkt) konvergiert und 
sich das \ac{GAN}-Konzept auf die Analyse von Tweets und möglicherweise auch 
auf andere Fragestellungen übertragen lässt. Um diesen Prototyp jedoch in 
Anwendungen aus der Realwirtschaft einzusetzen, muss das Verfahren weiter 
optimiert und die Verteilung der Ausgabewerte verbreitert werden.

\medskip
Wie die Wahrheitsmatrix (siehe \autoref{eq:confusionMatrix}) zeigt, wählt der 
Analysator in jedem Fall nur die neutrale Klasse, dadurch ergibt sich auch die 
Baseline-Genauigkeit von 75,53~\%. Dieses Problem ist bekannt: \QuoteDirect{One 
of the main failure modes for GAN is for the generator to collapse to a 
parameter setting where it always emits the same point.}
{DBLP:journals/corr/SalimansGZCRC16}{S. 3} (zu Deutsch etwa: \QuoteM{Einer der 
Haupt-Fehlermodi eines \acp{GAN} entsteht, wenn der Generator in eine 
Einstellung kollabiert, in der immer der gleiche Punkt ausgegeben wird.}) 

\begin{equation} \label{eq:confusionMatrix}
	\begin{pmatrix}
		0 & 0 & 0 \\
		14 & 71 & 9 \\
		0 & 0 & 0 \\
	\end{pmatrix}
\end{equation}

\medskip
Einige der Verfahren zur Verbreiterung der Verteilung der Ausgabewerte aus der 
Arbeit von Salimans et~al. 
\QuoteIndirectNoPage{DBLP:journals/corr/SalimansGZCRC16} werden in dieser 
Arbeit eingesetzt. Jedoch handelt es sich bei diesen Verfahren um Heuristiken, 
die keine Verbreiterung garantieren. Erschwerend kommt hinzu, dass die neutrale 
Klasse mit einem Anteil von 75,53~\% deutlich überwiegt. Jedoch erbrachten 
Versuche, die Menge der neutralen Fälle zu minimieren, keinen nennenswerten 
Erfolg.

\begin{table}[htb]
	\centering
	\begin{tabular}{|c|c|c|}
		\hline
		 	& \texttt{Macro} & \texttt{Weighted} \\ 
		\hline \hline 
			F-Maß & 0,287 & 0,650 \\ 
		\hline
			Präzision &  0,252 & 0,571 \\ 
		\hline 
			Trefferquote & 0,333 & 0,755 \\ 
		\hline
		\multicolumn{3}{c}{} \\
		\hline 
		 	& Intervall % Geordnet
		 	& Nominal \\ % Ungeordnet
		\hline \hline
			Krippendorff's $\alpha$ & 0,000 & -0,097 \\ 
		\hline
	\end{tabular}
	\caption{Weitere Metriken eines Trainingslaufes des \ac{DAN}-Systems}
	\label{tab:MetricsResults}
\end{table}

\bigskip
Die Metriken aus \autoref{tab:MetricsResults} sowie die Wahrheitsmatrix (siehe 
\autoref{eq:confusionMatrix}) beziehen sich auf die Ergebnisse der Testläufe 
der Epochen größer 15 und kleiner 189 des Trainingslaufes 
\texttt{2017-11-07T16:00:""27.""043856}. Wie in \autoref{sec:TestsMetrics} 
schon beschrieben, werden die Metriken, die alle Klassen miteinbeziehen, sowohl 
gewichtet (\texttt{weighted}) als auch ungewichtet (\texttt{macro}) 
dargestellt. Da keine Fälle als \QuoteM{Positiv} oder \QuoteM{Negativ} 
klassifiziert sind, lassen sich die einzelnen Metriken nur für den neutralen 
Fall berechnen. Somit werden die für jede Klasse einzeln berechneten Metriken 
nicht aufgeführt. Weiterhin fällt auf, dass der nominale Werte von 
Krippendorff's alpha negativ ist. Wie auch bei Fleiss' kappa (siehe 
\autoref{subsubsec:kappa}) kann dies als Anzeichen einer umgekehrten 
Übereinstimmung betrachtet werden.

\index{Interrater-Reliabilität>Krippendorff's alpha}
\index{F-Maß}
\index{Präzision}
\index{Trefferquote}

\smallskip
Da alle Fälle einer Klasse zugeordnet sind, entspricht die gewichtete 
Trefferquote (Verhältnis aus richtig klassifizierten Fällen und allen Fällen, 
siehe \autoref{sec:Metriken}) der Genauigkeit: $\frac{71}{71+14+9}$. Ansonsten 
veranschaulichen die geringen Werte der anderen Metriken, auch von 
Krippendorff's alpha, den Umstand, dass zwei Klassen in der Auswahl des 
Klassifikators vollkommen übergangen werden. Auch verglichen mit der Arbeit von 
Flender et~al. \QuoteIndirectNoPage{DBLP:conf/lwa/FlenderG17} spiegeln die 
Ergebnisse der Metriken die zu starke Gewichtung der neutralen Klasse wieder.

\subsection{Variationen der Parameter}
\label{subsec:VariationenParameter}

Werden die Korpora getauscht, sodass der Korpus von Mozeti{\v{c}} et~al. für 
den Analysator und der von Narr et~al. für den Diskriminator genutzt wird, so 
ändern sich die Ergebnisse nur unwesentlich. Damit die vorhandenen Ressourcen 
für solch einen Trainingslauf ausreichen, muss zum einen die Stapelgröße und 
zum anderen das Verhältnis zwischen Trainings- und Test-Daten auf wesentlich 
geringere Werte, z.B. 30 und 0,001 eingestellt werden, wobei die konkreten 
Zahlen von der genutzten Hardware abhängen. Ist dies geschehen, so zeigt sich 
ein ähnliches Bild wie weiter oben beschrieben: Nach einigen Epochen beginnt 
der Analysator, alle Fälle nur einer Klasse zuzuordnen. 

\medskip
Da die Gewichte der \acp{ANN} des \ac{DAN}-Systems mit Zufallswerten 
initialisiert werden, kann es passieren, dass das Netzwerk nicht konvergiert. 
In diesem Fall wird das Training vorzeitig abgebrochen, da das System i.d.R. 
nur positive oder negative Fälle auswählt. Anschließend wird ein neuer 
Trainingslauf gestartet. Auch bei klassischen \acp{GAN} kann dieser Fall 
vorkommen und wird genauso behandelt \QuoteIndirectNoPage{GAN:Train}.

\medskip 
Weitere Änderungen des Programms oder seiner Parameter führen zu keiner 
messbaren Verbesserung der Ergebnisse. Weder durch den Einsatz von Feature 
Match\-ing noch durch Änderungen an der Netztopologie können die Ergebnisse 
verbessert werden. Da die alternative Fehlerfunktion wie die bestehende 
arbeitet, nur dass sie mittels TensorFlow-Funktionen implementiert ist, kann 
mit ihr auch keine Verbesserung erreicht werden. Weitere erfolglos getestete 
Parameter sind der Einsatz der Kodierung auf Wortebene, andere 
Aktivierungsfunktionen, ein stärkerer Dropout und andere \acp{RNN}-Zellen 
inklusive Tauschen der \ac{LSTM}-Varianten (mit Guckloch oder ohne). Die Größe 
der einzelnen \acp{ANN} ist durch die genutzte Hardware beschränkt und kann so 
nur geringfügig geändert werden. Auch andere Werte für die Lernrate oder andere 
Optimierungsfunktionen führen zu keiner Verbesserung.
