# To make a .tex file: texlive-eepic and texlive-bxeepic needs to be installed
set terminal epslatex color size 16cm, 9cm

# The name an location of the .tex file
set output "DANvsSingle.tex"

# The lable for the x axsis
set xlabel "Epoche"

# The lable and rotation for the y axsis
set ylabel "Genauigkeit"

# Set the marking for x and y axsis to latex math mode
set format xy "$%g$"

# The position of the legend
set key right bottom

set grid ytics

set datafile separator ","

# First line as x value the other as y value
# with lines, line stlye 1 (full line), line color different
# Smooth wit a bezier
# As Titel will be the first line used

# Use for smother plott: smooth sbezier

fhOrange = "#ef8400"
fhBlue = "#1c95c2"
fhRed = "#b4007b"
fhGreen = "#a4b307"

set style line 1 lc rgb fhBlue  lt 1 lw 2 pt 7 ps 0.5   
set style line 2 lc rgb fhRed   lt 1 lw 2 pt 5 ps 0.5 
set style line 3 lc rgb fhGreen lt 1 lw 2

set arrow from graph 0,first 0.75531914893617021276 to graph 1,first 0.75531914893617021276 nohead ls 3 

plot "run_2017-11-11T10_36_39.793399_summaries_anal_test,tag_Analyzer_Metrics_anal_accuracy_1.csv"  using 2:3 with linespoints ls 2 title "Test"  ,\
     "run_2017-11-11T10_36_39.793399_summaries_anal_train,tag_Analyzer_Metrics_anal_accuracy_1.csv" using 2:3 with linespoints ls 1 title "Training" ,\
     1/0 t "Baseline" ls 3
