# To make a .tex file: texlive-eepic and texlive-bxeepic needs to be installed
set terminal epslatex color size 16cm, 9cm

# The name an location of the .tex file
set output "GoogleTrendsSA.tex"

# The lable for the x axsis
set xlabel "Zeit"

# The lable and rotation for the y axsis
set ylabel "Relatives Interesse [\\%]"

# Set the marking for x and y axsis to latex math mode
set format xy "$%g$"

# The position of the legend
set key right bottom

set datafile separator ","

set xdata time
set timefmt "%Y-%m"
set format x "%Y"
set xrange ["2003-10-01 00:00":"2018-01-01 00:00"]

set grid ytics

# First line as x value the other as y value
# with lines, line stlye 1 (full line), line color different
# Smooth wit a bezier
# As Titel will be the first line used

# Use for smother plott: smooth sbezier

fhOrange = "#ef8400"
fhBlue = "#1c95c2"
fhRed = "#b4007b"
fhGreen = "#a4b307"

set style line 1 lc rgb fhBlue lt 1 lw 2 pt 7 ps 0.5   # --- blue

plot "multiTimeline2.csv"  using 1:2 with linespoints ls 1 title ""
