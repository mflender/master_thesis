\label{sec:Bedeutung}

Stimmungen und Emotionen gegenüber Objekten spielen eine zentrale Rolle für den 
Menschen. Vor jeder größeren Entscheidung, egal ob es sich dabei um die Buchung 
des nächsten Urlaubes, den Kauf einer Aktie oder die Entscheidung für eine 
Partei in der kommenden Wahl handelt, holen sich die meisten Menschen den Rat 
von Freunden und Bekannten ein. Dank des Internets ist die Suche nach solch 
einer Empfehlung nicht mehr auf das direkte Umfeld des Entscheiders begrenzt. 
Zu beinah jedem Thema lassen sich viele Meinungen und Stimmungen im Internet 
finden.

\medskip
Unternehmen und Organisationen sind sich dieses Umstandes bewusst und suchen 
nach Möglichkeiten, das Internet und weitere Quellen nach solchen Stimmungen 
und Emotionen zu durchsuchen und diese anschließend zu analysieren. Durch 
dieses Vorgehen lassen sich wichtige Daten sammeln, die als Basis für die 
wirtschaftliche, aber auch persönliche Entscheidungsfindung dienen können.

\medskip
Derartige Maßnahmen sind erst durch das Web 2.0 mit seinem
\QuoteM{User-generated content} und der damit stark anwachsenden Menge an 
leicht verfügbaren Daten im Internet, aber auch durch leistungsfähige moderne 
Computersysteme möglich und auch notwendig geworden, da die enormen Mengen an 
Informationen manuell nicht mehr verarbeitbar sind 
\QuoteIndirectNoPage{SocialMediaResearchSummary}.
\index{Web 2.0}

\bigskip
Die oben beschriebene Notwendigkeit soll nun anhand einiger Beispiele genauer 
dargestellt werden.

\subsection{Anwendungsfall: Politik}
\label{subsec:Politik}

\begin{itemize}
	\setlength\itemsep{\Itemizespace}
	\item \textit{Politische Umfragen} liefern nur eine Momentaufnahme der 
	Situation, die nur schlecht Rückschlüsse auf Übergangspunkte in der 
	Stimmung und deren Ursachen zulässt. Weiterhin ist es schwierig, mit einer 
	politischen Umfrage ein repräsentatives Ergebnis zu erzielen. Durch die 
	Analyse von User-generated content lässt sich ein kontinuierliches, 
	repräsentatives Bild der politischen Situation erzeugen
	\QuoteIndirectNoPage{Akcora:2010:IBP:1964858.1964867}.
	\index{Politik>Umfrage}

	\item Ähnlich wie das oben beschriebene System kann man \ac{SA} von 
	User-generated content zur \textit{Wahlprognose} benutzen. In der Arbeit 
	von Tumasjan el~al. wurde ein solches System genutzt, um die Ergebnisse der 
	Bundestagswahl vom 27.~September 2009 vorherzusagen. Die erreichte 
	Genauigkeit lag dabei im selben Bereich wie konkurrierende traditionelle 
	Methoden \QuoteIndirectNoPage{tumasjan2010predicting}. Ein ähnliches System 
	wurde von Wang el~al. für die US-amerikanische Präsidentenwahl im Jahr 
	2012 entwickelt. Bei diesem System lag der Fokus auf einer direkten 
	(real-time) Verfügbarkeit der Analyseergebnisse
	\QuoteIndirectNoPage{Wang12asystem}.
	\index{Prognose>Wahl}

	\item Weiterhin lassen sich auch allgemeinere \textit{politische 
	Diskussionen} im Internet analysieren
	\QuoteIndirectNoPage{mullen2006preliminary}.
	\index{Politik>Diskussion}
\end{itemize}

\subsection{Anwendungsfall: Wirtschaft}
\label{subsec:Wirtschaft}

\begin{itemize}
	\setlength\itemsep{\Itemizespace}
	\item Wie Kramer et~al. gezeigt haben, können sich Stimmungen und Emotionen 
	auch über soziale Medien verbreiten
	\QuoteIndirectNoPage{kramer2014experimental}. Deswegen ist \ac{SA} 
	insbesondere im Hinblick auf Börsengeschäfte interessant und lukrativ, da 
	sowohl die Märkte als auch die sozialen Medien im Internet auf nationale 
	und globale Ereignisse reagieren 
	\QuoteIndirectNoPage{DBLP:journals/corr/abs-0911-1583}. Durch den Einsatz 
	solcher \ac{SA}-Verfahren lassen sich \textit{Prognosen für die Wirtschaft} 
	generieren \QuoteIndirectNoPage{bollen2011twitter, conf/epia/OliveiraCA13}.
	\index{Prognose>Markt}

	\item Innerhalb von vielen sozialen Netzwerken werden Unternehmen und ihre 
	Produkte besprochen. Dieses \textit{Mundpropaganda-Marketing} (oder auch 
	Empfehlungsmarketing) zu analysieren, um so zum einen eine ungefilterte 
	Rückmeldung der Endkunden zu bekommen und zum anderen \ac{PR}-Maßnahmen 
	besser steuern zu können, stellt ein weiteres wichtiges Anwendungsgebiet 
	der \ac{SA} dar \QuoteIndirectNoPage{DBLP:journals/corr/abs-1101-0510, 
	Jansen:2009:MOW:1520340.1520584, riegner2007word}.
	\index{Mundpropaganda-Marketing}
	
	\item Genauso wichtig wie die indirekte Kundenkommunikation über das 
	Internet ist die \textit{direkte Kommunikation} z.B. über Ticket-Systeme, 
	ein Helpdesk, ein Callcenter \QuoteIndirectNoPage{Huber2000RecognitionOE} 
	oder \ac{CRM}-Systeme. Hier kann beispielsweise die Dringlichkeit einer 
	Anfrage unter anderem durch eine \ac{SA} eingestuft werden.
	\index{Direkte Kundenkommunikation}
	
	\item Ein zumeist unerwünschter Nebeneffekt des User-generated content sind 
	oftmals grundlos böswillige oder aggressive Nachrichten (auf Englisch 
	\QuoteM{flame}). Die \ac{SA} kann helfen, solche Nachrichten automatisiert 
	zu erkennen und zu löschen
	\QuoteIndirectNoPage{Spertus:1997:SAR:1867406.1867616}.
	\index{Flame Recognition}
\end{itemize}
