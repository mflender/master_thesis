\label{sec:Korpora}

In diesem Abschnitt wird die Auswahl der konkreten Korpora vorgestellt.
Da statistische \ac{SA}-Verfahren ihre Modelle der Realität auf Basis von 
Korpora entwickeln, nimmt der genutzte Korpus eine wichtige Rolle ein. Um eine 
möglichst hohe Klassifikationsqualität zu erzielen, sind bei der Auswahl und 
Beurteilung eines Korpus einige Punkte zu beachten. Zunächst sollen einige 
dieser Aspekte, darunter auch verschiedene Metriken zur 
Interrater-Reliabilität, angesprochen werden. Anschließend werden die in dieser 
Arbeit genutzten Korpora ausgewählt und genauer besprochen.

\subsection{Subjektivität der Empfindung}
\label{subsec:Subjektivität}

Die Beschreibung der \ac{SA} in \autoref{sec:Interpretation} 
\QuoteM{\nameref{sec:Interpretation}} auf \autopageref{sec:Interpretation} 
unterschlägt einen wichtigen Aspekt: Die Subjektivität einer Empfindung. Viele 
Sätze können je nach Standpunkt des Lesers sehr unterschiedliche Empfindungen 
auslösen. Beispielsweise wird die Nachricht über den Sieg einer Sportmannschaft 
oder einer politischen Partei bei einer Wahl in Anhängern eine andere Stimmung 
auslösen als in Gegnern. Dazu ein Beispiel aus dem Korpus von Narr et~al. 
\QuoteIndirectNoPage{dailabor2012twittersentiment}:
\index{Sentiment Analysis>Subjektivität}

\begin{quote}
	\QuoteM{yoga und blauer himmel!}
\end{quote}

Dieser Satz kann sowohl als \QuoteM{Neutral} als auch als \QuoteM{Positiv} 
klassifiziert werden, da er zum einen als wertfreie und zum anderen als 
implizit positive Aussage betrachtet werden kann.

\smallskip
Eine subjektive Empfindung ist stark von dem kulturellen sowie 
sozio-politischen Umgebungssystem und der Zeit, in der sie bewertet wird, 
abhängig. Da das Verfahren nur das lernen kann, was es im Korpus vorfindet, 
kommt durch diesen Umstand dem Korpus eine bedeutende Rolle zu 
\QuoteIndirectNoPage{DBLP:journals/corr/MozeticGS16}. Aus diesem Grund sollte 
ein Korpus von mehreren Menschen annotiert werden (Übereinkunft), dadurch kann 
eine subjektive zu einer intersubjektiven Empfindung verallgemeinert werden 
\QuoteIndirect{DBLP:journals/corr/MozeticGS16}{S. 14}. Weiterhin sollten 15~\% 
bis 20~\% der Tweets von der gleichen Person doppelt annotiert werden 
(Selbst-Übereinkunft), um Abweichungen, wie Tagesform oder ungenaues Lesen, 
erkennen und entsprechend behandeln zu können 
\QuoteIndirect{DBLP:journals/corr/MozeticGS16}{S. 14}.
\index{Interrater-Reliabilität>Übereinkunft}
\index{Interrater-Reliabilität>Selbst-Übereinkunft}

\medskip
Aus diesen Informationen lassen sich einige Metriken über die Übereinstimmung 
(Interrater-Reliabilität) der Menschen, die den Korpus annotiert haben 
(Beobachter oder Rater), berechnen. Der folgende Unterabschnitt geht genauer 
auf diese Metriken ein. Wie Mozeti{\v{c}} et~al. zeigen konnten, kann diese 
Übereinkunft als obere Grenze für die erreichbare Klassifikationsqualität 
dienen \QuoteIndirect{DBLP:journals/corr/MozeticGS16}{S. 2, 6, 13}.
\index{Interrater-Reliabilität}
\index{Interrater-Reliabilität>Beobachter}

\medskip
So konnte beispielsweise mit dem von Flender und Gips vorgestellten Verfahren 
\QuoteIndirectNoPage{DBLP:conf/lwa/FlenderG17} auf dem Korpus von Mozeti{\v{c}} 
et~al. nur eine Klassifikationsgenauigkeit erreicht werden, die den Ergebnissen 
von Mozeti{\v{c}} et~al. entspricht (ca. 61~\% bis 64~\% Genauigkeit in beiden 
Fällen), obwohl dieses Verfahren auf dem Korpus von Narr et~al. wesentlich 
bessere Ergebnisse erzielt hat. Dies ist in der Qualität der Korpora begründet, 
da der Korpus von Narr et~al. nur aus Tweets besteht, die von drei 
unterschiedlichen Menschen mit der gleichen Klasse annotiert wurden. Somit kann 
gezeigt werden, dass die Klassifikationsgenauigkeit des Verfahrens massiv von 
der Qualität des Korpus abhängt 
\QuoteIndirect{DBLP:journals/corr/MozeticGS16}{S. 13}.

\bigskip
Erschwerend kommt hinzu, dass in einem Tweet mehrere widersprüchliche 
Empfindungen enthalten sein können. Auch dazu zwei Beispiele aus dem Korpus von 
Narr et~al. \QuoteIndirectNoPage{dailabor2012twittersentiment}:

\index{Korpus}

\begin{quote}
	\QuoteM{Schade; dass du gehst. Es war so schön; bevor du kamst.}
	
	\smallskip
	\QuoteM{\AdjustWords{Der Nutzer} Ist mal wieder auf dem Weg nach Mannheim. 
	Schön. Und wieder ein mal \SIC ist die Klimaanlage ausgefallen. Nicht 
	schön.}
\end{quote}

Solch einen Satz einer Klasse zuzuordnen, ist nicht ohne Weiteres machbar. Eine 
mögliche Lösung für dieses Problem besteht darin, jeden Teilsatz für sich mit 
einer Klasse zu annotieren. Eine andere Möglichkeit, die von Narr et~al. 
genutzt wird, ist die Zuordnung solcher Tweets zu einer vierten Klasse: 
\QuoteM{na} bzw. \QuoteM{Irrelevant}. Jedoch ist dieser Fall mit 0,93~\% im 
Korpus von Narr et~al. \QuoteIndirectNoPage{dailabor2012twittersentiment} 
vernachlässigbar selten.

\subsection{Genutzte Metriken}
\label{subsec:GenutzteMetriken}

Wie in \autoref{sec:Metriken} \QuoteM{\nameref{sec:Metriken}} auf 
\autopageref{sec:Metriken} beschrieben, besitzt die Genauigkeit einige 
Nachteile und soll in dieser Arbeit um weitere Maße ergänzt werden. Zusätzlich 
dazu sollen Metriken eingeführt werden, die im Folgenden genutzt werden, aber 
nicht in \autoref{sec:Metriken} besprochen wurden. 

\medskip
Die meisten der hier vorgestellten Maße dienen der Ermittlung der 
Interrater-Re\-li\-a\-bi\-li\-tät (auf Englisch \QuoteM{Inter-rater 
reliability} oder \QuoteM{Concordance}). Es kann zwischen Metriken, die sich 
nur auf die Ergebnisse von zwei Beobachtern beziehen, und Metriken, die 
beliebig viele Beobachter zulassen, unterschieden werden 
\QuoteIndirectNoPage{IntercoderReliability}.

\medskip
In erste Kategorie fallen Scott's pi ($\pi$) und Cohen's kappa ($\kappa$), die 
hier nicht weiter besprochen werden, da die in dieser Arbeit genutzten Korpora 
von mehr als zwei Beobachtern annotiert wurden. Fleiss' kappa (K) und 
Krippendorf's alpha ($\alpha$) sind Bestandteil der zweiten Kategorie und 
sollen im Folgenden genauer beschrieben werden 
\QuoteIndirectNoPage{IntercoderReliability}.

\index{Interrater-Reliabilität>Inter-rater reliability}

\subsubsection{Krippendorff's Alpha}
\label{subsubsec:alpha}

Stimmen alle Beobachter bei jedem Tweet überein, so ergibt sich ein Wert von 
$\alpha = 1$. Liegt die Übereinstimmung im Bereich des Zufalls, so nimmt 
$\alpha$ einen Wert von 0 an \QuoteIndirectNoPage{krippendorff04}. Dabei lässt 
sich Krippendorff's alpha an unterschiedliche Arten von Klassen anpassen. Es 
lassen sich Werte für nominale Klassen, die keine natürliche Ordnung besitzen 
(z.B. Emotionen: Wut, Ekel, Angst, $\dots$), und für Intervallklassen, die sich 
ordnen lassen (z.B. Kleidergrößen: S, M, L, $\dots$), berechnen. Dadurch ist 
dieses Maß sehr flexibel einsetzbar. Die Berechnung beider Werte bedeutet wenig 
zusätzlichen Aufwand gegenüber der Berechnung von nur einem Wert. Somit werden 
auch beide berechnet, obwohl wie in \autoref{sec:Metriken} gezeigt, die 
genutzten Klassen als geordnet betrachtet werden können 
\QuoteIndirect{DBLP:journals/corr/MozeticGS16}{S. 3}.

\index{Klassen>Nominal}
\index{Klassen>Intervall}

\medskip
Laut Mozeti{\v{c}} et~al. hat sich folgende Daumenregel für die noch 
akzeptabele Qualität eines Korpus bewährt
\QuoteIndirect{DBLP:journals/corr/MozeticGS16}{S. 13}:

\begin{description}
	\setlength\itemsep{\Itemizespace}
	\item[Übereinkunft:] $\alpha > 0,6$ 
	\item[Selbst-Übereinkunft:] $\alpha > 0,4$ 
\end{description}

Von den hier beschriebenen Metriken ist Krippendorff's alpha am schwersten zu 
berechnen, aber auch die zuverlässigste Metrik. Sie hat einige Vorteile, so 
können beliebig viele Beobachter, unterschiedliche Arten von Klassen 
(Nominalskala, Intervallskala, u.s.w.) und sogar unvollständige Daten 
abgebildet werden \QuoteIndirectNoPage{IntercoderReliability}.

Eine genaue Beschreibung der Berechnung lässt sich unter anderem in der Arbeit 
von Mozeti{\v{c}} et~al. finden, sodass an dieser Stelle darauf verzichtet 
werden kann \QuoteIndirect{DBLP:journals/corr/MozeticGS16}{S. 17 f.}.

\index{Interrater-Reliabilität>Krippendorff's alpha}

\subsubsection{Fleiss' kappa}
\label{subsubsec:kappa}

Fleiss' kappa stellt eine Erweiterung von Scott's pi ($\pi$) auf beliebig viele 
Beobachter dar. In der Interpretation der Werte sind sich Fleiss' kappa und 
Krippendorff's alpha sehr ähnlich, auch hier entspricht $K=1$ einer 
vollkommenen Übereinstimmung und $K=0$ einer zufälligen Übereinstimmung. Wobei 
auch negative Werte, die bei einer umgekehrten Übereinstimmung auftreten, 
möglich sind \QuoteIndirectNoPage{IntercoderReliability}.

\medskip
Gegenüber Krippendorff's alpha hat Fleiss' kappa den Nachteil, dass es nur auf 
nominale Klassen und vollständige Daten anwendbar ist. Aus diesem Grund wird es 
in dieser Arbeit nicht genutzt. Die Beschreibung an dieser Stelle dient 
lediglich dem besseren Verständnis von \autoref{subsubsec:Narr}.

\smallskip
Folgende Anleitung hilft, die Werte von Fleiss' kappa besser zu interpretieren
\QuoteIndirectNoPage{Fleiss81themeasurement}:

\begin{description}
	\setlength\itemsep{\Itemizespace}
	\item[Schlechte Übereinstimmung:] $K < 0,40$
	\item[Durchschnittliche bis gute Übereinstimmung:] $0,60 < K < 0,74$
	\item[Sehr gute Übereinstimmung:] $K\geq 0,75$
\end{description}

Da die Formel von Fleiss' kappa eine Übereinstimmung von 100~\% nicht zulässt 
(Division durch 0), eignet sich diese Metrik insbesondere für große Datensätze, 
bei denen solch eine Übereinstimmung unwahrscheinlich ist
\QuoteIndirectNoPage{IntercoderReliability}. Auch hier soll für eine genaue 
Beschreibung der Berechnung von Fleiss' kappa auf die Arbeit von Joyce 
verwiesen werden \QuoteIndirectNoPage{IntercoderReliability}.

\index{Interrater-Reliabilität>Fleiss' kappa}

\subsubsection{Genauigkeit innerhalb eins}
\label{subsubsec:ACC:PM1}

Die Genauigkeit innerhalb eins (auf Englisch \QuoteM{Accuracy within 1} oder 
\QuoteM{Acc$\pm$1}) berücksichtigt nur Fehler zwischen den Extremwerten der als 
geordnet angenommenen Klassen \QuoteM{Positiv} und \QuoteM{Negativ}. Somit 
handelt es sich um einen Spezialfall der Genauigkeit innerhalb $n$ 
\QuoteIndirectNoPage{Gaudette:2009:EMO:1560466.1560494}. Die Genauigkeit 
innerhalb eins lässt sich maximieren, indem für jeden Fall die Klasse 
\QuoteM{Neutral} angenommen wird. Sie berechnet sich mittels \autoref{eq:acc}, 
wobei $N(+,-)$ und $N(-,+)$ die Anzahl der falschen Klassifikationen und $N$ 
die gesamte Anzahl der Klassifikationen symbolisiert 
\QuoteIndirect{DBLP:journals/corr/MozeticGS16}{S. 18}. 

\begin{equation} \label{eq:acc}
Acc \pm 1 = 1- \frac{N(+,-)+N(-,+)}{N}
\end{equation}

Auch diese Metrik wird im weiteren Verlauf dieser Arbeit nur zur Beschreibung 
eines der genutzten Korpora gebraucht.

\index{Interrater-Reliabilität>Genauigkeit innerhalb eins}

\subsection{Auswahl der Korpora}
\label{subsec:Auswahl}

Wie schon in \autoref{subsubsec:Quellen} und \autoref{subsec:Tweets} 
angesprochen, ist Twitter eine beliebte und häufig genutzte Quelle für 
\ac{SA}-Korpora. Mithin wurden auch einige Korpora zu dieser Thematik 
veröffentlicht.

\medskip
So gibt es beispielsweise den Sentiment 140 Korpus von Go 
et~al., der sich mit englischsprachigen Tweets in zwei Klassen auseinandersetzt
\QuoteIndirectNoPage{Sentiment140}. Laut Dokumentation sind drei Klassen 
vorhanden, wobei der neutrale Fall nie im Trainings-Korpus erscheint. Insgesamt 
enthält dieser Korpus $1\,600\,498$ Einträge. 

\medskip
Ein weiterer umfangreicher Korpus wurde von Mozeti{\v{c}} et~al. 
zusammengestellt, er enthält $1\,600\,000$  Tweet-IDs in 13 Datensätzen zu 
jeweils unterschiedlichen Sprachen 
\QuoteIndirectNoPage{DBLP:journals/corr/MozeticGS16}. Um nicht in Konflikt mit 
den Geschäftsbedingungen von Twitter zu geraten, werden häufig nicht die Tweets 
selbst, sondern nur die Tweet-IDs im Korpus hinterlegt. Durch die 
Twitter-\ac{API} lassen sich die Tweets und ihre Metadaten, falls sie nicht 
gelöscht wurden, abrufen. 

\medskip
Der Korpus von Narr et~al. ist auch mehrsprachig ausgelegt, dabei wurden in 
vier Sprachen $12\,597$ Tweets analysiert. Der deutschsprachige Teil umfasst 
dabei 949 Tweets \QuoteIndirectNoPage{dailabor2012twittersentiment}. 

\medskip
Zu dem in \autoref{subsubsec:StatistischeVerfahren} vorgestellten Verfahren von 
Haldenwang und Vornberger wurde ein englischsprachiger Korpus veröffentlicht, 
der $14\,506$ Tweet-IDs und ihre Klassen enthält
\QuoteIndirectNoPage{haldenwang2015uncertainty}.
\index{Twitter>Korpus}
\index{Twitter>Tweet-ID}

\medskip
Da es, wie in \autoref{sec:Schwächen} dargestellt, nur wenige Ausarbeitungen 
zur \ac{SA} von deutschsprachigen Texten gibt, konnten auch entsprechend wenig 
Korpora auf diesem Gebiet gefunden werden. Hinzukommen die in 
\autoref{subsec:Subjektivität} beschriebenen hohen Anforderungen an ein Korpus, 
der sich mit deutschsprachigen Twitter-Nachrichten auseinandersetzt. Dies alles 
führt dazu, dass nur wenige mögliche Korpora für diese Arbeit zur Verfügung 
stehen.

\medskip
Von den vier oben angesprochenen Twitter-Korpora enthalten nur zwei 
deutschsprachige Tweets bzw. Tweet-IDs. Aus diesem Grund sollen in dieser 
Arbeit die Korpora von Mozeti{\v{c}} et~al. 
\QuoteIndirectNoPage{DBLP:journals/corr/MozeticGS16} und Narr et~al.
\QuoteIndirectNoPage{dailabor2012twittersentiment} genutzt werden. Der Korpus 
von Narr et~al. ist mit 949 Einträgen sehr klein, besitzt aber eine gute 
Qualität, da jeder dieser Einträge dreifach gleich annotiert wurde. Bei dem 
Korpus von Mozeti{\v{c}} et~al. sind mit $109\,130$ (bzw. $91\,171$) wesentlich 
mehr Tweets vorhanden, die jedoch eine schlechtere Qualität aufweisen. Auf 
beide Korpora soll im Folgenden genauer eingegangen werden.

\subsubsection{Der Korpus von Narr et~al.}
\label{subsubsec:Narr}

Der von Narr, Hülfenhaus und Albayrak veröffentlichte Korpus enthält Tweets 
verfasst in vier Sprachen: Englisch, Deutsch, Französisch und Portugiesisch, 
wobei im Folgenden nur der deutschsprachige Teil betrachtet wird. Dabei ist zu 
beachten, dass in der ursprünglichen Arbeit dieser Korpus nur zum Testen des 
Klassifikators genutzt wurde. Die eigentlichen Trainingsdaten stammen aus einem 
anderen, hier nicht genutzten Korpus und wurden lediglich über Emoticons als 
Noisy Labels klassifiziert \QuoteIndirectNoPage{dailabor2012twittersentiment}.

\smallskip
Um eine hohe Klassifikationsqualität zu erreichen, eignen sich Korpora, die 
über Noisy Labels klassifiziert sind, nur bedingt, da wie in 
\autoref{subsec:Subjektivität} gezeigt, die Qualität des Korpus direkt Einfluss 
auf die Klassifikationsqualität hat und solche Noisy Labels, verglichen mit von 
Menschen annotierten Korpora, zu ungenau sind. Unter anderem deswegen wird hier 
nur der Test-Korpus der ursprünglichen Arbeit genutzt.

\medskip
Die $1\,800$ Tweets in diesem Korpus wurden aus dem Trainings-Korpus 
ausgewählt. Die erste Hälfte wurde zufällig gewählt und die andere musste einen 
der folgenden Markennamen enthalten: \QuoteM{Adidas}, \QuoteM{Audi}, 
\QuoteM{Bing}, \QuoteM{Gucci}, \QuoteM{Hilton}, \QuoteM{Microsoft}, 
\QuoteM{Nike}, \QuoteM{Nintendo}, \QuoteM{Samsung} oder \QuoteM{Sony}. Diese 
Tweets wurden in die vier Klassen \QuoteM{Positiv}, \QuoteM{Negativ}, 
\QuoteM{Neutral} und \QuoteM{Irrelevant} aufgeteilt. Dabei wurde jeder Eintrag 
von drei Amazon-Mechanical-Turk-Mitarbeitern annotiert. Somit gibt es Fälle, 
bei denen ein Tweet unterschiedlichen Klassen zugeteilt wurde. Deswegen wurden 
drei unterschiedliche Datensätze gebildet. Der erste enthält alle $1\,800$, 
dabei wird jeder Tweet dreimal aufgeführt. Der nächste Datensatz enthält nur 
Tweets, bei denen zwei oder drei Menschen die gleiche Klasse gewählt haben. Der 
dritte Datensatz beinhaltet nur die Tweets, bei denen sich alle drei Beobachter 
einig waren, dieser Datensatz wird zum Training des \ac{DAN}-Systems genutzt. 
Die Tweets in diesem Korpus wurden bereits vorverarbeitet, so wurden Links 
durch die \QuoteM{\textasciitilde http}-Markierung und Namen von 
Twitter-Nutzern durch \QuoteM{@user} ersetzt 
\QuoteIndirectNoPage{dailabor2012twittersentiment}.

\begin{table}[htb]
	\begin{tabularx}{\textwidth}
		{|>{\Centering}X|
		>{\Centering}X|
		>{\Centering}X|
		>{\RaggedLeft}X
		>{\RaggedRight}X|
		>{\RaggedLeft}X
		>{\RaggedRight}X|}
		
		\hline
		Insgesamt annotiert &
		Fleiss' Kappa &
		Klas\-sen &
		\multicolumn{2}
		{>
		{\hsize=\dimexpr2\hsize+2\tabcolsep+\arrayrulewidth\relax\Centering}X|}
		{Übereinstimmung $\geq 2$} &
		\multicolumn{2}
		{>
		{\hsize=\dimexpr2\hsize+2\tabcolsep+\arrayrulewidth\relax\Centering}X|}
		{Übereinstimmung = 3} \\
		\hline 
		
		\multirow{4}{*}{$1\,800$} & \multirow{4}{*}{0,419} & 
		    pos  & 353 & (20,91~\%)  & 143 & (15,07~\%) \\
		& & neut & $1\,096$ & (64,93~\%) & 711 & (74,92~\%) \\
		& & neg  & 239 & (14,16~\%)  & 95  & (10,01~\%) \\
		& & total & \multicolumn{2}{>{\RaggedLeft}X|}{$1\,688$} &
		            \multicolumn{2}{>{\RaggedLeft}X|}{949} \\
		\hline
		
	\end{tabularx}
	\caption[Daten und Statistiken zum Korpus von Narr et~al.]
	{Daten und Statistiken zum Korpus von Narr et~al., 
	Quelle: \cite[S. 4, Tab. 1]{dailabor2012twittersentiment}}
	\label{tab:Narr}
\end{table}

%\medskip
Unter anderem werden die konkreten Größen der einzelnen Datensätze in 
\autoref{tab:Narr} dargestellt. Während der Großteil des Korpus 
\ac{UTF-8}-kodiert ist, sind einige Einträge nicht \ac{UTF-8}-kodiert. Diese 
werden vor dem Training ausgefiltert und dadurch ignoriert. Aus diesem Grund 
weichten die Werte in dieser Tabelle etwas von den Werten des originalen 
Datensatzes ab. 

\medskip
Der angegebene Wert der Fleiss' Kappa-Übereinstimmung von 0,419 und die 
prozentuale Übereinstimmung ($1\,719/1\,800=95,50~\%$ und $958/1\,800=53,22~\%
$) zeigen, wie schlecht selbst beim Menschen die Übereinstimmung im Bezug auf 
eine Empfindung ist und damit auch die Schwierigkeit der \ac{SA}. Bei den 
subjektiven Einträgen fällt eine Tendenz zu positiven Tweets auf, dieser 
Umstand wurde bereits von Dodds et~al. beschrieben 
\QuoteIndirectNoPage{dodds2015a}.

\subsubsection{Der Korpus von Mozeti{\v{c}} et~al.}
\label{subsubsec:Moz}

Der von Mozeti{\v{c}}, Gr{\v{c}}ar und Smailovi{\'c} veröffentlichte Korpus 
enthält 1,6 Millionen Tweets, verfasst in 13 Sprachen: Albanisch, Bosnisch, 
Bulgarisch, Deutsch, Englisch, Kroatisch, Polnisch, Portugiesisch, Russisch, 
Serbisch, Slowakisch, Spanisch, Schwedisch und Ungarisch 
\QuoteIndirect{DBLP:journals/corr/MozeticGS16}{S. 3}. Wobei, wie auch schon in 
\autoref{subsubsec:Narr}, nur der deutschsprachige Korpus genauer beschrieben 
werden soll. In der ursprünglichen Arbeit wurden verschiedene \ac{SA}-Verfahren 
an dem Korpus getestet, um unter anderem zu erforschen, wie stark die Qualität 
des Korpus einen Einfluss auf die Klassifikationsqualität ausübt 
\QuoteIndirect{DBLP:journals/corr/MozeticGS16}{S. 1}.

\begin{table}[htb]
	\begin{tabularx}{\textwidth}
	{|>{\Centering}X|
	>{\Centering}X|
	>{\Centering}X|
	>{\Centering}X|
	>{\Centering}X|
	>{\Centering}X|}
	
		\hline
		\multicolumn{6}{|c|}{Selbst-Übereinkunft} \\
		\hline
		Anzahl & Häufigkeit & Genauigkeit innerhalb 1 & 
		Genauigkeit & F-Maß & $\alpha$ \\ 
		\hline 
		$6\,643$ & 6,1~\% & 0,975 & 0,814 & 0,731 & 0,666 $\pm$ 0,022 \\
		
		\hline \hline
		
		\multicolumn{6}{|c|}{Übereinkunft} \\ 
		\hline
		Anzahl & Häufigkeit & Genauigkeit innerhalb 1 & 
		Genauigkeit & F-Maß & $\alpha$ \\ 
		\hline 
		$4\,539$ & 4,2~\% & 0,974 & 0,497 & 0,418 & 0,344 $\pm$ 0,026 \\ 
		\hline
		
	\end{tabularx} 

	\caption[Daten und Statistiken zur Übereinkunft im Korpus von 
	Mozeti{\v{c}} et~al.]
	{Daten und Statistiken zur Übereinkunft im Korpus von Mozeti{\v{c}} et~al., 
	Quelle: \cite[S. 16, 19, Tab. 3, Tab. 4]{DBLP:journals/corr/MozeticGS16}}
	\label{tab:Moz}
\end{table}

\medskip
Wie in \autoref{subsec:Subjektivität} vorgeschlagen, wurde ein Teil der Tweets 
sowohl von dem gleichen Beobachter (Selbst-Übereinkunft) als auch von zwei 
unterschiedlichen Beobachtern (Übereinkunft) klassifiziert. In 
\autoref{tab:Moz} werden die daraus berechneten Metriken und die Anzahl der 
mehrfach klassifizierten Einträge dargestellt 
\QuoteIndirect{DBLP:journals/corr/MozeticGS16}{S. 4}.

\begin{table}[htb]
	\centering
	\begin{tabular}{|c|c|c|}
	\hline 
	Klasse & Absolut & Relativ [\%] \\ 
	\hline \hline \Grayrow
	Positiv
	 & $28\,452$
	 & 26,07 \\ 
	\hline 
	Neutral
	 & $60\,061$
	 & 55,04 \\ 
	\hline \Grayrow
	Negativ 
	 & $20\,617$
	 & 18,89 \\ 
	\hline 
	Total
	 & $109\,130$
	 & \\ 
	\hline 
	\end{tabular} 

	\caption[Daten und Statistiken zum Korpus von Mozeti{\v{c}} et~al.]
	{Daten und Statistiken zum Korpus von Mozeti{\v{c}} et~al., 
	Quelle: \cite[S. 15, Tab. 1]{DBLP:journals/corr/MozeticGS16}}
	\label{tab:Moz2}
\end{table}

%\medskip
Obwohl dieser Korpus von fünf Menschen annotiert wurde, haben zwei von ihnen 
90~\% der Arbeit geleistet. Dabei unterscheidet sich die Qualität ihrer Arbeit 
stark: Die Selbst-Übereinkunft des Ersten erreicht einen Wert von 
$\alpha_A=0,59$, während die des Zweiten mit $\alpha_B=0,76$ wesentlich höher 
liegt \QuoteIndirect{DBLP:journals/corr/MozeticGS16}{S. 8, 16}. Alle Tweets 
wurden zwischen Februar und Juni 2014 veröffentlicht
\QuoteIndirect{DBLP:journals/corr/MozeticGS16}{S. 15, Tab. 1}. Insgesamt ist 
die Übereinkunft mit $\alpha=0,344$ niedriger als in \autoref{subsubsec:alpha} 
gefordert \QuoteIndirect{DBLP:journals/corr/MozeticGS16}{S. 8}. Mangels 
Alternativen wird der Korpus trotz dieser Unzulänglichkeit genutzt.

\medskip
Wenn auch nicht so extrem wie bei dem Korpus von Narr et~al., fällt in 
\autoref{tab:Moz2} eine Tendenz der subjektiven Tweets Richtung positiven 
Aussagen auf.

\medskip
Der Korpus kann in einem Datensatz pro Sprache heruntergeladen werden. Der 
Datensatz enthält die Tweet-IDs, die Klasse und eine anonymisierte 
Beobachter-ID \QuoteIndirect{DBLP:journals/corr/MozeticGS16}{S. 16}. Um aus 
diesen Informationen einen für das \ac{DAN}-System brauchbaren Korpus zu 
generieren, muss der Datensatz um den Text des eigentlichen Tweets erweitert 
werden. Weiterhin sollte der Tweet vorverarbeitet werden, um Nutzernamen, Links 
und Zeilenumbrüche zu entfernen.

\smallskip
\begin{minipage}{\linewidth}
\begin{pythonsmall}{Generierung der Liste zum Herunterladen der Tweets, in Anlehnung an: \cite{API:Twitter}}{Generierung der Liste zum Herunterladen der Tweets}{list:GetMoz1}
    with open(idfilepath, 'rb') as idfile:
        for line in idfile:
            (TweetID, HandLabel, AnnotatorID) = line.split(',')
            
            tweet_ids.append(TweetID)
            rest_list.append(TweetID + "," + HandLabel + "," + AnnotatorID)
    
            # API limits batch size to 100
            if len(tweet_ids) >= 100:
                get_tweet_list(twapi, tweet_ids, rest_list)
                tweet_ids = list()
                rest_list = list()

    if len(tweet_ids) > 0:
        get_tweet_list(twapi, tweet_ids, rest_list)
\end{pythonsmall}
\end{minipage}

\medskip
Mittels eines Python-Scripts, das die Tweepy-Bibliothek zur einfacheren 
Bedienung der Twitter-\ac{API} nutzt, können die Tweets anhand ihrer ID, sofern 
sie nicht gelöscht wurden, heruntergeladen werden 
\QuoteIndirectNoPage{API:Tweepy, API:Twitter}. Danach kann mittels eines 
gängigen Texteditors und regulären Ausdrücken der Korpus weiterverarbeitet 
werden.

\smallskip
\begin{minipage}{\linewidth}
\begin{pythonsmall}{Herunterladen der Tweets, in Anlehnung an: \cite{API:Twitter}}{Herunterladen der Tweets}{list:GetMoz2}
def get_tweet_list(twapi, idlist, rest_list):
    tweets = twapi.statuses_lookup(id_=idlist, include_entities=True, trim_user=True)
    
    for line in rest_list:       
        (TweetID, HandLabel, AnnotatorID) = line.split(',')
        TweetText, TweetIDint = "", 0
        
        for i in tweets:
            TweetIDint = int(TweetID)
            if i.id == TweetIDint:
                if i.retweeted_status:
                    TweetText = "RT: " + i.retweeted_status.text.encode('UTF-8', 'backslashreplace')
                    break
                TweetText = i.text.encode('UTF-8', 'backslashreplace')
                break
            else:
                TweetText = "--NO_-_TWEET_-_FOUND--"

        TweetText = TweetText.replace('\n', ' ').replace('\r', ' ')

        print(TweetID + "\t" + HandLabel + "\t" + AnnotatorID[:-2] + "\t" + TweetText)
\end{pythonsmall}
\end{minipage}

\medskip
Um auf die Twitter-\ac{API} zugreifen zu können, werden einige Werte zur 
Authentifizierung des Nutzers und der Anwendung benötigt. Um diese Werte zu 
erlangen, ist eine Registrierung bei Twitter notwendig. Ist die 
Authentifizierung erfolgt, kann der Datensatz eingelesen und die Tweets in 
Blöcken von maximal 100 Stück heruntergeladen werden (siehe 
\autoref{list:GetMoz1}). Falls es sich bei dem Tweet um einen ReTweet handelt, 
wird dem Text ein \QuoteM{RT: } vorangestellt, um die maximale Länge von 140 
Zeichen nicht zu überschreiten. Weiterhin werden Zeilenumbrüche gefiltert und 
der Text in \ac{UTF-8} codiert. Falls der Tweet nicht mehr vorhanden ist, wird 
ein Platzhalter eingefügt (siehe \autoref{list:GetMoz2}).

\medskip
Da seit der Erstellung des Korpus einige Tweets gelöscht wurden, konnten von 
den insgesamt $109\,130$ Tweets nur noch $91\,171$ (83,54~\%) heruntergeladen 
werden.

\medskip
Bei der Vorverarbeitung des Korpus wird \verb!.*--NO_-_TWEET_-_FOUND--\n! durch 
einen leeren String ersetzt, so werden die bereits gelöschten Tweets aus dem 
Korpus entfernt. Um die Nutzernamen zu vereinheitlichen, wird \verb!@(\w)*! 
durch \texttt{\grqq USER\grqq} ersetzt. Anschließend werden die Links, 
gekennzeichnet als \path{(http|ftp|https)://([\w_-]+(?:(?:\.[\w_-]+)+))([\w.,@?
^=%&:/~+#-]*[\w@?^=%&/~+#-])?}, durch \\ \texttt{\grqq URL\grqq} ersetzt.