\label{sec:Auswahl}

Im Folgenden soll ein kurzer Überblick über die Historie solcher Deep Learning 
Frameworks gegeben werden. Danach wird das für diese Anwendung genutzte 
Framework durch die Definition von Anforderungen und eine Marktstudie 
festgelegt.

\medskip
Die ersten Anfänge der Deep Learning Frameworks reichen in die Zeit nach der 
Jahrtausendwende zurück, z.B. OpenNN, dessen Entwicklung 2004 begonnen wurde 
\QuoteIndirectNoPage{FLOODs}, oder Torch, welches auf Lua basiert und 2002 
veröffentlicht wurde \QuoteIndirectNoPage{bengio:2002:idiap-02-46}. Aus der 
gleichen Zeit stammen die ersten Überlegungen, die Effizienz einer \ac{GPU} in 
der Matrixmultiplikation zur Beschleunigung des Lernens eines \acp{ANN} zu 
nutzen \QuoteIndirectNoPage{OH20041311}. Der initiale Commit der inzwischen 
eingestellten \QuoteIndirectNoPage{Theano:future} Theano-Bibliothek fand im 
Jahr 2008 statt \QuoteIndirectNoPage{github:Theano}. Das erste CUDA-Toolkit 
wurde ein Jahr vorher, im Jahre 2007, veröffentlicht 
\QuoteIndirectNoPage{CUDA:Version1}.

\smallskip
Die Wurzeln der meisten Deep Learning Frameworks liegen in der Zeit zwischen 
2011 und 2015, so entstand TensorFlow in Jahre 2011 
\QuoteIndirectNoPage{DBLP:journals/corr/AbadiABBCCCDDDG16}, die initialen 
Commits von Caffe und Keras stammen aus dem Jahr 2013 
\QuoteIndirectNoPage{github:Caffe} und 2015 
\QuoteIndirectNoPage{github:keras}.

\subsection{Anforderungen an das Deep Learning Framework}
\label{subsec:Anforderungen}

Im Folgenden sollen einige Anforderungen an ein für diese Arbeit geeignetes 
Deep Learning Framework aufgestellt werden, diese Anforderungen werden in 
Anlehnung an die Normsprache nach Rupp formuliert 
\QuoteIndirect{RequirementsEngineering}{S. 215 ff.}. Einige dieser 
Anforderungen lassen sich auch in anderen Ausarbeitungen zu diesem Thema finden 
\QuoteIndirectNoPage{Medium:Landscape}.
\index{Normsprache nach Rupp}

\begin{enumerate}
	\setlength\itemsep{\Itemizespace}

	\item \label{enum:Kosten} Für den geplanten Anwendungszweck sollten keine 
	finanziellen Kosten durch die Benutzung des Deep Learning Frameworks 
	entstehen.

	\item \label{enum:etabliert} Das Deep Learning Framework sollte am Markt 
	etabliert sein.
	
	\begin{itemize}
		\setlength\itemsep{\Itemizespace}

		\item Ein etabliertes Deep Learning Framework besitzt eine größere 
		Nutzergemeinde. So lassen sich für Fehler und Probleme schneller 
		Lösungen finden.

		\item Die Wahrscheinlichkeit, dass der Hersteller die Weiterentwicklung 
		des Deep Learning Frameworks einstellt, ist bei etablierten Frameworks 
		geringer. Umgekehrt werden neue Anforderungen schneller umgesetzt.

	\end{itemize}

	\item \label{enum:aktivGepflegt} Das Framework muss aktiv gepflegt werden.

	\item \label{enum:Anforderungen} Die in \autoref{sec:Anforderungen} 
	definierten Anforderungen sollten sich möglichst einfach mit dem Deep 
	Learning Framework in die Realität umsetzen lassen.

	\begin{itemize}
		\setlength\itemsep{\Itemizespace}

		\item Das Framework muss eine \ac{GPU}-Beschleunigung mittels CUDA 
		unterstützen. Das Framework muss jedoch auch ohne 
		\ac{GPU}-Beschleunigung (auf der \ac{CPU}) lauffähig sein (siehe 
		\ref{text:CUDA}).

		\item Das Deep Learning Framework sollte Möglichkeiten bereitstellen, 
		Metriken zu messen, abzuspeichern (siehe \ref{text:MetadataTrain} und 
		\ref{text:MetadataTest}) und zu visualisieren (siehe 
		\ref{text:Tensorboard}).

		\item Das Framework muss mit der Hardware eines Desktop-\acp{PC} 
		auskommen (siehe \ref{text:Hardware}).

		\item Das Deep Learning Framework muss sich sowohl auf der aktuellen 
		Version von Windows als auch der aktuellen Version einer bekannten 
		Linux-Distribution ausführen lassen (siehe \ref{text:Software}).

	\end{itemize}

	\item \label{enum:dokumentiert} Die Funktionalitäten des Frameworks müssen 
	gut dokumentiert sein. Darüber hinaus ist das Vorhandensein von Tutorials, 
	insbesondere für \acp{GAN}, wünschenswert.

	\item \label{enum:einfachBedienen} Das Deep Learning Framework sollte sich 
	einfach bedienen lassen.

	\item \label{enum:Python} Die \ac{API} des Frameworks muss eine bekannte 
	Sprache, idealerweise Python, unterstützen.

	\item \label{enum:skalieren} Das Framework sollte in der Lage sein, auf 
	mehreren \acp{CPU}, \acp{GPU} und Computern zu skalieren. Wobei dies nur 
	eine schwache Anforderung ist, da im Rahmen dieser Masterarbeit nicht genug 
	Hardware zur Verfügung steht, um eine vertikale oder horizontale 
	Skalierbarkeit durchzusetzen.

	\item \label{enum:schnellesTraining} Das Framework sollte ein möglichst 
	schnelles Training erlauben.

	\item \label{enum:DANAlgorithmen} Das Framework muss alle für das 
	\ac{DAN}-System nötigen Algorithmen beinhalten. Darüber hinaus sollte es 
	Funktionen zur einfachen Verarbeitung und Vorverarbeitung von Text 
	bereitstellen.

\end{enumerate}

\subsection{Marktstudie}
\label{subsec:Marktstudie}

Im Folgenden werden einige der aktuell verfügbaren Deep Learning Frameworks 
hinsichtlich ihrer Eignung für das \ac{DAN}-System beschrieben. Da die Auswahl 
an Frameworks sehr groß ist \QuoteIndirectNoPage{DL4J:Compare, wiki:dls, 
DLL:lang, medium:DLF2017}, muss eine Vorauswahl getroffen werden, die nur die 
bekanntesten Frameworks berücksichtigt.

\bigskip
\textit{Caffe}\footnote{\url{http://caffe.berkeleyvision.org}} geht auf die 
Doktorarbeit von Yangqing Jia an der Berkeley-Universität in Kalifornien zurück 
und wird auch von dieser weiter gepflegt \QuoteIndirectNoPage{Caffe}. Dieses 
Framework fokussiert sich insbesondere auf \ac{CNN} und maschinelles Sehen. 
\index{Deep Learning Framework>Caffe}

\smallskip
Bei einem \ac{CNN} wird ein kleiner Bereich der Eingabedatei (oftmals ein Bild) 
zeilen- und spaltenweise abgerastert und das entstehende Teilbild verkleinert, 
dadurch lassen sich Strukturen unabhängig von ihrer konkreten Position in der 
Eingabedatei betrachten. Obwohl es Ansätze gibt, diese \ac{CNN}-Technologie 
auch für \ac{NLP} und \ac{SA} zu nutzen 
\QuoteIndirectNoPage{DBLP:journals/corr/Kim14f, DBLP:journals/corr/KimJSR15}, 
ist Caffe durch diese Spezialisierung für dieses Projekt ungeeignet.
\index{Convolutional Neural Network}

\smallskip
Weiterhin ist das Framework nur schwach dokumentiert und dank vieler 
Abhängigkeiten schwierig zu installieren. Caffe besitzt eine \ac{API}, die sich 
mittels Python und MATLAB ansteuern lässt \QuoteIndirectNoPage{Medium:DLFrev}.

\bigskip
Aufbauend auf Caffe wurde 2015 die Entwicklung von \textit{Caffe2}%
\footnote{\url{https://caffe2.ai}} begonnen 
\QuoteIndirectNoPage{github:Caffe2}. Im April 2017 hat Facebook das Framework 
und seinen Quellcode veröffentlicht \QuoteIndirectNoPage{Caffe2:openSource}.
In Caffe2 wurden einige Schwächen von Caffe beseitigt, beispielsweise können 
nun auch Aufgaben jenseits des maschinellen Sehens erfüllt werden. Caffe2 lässt 
sich auch auf mobilen Endgeräten einsetzen 
\QuoteIndirectNoPage{Caffe2:vsCaffe}. Wie bei Caffe lässt sich die \ac{API} von 
Caffe2 mit Python ansteuern. Neben Windows und MacOS X werden verschiedene 
Linux-Distributionen (inklusive Android) unterstützt 
\QuoteIndirectNoPage{Caffe2:Install}.
\index{Deep Learning Framework>Caffe2}

\smallskip
Verglichen mit anderen Deep Learning Frameworks (z.B. TensorFlow) ist Caffe2 
stärker auf eine Anwendung in der Wirtschaft und fertigen Produkten, 
insbesondere auf Smartphones, ausgelegt. Für wissenschaftlich orientierte 
Anwendungen eignen sich andere Frameworks besser
\QuoteIndirectNoPage{quora:Caffe2vsTF}.

\bigskip
Für viele Anwendungen, insbesondere für die schnelle Entwicklung eines 
Prototypen, sind die \acp{API} der meisten Deep Learning Frameworks zu komplex 
und dadurch ist die Entwicklung zu zeitaufwendig. Um ohne viel Ballast Ideen 
ausprobieren zu können, hat sich \textit{Keras}%
\footnote{\url{https://keras.io}} etabliert. Dabei dient Keras nur als 
High-Level-\ac{API}, geschrieben in Python, die auf anderen Frameworks (z.B. 
TensorFlow, Theano oder Microsoft Cognitive Toolkit) aufsetzt und deren 
Funktionalitäten nutzt \QuoteIndirectNoPage{keras:higlevel}. Am Beispiel eines 
einfachen \acp{ANN}, das eine XOR-Funktion erlernen soll, ist dieser 
High-Level-Ansatz gut ersichtlich \QuoteIndirectNoPage{github:XOR}. Ende 2016 
war Keras (nach TensorFlow) das am zweitschnellsten wachsende Deep Learning 
Framework \QuoteIndirectNoPage{Twitter:Landscape}.

\smallskip
Der High-Level-Ansatz von Keras kann auch als Nachteil ausgelegt werden, denn 
viele Abläufe können nicht mehr direkt gesteuert werden. Da eine akademische 
Anwendung etwas detaillierter und besser nachvollziehbar sein sollte, kann 
Keras für diese Arbeit als unpassend betrachtet werden, obwohl es sich bei dem 
\ac{DAN}-System um einen Prototypen handelt, der damit in den Bereich von Keras 
fallen würde.
\index{Deep Learning Framework>Keras}

\bigskip
Auf Github begann die Entwicklung des \textit{Microsoft Cognitive Toolkit}%
\footnote{\url{https://www.microsoft.com/en-us/cognitive-toolkit}} 
im Jahre 2014 \QuoteIndirectNoPage{github:CNTK}. Auch dieses Framework stellt 
unter anderem eine Python-\ac{API} zur Interaktion bereit. Das zuerst als CNTK 
bekannte Framework zeichnet sich durch eine hohe Geschwindigkeit und gute 
Skalierungseigenschaften aus. Insbesondere die horizontale Skalierung ist dank 
starker Optimierung verlustärmer als bei anderen Frameworks 
\QuoteIndirectNoPage{Medium:DLFrev, Microsoft:Toolkit}. Dies lässt sich auch 
durch Benchmarks zeigen \QuoteIndirectNoPage{Microsoft:performance}. Auf Basis 
dieses Frameworks wurde die erste Spracherkennungssoftware entwickelt, deren 
Fehlerrate mit der von Menschen vergleichbar ist.
\QuoteIndirectNoPage{Microsoft:speechRecognition,
DBLP:journals/corr/XiongDHSSSYZ16a}

\index{Deep Learning Framework>Microsoft Cognitive Toolkit}
\index{Deep Learning Framework>Microsoft Cognitive Toolkit>CNTK}

\smallskip
Obwohl sich mit dem Microsoft Cognitive Toolkit auch andere Arten von Problemen 
angehen lassen, liegt der Fokus dennoch auf der Spracherkennung und nicht, wie 
für diese Arbeit benötigt, beim \ac{NLP} \QuoteIndirectNoPage{CIO:Compare}. 
Wie auch schon Caffe2 eignet sich dieses Framework besser für wirtschaftliche 
Anwendungen.

\bigskip
\textit{TensorFlow}\footnote{\url{https://www.tensorflow.org}} ist eines der 
bekanntesten und meist genutzten Deep Learning Frameworks 
\QuoteIndirectNoPage{Twitter:Landscape, CIO:Compare, anal:5pop, 
Medium:Landscape, Medium:Peek}. Seine Wurzeln liegen bei einem Projekt namens 
\QuoteM{DistBelief} aus dem Jahre 2011
\QuoteIndirectNoPage{DBLP:journals/corr/AbadiABBCCCDDDG16}. Von da aus 
entwickelte sich das Projekt über den initialen Github commit im November 2015  
\QuoteIndirectNoPage{github:TensorFlow} bis zur Veröffentlichung der Version 
1.0.0 im Februar 2017 \QuoteIndirectNoPage{github:TF100} stetig weiter.

\smallskip
TensorFlow bietet \acp{API} für einige künstliche Sprachen an, darunter auch 
Python \QuoteIndirectNoPage{TF:API}. Dieses Framework lässt sich auf vielen 
Betriebssystemen (z.B. Windows, Linux oder MacOS X 
\QuoteIndirectNoPage{TF:Installing}) und Plattformen (z.B. Android oder iOS 
\QuoteIndirectNoPage{TF:Mobile}) ausführen. Darüber hinaus wird mit TensorBoard 
ein mächtiges Tool zur Visualisierung von Metriken, Datenströmen, zeitlichen 
Änderungen und des erstellten Graphen mitgeliefert 
\QuoteIndirectNoPage{TF:TensorBoard}. TensorFlow bietet sowohl 
Low-Level-Funktionen (z.B. \texttt{tf.nn}) an als auch darauf aufbauende 
abstraktere Schichten (z.B. \texttt{tf.contrib}). Auch spezielle 
\ac{NLP}-Funktionen, beispielweise zur Vorverarbeitung und Codierung eines 
Textes (wie z.B. \texttt{learn.preprocessing.VocabularyProcessor}), sind 
verfügbar.
\index{Deep Learning Framework>TensorFlow>TensorBoard}

\smallskip
Viele Tutorials für \acp{GAN} sind mittels TensorFlow umgesetzt 
\QuoteIndirectNoPage{GAN:aylienIntro, GAN:Jang}. Seit dem ersten September 2017 
bietet TensorFlow eine eigenständige Bibliothek für \acp{GAN} an 
\QuoteIndirectNoPage{TFGAN:readme, TFGAN}. Da zu diesem Zeitpunkt das 
\ac{DAN}-System bereits fertig implementiert war, wurden keine Funktionen aus 
dieser Bibliothek genutzt.
\index{Deep Learning Framework>TensorFlow}

\bigskip
Die Ursprünge von \textit{Torch}\footnote{\url{http://torch.ch}} reichen bis in 
das Jahr 2002 zurück \QuoteIndirectNoPage{bengio:2002:idiap-02-46}. Da die 
\ac{API} dieses Frameworks sich auf Lua beschränkt, soll an dieser Stelle 
stärker auf \textit{PyTorch}\footnote{\url{http://pytorch.org}} eingegangen 
werden, dessen \ac{API} auch über Python erreichbar ist. Der initiale Github 
commit von PyTorch fand im Januar 2012 statt 
\QuoteIndirectNoPage{github:pytorch}. Dieses Framework ist neu geschrieben und 
damit mehr als nur ein Python-Aufsatz für Torch 
\QuoteIndirectNoPage{pytorch:vsTF}. Das PyTorch-Framework wird für Linux und 
MacOS X \QuoteIndirectNoPage{pytorch:pattform}, nicht jedoch für Windows 
angeboten \QuoteIndirectNoPage{pytorch:win}. Der starren Ansatz von TensorFlow, 
einen Graphen erst zu definieren und danach die Berechnungen auszuführen, wird 
bei PyTorch nicht übernommen, damit ist es wesentlich dynamischer. Darüber 
hinaus bietet PyTorch keine Möglichkeit für verteiltes Training 
\QuoteIndirectNoPage{pytorch:vsTF}.

\index{Deep Learning Framework>Torch}
\index{Deep Learning Framework>PyTorch}

\bigskip
Mit einer Versionsgeschichte, die bis in das Jahr 2008 reicht, gehört
\textit{Theano}\footnote{\url{http://deeplearning.net/software/theano}} zu den 
älteren Deep Learning Frameworks \QuoteIndirectNoPage{github:Theano}. Ähnlich 
wie TensorFlow mit TensorBoard bietet auch Theano mit d3viz ein 
Visualisierungs-Tool an \QuoteIndirectNoPage{quora:uniqueTF}. Auch die \ac{API} 
dieses Frameworks lässt sich mittels Python erreichen 
\QuoteIndirectNoPage{Bergstra2011TheanoAC}. Theano verfügt nicht über 
Möglichkeiten der Skalierung \QuoteIndirectNoPage{Medium:DLFrev}. Darüber 
hinaus wird Theano seit der Version 1.0 nicht mehr weiterentwickelt 
\QuoteIndirectNoPage{Theano:future}.

\index{Deep Learning Framework>Theano}

\subsection{Review der Ergebnisse}
\label{subsec:Review}

Von den in \autoref{subsec:Marktstudie} vorgestellten Deep Learning 
Frameworks erfüllen nicht alle die in \autoref{subsec:Anforderungen} 
definierten Anforderungen. So eignet sich Caffe nicht, da es auf maschinelles 
Sehen spezialisiert ist (siehe \autoref{enum:DANAlgorithmen}). Die \ac{API} von  
Torch unterstützt nur Lua und erfüllt dadurch \autoref{enum:Python} nicht. 
PyTorch besitzt zwar eine Python-\ac{API}, ist aber nicht auf Windows lauffähig 
(siehe \autoref{enum:Anforderungen}). Theano wird nicht mehr aktiv gepflegt, 
somit bleibt \autoref{enum:aktivGepflegt} offen.

\medskip
Caffe2, Keras und das Microsoft Cognitive Toolkit erfüllen zwar alle kritischen 
Anforderungen, jedoch eignet sich TensorFlow für diese Ausarbeitung am besten, 
da es über die kritischen Anforderungen hinaus noch weitere Anforderungen (z.B.  
\autoref{enum:dokumentiert}) erfüllt.

\smallskip
Es bietet \acp{API} für viele Sprachen und ist auf allerhand Plattformen und 
Betriebssystemen lauffähig. Weiterhin bietet es mit TensorBoard eine 
vielseitige und einfache Möglichkeit an, gewonnene Metadaten zu visualisieren. 
Da TensorFlow zu den beliebtesten und meistgenutzten Deep Learning Frameworks 
gehört, finden sich entsprechend viele Tutorials, insbesondere auch über die 
Erstellung von \ac{GAN}-Systemen. TensorFlow bietet auch spezielle 
\ac{NLP}-Funktionen und eine gute Dokumentation an. Auch wenn es im Rahmen 
dieser Arbeit nicht genutzt wird, lässt sich mit TensorFlow ein \ac{ANN} auch 
verteilt trainieren.
