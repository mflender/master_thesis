\label{sec:Training}

In diesem Abschnitt werden die \acp{ANN} aus \autoref{sec:Network} und die 
vorverarbeiteten Korpora aus \autoref{sec:InputEncoding} zusammengeführt und so 
das Netzwerk trainiert. Die im Folgenden dargestellten Abläufe sind, falls 
nicht anders beschrieben, in der Datei \texttt{trainer.py} zusammengefasst.

\medskip
Um das Programm zu starten, wird der Python-Interpreter mit der Datei 
\texttt{main.py} und den entsprechenden Argumenten (siehe 
\ref{text:SystemparameterAnpassen}) gestartet. Außer der Verwaltung der 
Argumente, inklusive ihrer Hilfstexte (siehe \ref{text:HelpText}) und dem 
Aufruf des Trainings, findet in \texttt{main.py} nichts weiter statt.

\medskip
Da während des Trainings die erzeugten Modelle (siehe \ref{text:Save}) sowie 
verschiedene Metadaten (siehe \ref{text:MetadataTrain} und 
\ref{text:MetadataTest}) abgespeichert und gegebenfalls wieder aufgerufen 
werden müssen, wird zunächst ein neuer Ordner (\texttt{runs}) auf Ebene der 
Programmdateien angelegt. Durch \ref{text:SaveFolder} ist vorgeschrieben, dass 
unterschiedliche Testläufe durchführbar sein sollen. Deswegen wird anschließend 
für jeden Programmstart ein Unterordner mit dem aktuellem Zeitstempel im 
\ac{ISO}-Format angelegt, z.B. \texttt{2017-10-30T11:06:22.972781}.

\medskip
Um das Training des \ac{DAN}-Systems wiederaufnehmen zu können (siehe 
\ref{text:Retrain}) und eine gute Dokumentation zu gewährleisten, werden die 
Argumente aus \texttt{main.py} und einige wichtige Parameter in der Datei 
\texttt{args.txt} als Schlüssel-Wert-Paare abgespeichert (siehe 
\ref{text:SystemparameterAbspeichern}) und bei Bedarf wieder eingelesen sowie 
verändert. Falls die Kodierung auf Wortebene genutzt wird, wird das generierte 
Lexikon (\texttt{vocab}) auch an dieser Stelle abgelegt. Sowohl für die 
Metadaten (\texttt{summaries}) als auch die Speicherpunkte des Modells 
(\texttt{checkpoints}) werden weitere Unterordner angelegt.

\medskip
Alle nachfolgenden Operationen, inklusive des in \autoref{sec:Network} 
beschriebenen Anlegen der \ac{DAN}-Struktur, werden innerhalb eines 
\texttt{tf.Graph} und einer \texttt{tf.Session} gekapselt. Innerhalb dieser 
Umgebung wird der in \autoref{sec:Basis} beschriebene Graph aufgebaut und 
ausgeführt. 
\index{Graph}

\medskip
Um die in \autoref{sec:InputEncoding} generierten Merkmalsvektoren in das 
Netzwerk einzuleiten, werden zunächst Platzhalter-Tensoren 
(\texttt{tf.placeholder}) mit entsprechender Dimensionierung erstellt und die 
in \autoref{sec:Network} beschriebenen Netzwerke angelegt.

\bigskip
Damit das \ac{DAN}-System mit einem direkt trainierten Analysator verglichen 
werden kann (siehe \ref{text:AnalSingle}), ist es möglich, den Analysator 
einzeln zu trainieren. Falls diese Option gewählt wird, wird der in 
\autoref{list:LossAnalysator} dargestellte Code ausgeführt und damit die 
Fehlerfunktion definiert. Als Optimierungsalgorithmus kommt das Adam-Verfahren 
zum Einsatz \QuoteIndirectNoPage{DBLP:journals/corr/KingmaB14}.

\smallskip
Das Adam-Verfahren stellt eine Weiterentwicklung des klassischen 
Gradientenverfahrens dar. Zur Veranschaulichung kann der Fehler des Systems  
als 2\ac{D}-Fläche betrachtet werden, deren Minimum die beste Lösung 
darstellt. Um dieses Minimum zu erreichen, wird dem negativen Gradienten bis 
zum Erreichen des Tals schrittweise gefolgt. Dabei muss eine Schrittweite oder 
Lernrate vorgegeben werden: Ist sie zu groß, wird das Minimum leicht 
übersprungen, ist sie zu klein, verlängert sich die Trainingszeit. Beim 
Adam-Verfahren passt sich die Lernrate an die Steilheit des Gradienten an und 
findet so schneller das Minimum. Die initiale Lernrate muss bei diesem 
Verfahren vor dem Start angegeben werden \QuoteIndirectNoPage{Brownlee:ADAM}.
\index{AdamOptimizer}

\smallskip
\begin{minipage}{\linewidth}
\begin{pythonsmall}{Fehlerfunktion des einzeln trainierten Analysators}{Fehlerfunktion des einzeln trainierten Analysators}{list:LossAnalysator}
with tf.name_scope("Analyzer_Optimizer"):
	anal_loss = tf.reduce_mean(tf.nn.softmax_cross_entropy_with_logits(logits=anal_prediction, labels=anal_y))
	anal_optimizer = tf.train.AdamOptimizer(learning_rate=0.001)
	anal_train_op = anal_optimizer.minimize(anal_loss)
\end{pythonsmall}
\end{minipage}

\bigskip
Falls das \ac{DAN}-System als eine Einheit trainiert wird, werden zwei 
Diskriminator-Netze angelegt, das eine bezieht seine Daten aus dem 
Analysator $D(A(z))$, während das andere $D(X)$ vom zweiten Korpus gespeist 
wird. Die während des Trainings angepassten Gewichte werden dabei von beiden 
Netzwerken genutzt, sodass sie aus Sicht des Trainings eine Einheit bilden. 
Dieses Vorgehen ist nötig, da TensorFlow keine sinnvolle Möglichkeit bietet, 
ein Netzwerk aus zwei unterschiedlichen Quellen mit Eingaben zu versorgen.

\medskip
Auch um das \ac{DAN}-System zu trainieren, wird eine Fehlerfunktion (pro 
Netzwerk) benötigt. Über die in \autoref{sec:GAN} beschriebenen Funktionen aus 
der Arbeit von Goodfellow et~al. \QuoteIndirectNoPage{goodfellow2014generative} 
hinausgehend haben sich auch andere Fehlerfunktionen etabliert. So kann eine 
Alternative durch TensorFlows Kreuzentropie-Funktion aufgebaut werden 
\QuoteIndirectNoPage{GAN:Loss}. \autoref{list:LossDAN} stellt sowohl die 
Fehlerfunktionen aus \autoref{sec:GAN} als auch die Alternative dar. Als 
Optimierungsalgorithmus wird, unabhängig von der Fehlerfunktion auch für das 
\ac{DAN}-System, wieder das Adam-Verfahren genutzt.

\smallskip
\begin{minipage}{\linewidth}
\begin{pythonsmall}{Fehlerfunktionen des \ac{DAN}-Systems}{Fehlerfunktionen des \ac{DAN}-Systems}{list:LossDAN}
___Klassische Fehlerfunktion des Analysators:___
anal_loss = -tf.reduce_mean(tf.log(disc_from_anal_prediction))
___Alternative:___
anal_loss = tf.reduce_mean(tf.nn.sigmoid_cross_entropy_with_logits(labels=disc_from_anal_prediction, logits=tf.ones_like(disc_from_anal_prediction)))

___Klassische Fehlerfunktion des Diskriminators:___
disc_loss = -tf.reduce_mean(self.log(disc_corpus_prediction) + self.log(1 - disc_from_anal_prediction))
___Alternative:___
disc_loss_1 = tf.reduce_mean(tf.nn.sigmoid_cross_entropy_with_logits(labels=disc_corpus_prediction, logits=tf.ones_like(disc_corpus_prediction)))
disc_loss_2 = tf.reduce_mean(tf.nn.sigmoid_cross_entropy_with_logits(labels=disc_from_anal_prediction, logits=tf.zeros_like(disc_from_anal_prediction)))
disc_loss = tf.add(disc_loss_1, disc_loss_2)

___Feature matching:___
anal_loss = tf.sqrt(tf.reduce_sum(tf.pow(disc_corpus_intermediate - disc_from_anal_intermediate, 2)))
\end{pythonsmall}
\end{minipage}

\medskip
In Zeile 7 von \autoref{list:LossDAN} wird die Funktion \texttt{self.log} 
anstatt \texttt{tf.log} genutzt. Dies stellt eine Sicherheitsvorkehrung dar, da 
falls der Diskriminator extrem große Werte (z.B. $2,05 \cdot 10^7$) ausgibt,  
der Logarithmus einer negativen Zahl (z.B. $1 - 2,05 \cdot 10^7$) eine komplexe 
Zahl ergibt und TensorFlow damit nicht umgehen kann \mbox{(\ac{NaN})}. Um dies 
zu verhindern, wird der Eingangswert des Logarithmus auf eine positive Zahl  
begrenzt \QuoteIndirectNoPage{GAN:Minibatch}. Solch ein Verhalten kann sich 
ergeben, falls  die Ausgabeschicht des Diskriminators eine Aktivierungsfunktion 
mit (rechtsseitig) unbeschränkter Ausgabe nutzt, z.B. $(-\infty,\infty)$.

\bigskip
Darüber hinaus kann für den Analysator eine Fehlerfunktion, die das 
\QuoteM{Feature matching} implementiert, genutzt werden. Beim Feature matching 
handelt es sich um einen weiteren Ansatz aus der Arbeit von Salimans et~al. zur 
Verbesserung des Trainings eines \acp{GAN} 
\QuoteIndirect{DBLP:journals/corr/SalimansGZCRC16}{S. 2}. 

\smallskip
In den ersten Schichten eines \acp{ANN} bilden sich üblicherweise Strukturen, 
die aus dem Merkmalsvektor die informativen Merkmale extrahieren. Diesen 
Umstand nutzt das Feature matching aus, indem es den Analysator auf die 
Minimierung des Quadrates der Differenz zwischen den Hidden Layers der 
Diskriminatoren $D(A(z))$ und $D(X)$ optimiert 
\QuoteIndirectNoPage{GAN:Train2}. In Zeile 14 von \autoref{list:LossDAN} wird 
diese Fehlerfunktionen dargestellt. 
\index{Feature matching}

\bigskip
Nachdem nun sowohl die Netzwerke als auch ihre Fehlerfunktionen sowie die 
Optimierungsalgorithmen aufgebaut sind, werden die lokalen und globalen 
Variablen initialisiert und damit der Aufbau abgeschlossen, sowie das 
eigentliche Training begonnen.

\medskip
Um die Laufzeit des gesamten Trainings, aber auch einer einzelnen Epoche zu 
berechnen, wird zunächst die aktuelle Systemzeit gespeichert und anschließend 
das Training gestartet (siehe \ref{text:Print}). Nach einer als Argument 
definierten Anzahl von Trainingsläufen wird ein Testlauf durchgeführt (siehe 
\ref{text:TestPrintEvery}). Beim Training wird abermals zwischen dem gesamten 
\ac{DAN}-System und nur dem Analysator differenziert, zunächst soll das 
Training des Analysators beschrieben werden.

\medskip
Da, wie in \autoref{sec:InputEncoding} dargestellt, die kodierten Tweets als 
Python-Generator vorliegen, die Tweets aber in mehreren Stapeln verarbeitet 
werden, muss der Python-Generator in Stücke zerteilt werden, ohne dabei den 
gesamten Inhalt des Generators im \ac{RAM} zu halten. Um dies zu erreichen, 
kann z.B. das Itertools-Modul von Python genutzt werden
\QuoteIndirectNoPage{stack:Gen}.

\medskip
Mittels \texttt{sess.run} kann TensorFlow nun die Platzhalter durch die Daten 
aus der Eingabekodierung ersetzen und so die Werte der auch über diesen Befehl 
spezifizierten Ausgabeknoten innerhalb des Graphen berechnen und ausgeben. So 
kann das Netzwerk in einer Epoche mehrere Stapel und damit den gesamten Korpus 
zum Training benutzen. Auf beiden Ebenen sorgen Kommandozeilenausgaben dafür, 
dass der Anwender den aktuellen Stand des Trainings nachvollziehen kann.

\medskip
Falls das gesamte \ac{DAN}-System trainiert wird, wird nach dem Zerteilen des 
Python-Generators noch die Länge der beiden Korporateile überprüft, sodass  
falls ein Teil verbraucht ist, die Trainingsepoche beendet wird.

\bigskip
Wird ein bestehendes Training wieder aufgenommen (siehe \ref{text:Retrain}), 
muss der Graph nicht erneut angelegt werden. Nachdem mittels 
\texttt{tf.train.import\_meta\_graph} ein bestehendes Modell geladen wurde, 
können über die \texttt{graph.get\_""operation\_""by\_""name} Funktion alle 
nötigen Parameter aus dem Modell in eine Python-Variable geladen werden. Sind 
alle nötigen Variablen rekonstruiert, kann das Training wie oben beschrieben 
fortgesetzt werden.

\bigskip
Um mittels eines bestehenden Modells unbekannte Tweets zu klassifizieren, wird 
ähnlich vorgegangen und die nötigen Parameter werden zunächst aus dem Modell 
geladen. Anschließend muss der Tweet kodiert werden und das Analysator-Netzwerk 
mit dieser Eingabe ausgewertet werden. Abschließend wird die Stelle der 
One-Hot-kodierten Ausgabe mit dem größten Wert ermittelt und so die 
vorhergesagte Klasse bestimmt.
