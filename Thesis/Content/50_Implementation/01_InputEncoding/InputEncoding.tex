\label{sec:InputEncoding}

Wie in \ref{text:InputEncoder} gefordert, werden zwei konkurrierende 
Möglichkeiten der Eingabekodierung implementiert. Nach einer Beschreibung 
des Einlesens der Daten werden diese Möglichkeiten hier genauer betrachtet. Die 
im Folgenden beschriebenen Abläufe lassen sich in der Datei 
\path{input_encoder.py} wiederfinden.

\medskip
Beim Einlesen des Korpus von Narr et~al. werden die Zeilen, die keine 
\ac{UTF-8}-Zeichen enthalten oder mit der Klasse \QuoteM{irrelevant} annotiert 
wurden, ausgefiltert. Das Einlesen dieses Korpus kann teilweise aus einer 
früheren Arbeit übernommen werden 
\QuoteIndirectNoPage{DBLP:conf/lwa/FlenderG17}.

\medskip
Innerhalb des Datensatzes von Mozeti{\v{c}} et~al. erscheinen manche Tweets 
mehr als einmal, da sie mehrfach (i.d.R. doppelt) annotiert wurden. Mit maximal 
6,4~\% bzw. 4,2~\% ist dieser Fall nicht sehr häufig. Um mit diesem mehrfachen 
Erscheinen eines Tweets umzugehen, bieten sich verschiedene Möglichkeiten an: 
So können die Tweets im Korpus verbleiben, sodass das \ac{DAN}-System auch mit 
möglicherweise widersprüchlichen Daten umzugehen lernt. Eine andere Möglichkeit 
besteht darin, die Tweets zu vereinheitlichen, dabei kann eine der Klassen 
zufällig gewählt oder ein Durchschnitt berechnet werden. Da i.d.R. Tweets nur 
doppelt vorkommen und die Klassen nicht kontinuierlich aufgebaut sind, ist 
diese Berechnung schwierig. Unter anderem deswegen werden die mehrfachen Tweets 
in dieser Arbeit nicht entfernt.

\medskip
Üblicherweise wird ein \ac{ANN} in Epochen trainiert, wobei das Netz in einer 
Epoche alle verfügbaren Daten einmal vorgeführt bekommt. Da das \ac{DAN}-System 
zwei unterschiedlich große Korpora nutzt, beschränkt sich hier eine Epoche auf 
das vollständige Abarbeiten des kleineren Korpus. Innerhalb einer Epoche werden 
dem Netz mehrere Stapel (auf Englisch \QuoteM{Batch}), bestehend aus einer 
vorher festgelegten maximalen Anzahl an Tweets (auf Englisch 
\QuoteM{Batchsize}), vorgelegt.
\index{Trainingsepoche}
\index{Stapel>Batch}
\index{Stapel>Batchsize}

\smallskip
Um zu verhindern, dass das \ac{ANN} Strukturen in der Reihenfolge der Tweets 
erkennt, werden die Daten bei jeder Trainingsepoche zufällig sortiert. Dies 
führt dazu, dass in zwei unterschiedlichen Epochen andere Bestandteile des 
größeren Korpus betrachtet werden und dadurch das \ac{DAN}-System jeden Fall 
betrachten kann.

\medskip
Damit ein \ac{ANN} die Daten aus den Korpora auswerten kann, müssen die 
Korpora, die zum größten Teil aus Text bestehen, in eine Zahlenstruktur 
umgewandelt werden, denn ein \ac{ANN} kann nur Zahlen, aber keinen Text 
verarbeiten. Sowohl die Tweets als auch die Klassen müssen mithin kodiert 
werden. Zunächst soll das Verfahren zum Kodieren der Klassen genauer 
beschrieben werden. 

\medskip
Um die Klassen darzustellen, werden alle Werte üblicherweise 1-aus-n-kodiert 
(auf Englisch \QuoteM{One-hot encoding}). Bei solch einer Kodierung wird eine 
beliebige Zahl $n$ binär mit $n$ Ziffern dargestellt, wobei $n-1$ Bits auf 0 
gesetzt werden. Das der Zahl entsprechende Bit bekommt den Wert 1, z.B. wird 
die Zahl vier als $0,0,0,1$ dargestellt. Für die drei hier genutzten Klassen 
ergibt sich folgende Kodierung: \QuoteM{Positiv} ($1, 0, 0$), \QuoteM{Neutral} 
($0, 1, 0$) und \QuoteM{Negativ} ($0, 0, 1$).
\index{Kodierung>One-Hot}
\index{Kodierung>Klassen}

\smallskip
Obwohl diese Art der Kodierung durch die hohe Redundanz unpassend erscheint, 
wird sie im Umfeld der \acp{ANN} häufig genutzt, da jede Stelle der Zahl durch 
ein Neuron der Eingabeschicht aufgenommen wird.

\bigskip
Eine Möglichkeit, die Tweets zu kodieren, wurde bereits in \autoref{sec:GAN} 
angesprochen: Das von Mikolov et~al. 
\QuoteIndirectNoPage{Mikolov:2013:DRW:2999792.2999959} beschriebene 
Word2Vec-Verfahren dient zur Kodierung von Text auf Wortebene. Da die 
üblicherweise auf Twitter genutzte Sprache sehr informell ist, werden einige 
der im Korpus enthaltenen Wörter keine Entsprechung haben. Hinzu kommt, dass 
der ursprüngliche Word2Vec-Datensatz englischsprachig ist. Aus diesem Grund 
wird der Word2Vec-Ansatz hier nicht weiter verfolgt 
\QuoteIndirectNoPage{ashby:TensorFlow}.

\medskip
Da insbesondere der Korpus von Mozeti{\v{c}} et~al. zu groß ist, um vollständig 
im \ac{RAM} eines üblichen Desktop-\acp{PC} (siehe \ref{text:Hardware}) 
gehalten zu werden, können die Korpora nur stückchenweise verarbeitet werden, 
sodass nur ein Teil im Hauptspeicher liegt. Dazu werden für beide Kodierungen 
die von Python bereitgestellten Generatoren genutzt.

\medskip
Bis auf \acp{CNN} können neuronale Netze nur schlecht unterschiedlich große 
Eingabedaten verarbeiten, somit müssen für diese Anwendung (da keine \acp{CNN} 
genutzt werden) die Korpora so kodiert sein, dass alle Tweets gleich lang sind. 
Da sich die \acp{ANN} in den ersten Schichten die nützlichsten Informationen 
aus dem Merkmalsvektor selbst suchen, wird auf eine weiterführende 
Vorverarbeitung verzichtet.
\index{Convolutional Neural Network}

\subsection{Kodierung auf Zeichenebene}
\label{subsec:Zeichenebene}

Der primäre Datentyp in TensorFlow ist ein Tensor, dabei handelt es sich um ein 
Feld (auf Englisch \QuoteM{Array}) beliebiger Dimension, gefüllt mit einem 
primitiven Datentyp \QuoteIndirectNoPage{TensorFlow:GettingStarted}. Damit kann 
ein Skalar als 0\ac{D}- und ein Vektor als 1\ac{D}-Tensor betrachtet werden. 
Die hier beschriebene Kodierung der Tweets erzeugt einen 4\ac{D}-Tensor.
\index{Tensor}

\medskip
Um einen Tweet auf Zeichenebene zu kodieren, wird zunächst eine Liste mit allen 
darstellbaren Zeichen erzeugt. Da Groß- und Kleinschreibung keinen starken 
Einfluss auf die Klassifikationsqualität besitzen 
\QuoteIndirectNoPage{DBLP:conf/lwa/FlenderG17}, werden in dieser Arbeit alle 
Großbuchstaben konvertiert. Somit besteht die Liste der darstellbaren Zeichen 
aus allen Kleinbuchstaben, allen Ziffern, einigen Sonderzeichen, allen Umlauten
und den häufigsten \ac{UTF-8}-Smileys (insgesamt 109 Zeichen).

\medskip
Jedes Zeichen eines Wortes wird durch diese One-Hot-kodierte Liste dargestellt. 
Da der Merkmalsvektor, wie oben beschrieben, für jeden Tweet gleich lang sein 
muss, wird die Wortlänge mit 30 Zeichen angenommen. Ein empirischer Test am 
Korpus von Mozeti{\v{c}} et~al. hat zwar gezeigt, dass die maximal vorhandene 
Wortlänge bei 78 Zeichen liegt, jedoch gibt es wenige sinnvolle Wörter länger 
als 30 Zeichen. Ist ein Wort kürzer als 30 Zeichen, wird der Freiraum mit einem 
Platzhalter aufgefüllt \QuoteIndirectNoPage{ashby:Bidirectional}. Ist ein Wort 
hingegen länger als 30 Zeichen, wird das Ende des Wortes abgetrennt und 
ignoriert.

\medskip
Die maximale Anzahl der Wörter pro Tweet wird automatisch ermittelt und liegt 
für die genutzten Korpora bei 38. Wird eine Batchsize von 300 Tweets 
angenommen, ergibt sich daraus ein Ausgabetensor der Dimension 
$[300, 38, 30, 109]$. Nimmt man weiterhin für jeden Wert eine 
32-Bit-Fließkomma-Zahl an, so verbraucht ein Batch 1,11~GiB an Speicherplatz.

\bigskip
Der Vorteil dieser Kodierung ist, dass sich beliebige Wörter darstellen lassen, 
weiterhin können für andere Anwendungen auch Textstücke, die länger als 140 
Zeichen sind, dargestellt werden.

\subsection{Kodierung auf Wortebene}
\label{subsec:Wortebene}

Die Kodierung auf Wortebene erzeugt einen weit weniger komplexen 
2\ac{D}-Tensor, gebildet aus der Batchsize und der maximalen Anzahl an Worten 
pro Tweet (z.B: [300, 38]). Bei dieser Kodierung wird, beginnend mit dem ersten 
Eintrag, jedem Wort eine Zahl zugeordnet, wobei gleiche Wörter gleiche IDs 
erhalten \QuoteIndirectNoPage{wildml:VocabularyProcessor}. Enthält der Tweet 
weniger als 38 Wörter, wird der Merkmalsvektor mit 0 aufgefüllt. Dazu ein 
Beispiel aus dem Korpus von Narr et al. 
\QuoteIndirectNoPage{dailabor2012twittersentiment}:

\begin{quote}
	\QuoteM{Schnee Schnee Schnee \textasciitilde http}\\
	$[3648, 3648, 3648, 11, 0,0, \dots]$
\end{quote}

An diesem Beispiel lässt sich erkennen, dass häufig genutzte Worte, wie in 
diesem Fall die \QuoteM{\textasciitilde http}-Markierung, eine kleinere ID 
zugewiesen bekommen, da sie weiter vorne im Korpus auftreten. Diese Kodierung 
umzusetzen, ist sehr einfach, da TensorFlow unter
\texttt{tensorflow.contrib.learn.preprocessing} mit dem 
\texttt{Vocabulary\-Pro\-ces\-sor} eine fertige Implementierung anbietet
\QuoteIndirectNoPage{TensorFlow:VocabularyProcessor}. 

\medskip
Diese Kodierung ist zweistufig ausgelegt, in der ersten Stufe wird ein Lexikon, 
das jedes gefundene Wort und die dazugehörige ID beinhaltet, angelegt und 
abgespeichert. Damit das \ac{DAN}-System diese Kodierung sinnvoll nutzen kann, 
ist es wichtig, ein Lexikon für beide Korpora zu bilden, da sonst Worte, die 
nur in einem Korpus enthalten sind, ignoriert werden. In der zweiten Stufe wird 
der konkrete Tweet durch den oben beschriebenen Tensor mit den IDs ersetzt und 
so der Merkmalsvektor aufgebaut.

\smallskip
Damit diese Kodierung auch auf die spätere Klassifikation von unbekannten 
Tweets anwendbar ist, muss das zuvor erstellte Lexikon wiederverwendet werden. 
Somit wird das Lexikon zu einem Teil des Modells und als solches abgespeichert.

\bigskip
Verglichen mit der Kodierung auf Zeichenebene verbraucht dieser Ansatz 
wesentlich weniger Hauptspeicher, kann dafür aber keine Rechtschreibfehler oder 
absichtliche Verlängerungen von Worten abbilden, da z.B. \QuoteM{Ja} und 
\QuoteM{Jaa} von dieser Kodierung als zwei unterschiedliche Worte interpretiert 
werden.
