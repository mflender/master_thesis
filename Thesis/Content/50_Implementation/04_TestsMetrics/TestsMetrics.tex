\label{sec:TestsMetrics}

Ist ein Trainingslauf abgeschlossen, so wird durch das Testen unter anderem die 
Klassifikationsqualität ermittelt und abgespeichert. Die meisten der so 
generierten Werte können durch TensorBoard visualisiert werden. Auch die in 
diesem Abschnitt beschriebenen Abläufe finden sich in der Datei 
\texttt{trainer.py} wieder.

\medskip
Mithilfe von TensorBoard lassen sich verschiedene Arten von Daten 
visualisieren. Am häufigsten werden in dieser Ausarbeitung Skalare genutzt, die 
sich über die Zeit (Trainingsepochen) verändern. Darüber hinaus können auch 
komplette Tensoren dargestellt werden. Dazu gibt es zwei Möglichkeiten: zum 
einen als Histogramm und zum anderen als Verteilungen. Auch wenn in dieser 
Arbeit davon kein Gebrauch gemacht wird, können Bilder, Audiosequenzen und 
Texte dargestellt werden. Der innerhalb von TensorFlow erzeugte Graph lässt 
sich mittels TensorBoard visualisieren, dabei können auch Informationen wie 
z.B. die Laufzeit oder während des Trainings verbrauchter Speicher dargestellt 
werden. Auch die Tweets und ihre Klassen lassen sich in einem 
$\mathbb{R}^3$-Raum abbilden.
\index{Deep Learning Framework>TensorFlow>TensorBoard}
\index{Skalar}

\bigskip
Es gibt zwei unterschiedliche Möglichkeiten, wie TensorFlow einen Wert ablegen 
kann, sodass er von TensorBoard dargestellt wird: Zum einen kann mittels
\texttt{tf.""summary.""scalar} ein Wert innerhalb des TensorFlow-Graphen 
abgespeichert werden. Zum anderen kann eine Python-Variable, die nicht 
Bestandteil des TensorFlow-Graphen ist, abgespeichert werden und ist damit 
auch durch TensorBoard darstellbar.

\smallskip
Um beispielsweise einen einzelnen Wert innerhalb des TensorFlow-Graphen mittels 
TensorBoard darzustellen, wird der Wert über \texttt{tf.summary.scalar} mit 
einem Namen versehen. Nachdem sich so eine Menge von Werten gesammelt hat, muss  
mittels \texttt{tf.summary.merge} eine Zusammenfassung generiert werden. Um 
diese abzuspeichern, wird ein \texttt{tf.summary.FileWriter} erzeugt. Später 
wird die Zusammenfassung mittels \texttt{sess.run}, wie andere Knoten auch, 
evaluiert und vom FileWriter zusammen mit der aktuellen Epoche und einigen 
Metadaten abgespeichert. Python-Variablen, die kein Teil des Graphen sind, 
können dem FileWriter direkt hinzugefügt und so abgespeichert werden. 

\medskip
Obwohl das System die Mehrzahl der Metriken während des Testens ermittelt, 
werden einige auch beim Training erfasst. So z.B. die durchschnittliche Dauer 
einer Epoche, die Trainingsgenauigkeit und das aktuelle Ergebnis der 
Fehlerfunktion. Um in regelmäßigen Abständen die ermittelten Werte 
abzuspeichern, sollte der FileWriter via \texttt{flush} geleert werden.

\medskip
Da nur der Analysator getestet wird, wird an dieser Stelle nicht mehr zwischen 
dem Training des gesamten \ac{DAN}-Systems und dem des einzelnen Analysators  
differenziert. Das Abspeichern des aktuellen Modells ist der erste Schritt 
eines Testlaufes. Wieder mittels \texttt{sess.run} werden die Testdaten 
klassifiziert und die Wahrheitsmatrix berechnet. Mit diesen Ergebnissen können 
nun die Metriken außerhalb des TensorFlow-Graphen berechnet und anschließend 
durch TensorBoard erfasst werden.

\bigskip
Um die Metriken zu generieren, bieten sich drei Möglichkeiten, mit jeweils 
anderen Nachteilen, an. Zunächst bietet TensorFlow unter 
\texttt{tf.contrib.metrics} eine ganze Reihe von Metriken und Maßen an, wobei 
spezialisierte Metriken wie z.B. Krippendorf's alpha fehlen. Weiterhin 
berechnen diese Metriken den Durchschnitt über alle bisherigen Epochen. Um dies 
zu umgehen, lässt sich zwar der Graph neu initialisieren, aber dies führt zu 
anderen Problemen \QuoteIndirectNoPage{TF:MetricsReset}. Darüber hinaus stimmen 
die Ergebnisse der Berechnungen nicht mit denen von Sklearn überein.

\medskip
Die nächste Möglichkeit besteht darin, die Metriken unter Zuhilfenahme einiger 
TensorFlow-Funktionen selbst zu implementieren
\QuoteIndirectNoPage{TF:MetricsSelf}. Ähnlich wie schon in \autoref{sec:Basis} 
dargestellt, ist dies zwar möglich, aber aufwendig und fehleranfällig, zumal in 
dieser Arbeit mehr als zwei Klassen genutzt werden.

\medskip
Die letzte und in dem \ac{DAN}-System umgesetzte Lösung besteht darin, weitere 
Bibliotheken, in diesem Fall Sklearn und \ac{NLTK}, zu nutzen. Bei Sklearn 
handelt es sich um eine allgemeine \ac{ML}-Bibliothek für Python, während 
\ac{NLTK} weitgehend auf \ac{NLP} spezialisiert ist. Dieses Vorgehen hat den 
Nachteil, dass die Berechnungen nicht Bestandteil des TensorFlow-Graphen sind. 
So werden die Klassen zunächst innerhalb des Graphen ermittelt, anschließend 
werden mittels Sklearn die Metriken berechnet und in den FileWriter exportiert.

\medskip
\ac{NLTK} bietet die Möglichkeit, Präzision, Trefferquote und das F-Maß für 
jede Klasse einzeln zu berechnen und darzustellen. Die Berechnung der Metriken 
für die einzelnen Klassen wird teilweise aus der Arbeit von Flender et~al. 
übernommen \QuoteIndirectNoPage{DBLP:conf/lwa/FlenderG17}. Für eine 
Zusammenfassung über alle Klassen wird Sklearn genutzt, dabei gibt es 
unterschiedliche Möglichkeiten, den Durchschnitt zu berechnen. Die erste 
Möglichkeit wird als \texttt{macro} bezeichnet und berechnet den ungewichteten 
Durchschnitt, als zweite Möglichkeit wird \texttt{weighted} eingesetzt. Hierbei 
wird der Durchschnitt mit der Häufigkeit der Klassen gewichtet und so die 
unausgewogene Häufigkeit der Klassen berücksichtigt 
\QuoteIndirectNoPage{sklearn:metrics}.
\index{NLTK}
\index{Sklearn}
\index{Sklearn>macro}
\index{Sklearn>weighted}

\bigskip
Da keine dieser Bibliotheken Krippendorf's alpha als eigenständige Metrik 
anbietet, muss auf die Implementierung von Grill zurückgegriffen werden
\QuoteIndirectNoPage{Grrr:Alpha}. Genutzt werden, wie schon in 
\autoref{subsubsec:alpha} beschrieben, sowohl Intervallklassen als auch 
nominale Klassen. Dies ist möglich, da die Berechnung einer weiteren Metrik nur 
wenig Ressourcen beansprucht.
