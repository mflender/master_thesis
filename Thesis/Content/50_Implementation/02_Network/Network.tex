\label{sec:Network}

Bei der Beschreibung des \ac{DAN}-Systems in \autoref{sec:DAN} wurde nicht auf 
den konkreten Aufbau der im Analysator und Diskriminator genutzten \acp{ANN} 
eingegangen, da dieser an die Aufgabenstellung angepasst sein muss und somit 
von der Implementierung abhängt. Im Folgenden soll diese Lücke gefüllt werden. 
Dazu werden auch einige Sonderformen von \acp{ANN} beschrieben.

\bigskip
Das einzelne in \autoref{sec:GAN} \QuoteM{\nameref{sec:GAN}} auf 
\autopageref{sec:GAN} beschriebene künstliche Neuron lässt sich mit anderen 
Neuronen zu unterschiedlichen Strukturen zusammenfügen. Wird ein Neuron einer 
Schicht mit jedem Neuron der nächsten Schicht verknüpft und gibt es mindestens 
eine Schicht (auf Englisch \QuoteM{Hidden Layer}) zwischen Ein- und 
Ausgabeschicht, handelt es sich um ein \ac{MLP} \QuoteIndirect{aima}
{S. 850 ff.}. In dieser Arbeit wird oftmals eine einzelne Schicht eines 
\acp{MLP} eingesetzt, diese wird als \ac{FCL} bezeichnet.
\index{Artificial neural network>Hidden Layer}
\index{Artificial neural network>Multi-layer perceptron}
\index{Artificial neural network>Fully-connected Layer}

\medskip
Ein Bild besitzt keine intrinsische Zeitachse, es stellt einen Moment ohne 
seine Vorgeschichte dar. Hingegen erschließt sich die Bedeutung eines Wortes in 
einem Text oft nur durch das Verständnis seiner Vorgänger. Somit besitzt ein 
Text eine Zeitachse. Die weiter oben beschriebenen \acp{MLP} besitzen keine 
Möglichkeit, solche historischen Abhängigkeiten zu modellieren. Um dieses 
Problem zu lösen, hat sich  das \ac{RNN} etabliert 
\QuoteIndirectNoPage{Olah:LSTM}.
\index{Artificial neural network>Recurrent neural network}

\medskip
Im Gegensatz zu einem \ac{MLP}, bei dem die Ausgänge jeder Schicht nur in die 
nachfolgende(n) Schicht(en) geleitet werden, erlaubt ein \ac{RNN} auch, dass 
Daten wieder in vorherige Schichten zurückfließen. Einfachere \acp{RNN} eignen 
sich gut für Daten, bei denen die Ausgabe nur von der direkten Vergangenheit 
abhängig ist. Wird ein größerer Kontext zur Lösung des Problems benötigt, 
eignen sich solche einfachen \acp{RNN} weniger. Um dieses Problem zu lösen, 
wurden von Hochreiter und Schmidhuber im Jahre 1997 \ac{LSTM}-Netzwerke 
eingeführt \QuoteIndirectNoPage{Hochreiter:1997:LSM:1246443.1246450}, die eine 
Unterkategorie der \acp{RNN} darstellen. Bei einem \ac{LSTM}-Netzwerk handelt 
es sich um eine Kombination von vier einzelnen \acp{ANN}, die so miteinander 
verbunden werden, dass es möglich ist, ältere Daten gezielt zu filtern und mit 
neuen Informationen anzureichern. Für eine tiefgreifende Beschreibung dieser 
\ac{LSTM}-Netze sei auf die Arbeiten von Olah und Hochreiter et~al. verwiesen
\QuoteIndirectNoPage{Hochreiter:1997:LSM:1246443.1246450, Olah:LSTM}.
Da das \ac{DAN}-System zur Analyse von Texten gedacht, ist werden an einigen 
Stellen solche \ac{LSTM}-Zellen eingesetzt.
\index{Artificial neural network>Long short-term memory}

\subsection{Analysator}
\label{subsec:Analysator}

Die folgende Beschreibung bezieht sich auf den Inhalt der Datei 
\texttt{analyzer\_LSTM.py}. \autoref{fig:Analysator} stellt die Topologie des 
Analysator-Netzwerkes schematisch dar, damit ergänzt sie eine der grau 
hinterlegten Flächen in \autoref{fig:DAN} auf \autopageref{fig:DAN}.

\bigskip
Aufgabe des Analysators ist die eigentliche Klassifikation des Tweets, somit 
ist der Tweet, in Form des in \autoref{sec:InputEncoding} beschriebenen 
Merkmalsvektors, die Eingabe dieses \acp{ANN}. Als Ausgabe wird eine der drei 
Klassen in der bereits beschriebenen One-Hot-Kodierung erwartet.

\begin{figure}[htb]
	\centering
	\includegraphics[scale=0.65] %0.35
	{Content/50_Implementation/02_Network/figures/Analysator_horizontal.pdf}
	\caption{Schematischer Aufbau des Analysators}
	\label{fig:Analysator}
\end{figure}

\medskip
Da, wie in \autoref{sec:InputEncoding} beschrieben, das \ac{DAN}-System zwei 
unterschiedliche Merkmalsvektoren nutzt, aber die \ac{LSTM}-Implementierung von 
TensorFlow nur mit 3\ac{D}-Tensoren arbeiten kann, müssen beide Eingangswerte 
erst konvertiert werden. Dazu werden in diesem Fall die Merkmalsvektoren 
entweder in einen Tensor der Form $[\text{batchSize},\allowbreak 
\text{MaxWords},\allowbreak \text{MaxChar} \cdot \text{Alphabet}]$ oder 
$[\text{batch""Size}, \text{MaxWords}, 1]$ umgewandelt.

\medskip
Nachdem der Merkmalsvektor in einer Form vorliegt, die von den \ac{LSTM}-Zellen 
verarbeitbar ist, können nun die einzelnen Zellen erzeugt werden. TensorFlow 
bietet dazu zwei unterschiedliche Implementierungen an: Zum einen die 
ursprüngliche Version von Hochreiter et~al. 
\QuoteIndirectNoPage{Hochreiter:1997:LSM:1246443.1246450} und zum anderen eine 
Version mit Guckloch (auf Englisch \QuoteM{Peephole}) von Sak et~al.
\QuoteIndirectNoPage{DBLP:journals/corr/SakSB14}, wobei an dieser Stelle zwei 
Zellen der Implementierung mit Guckloch zum Einsatz kommen. Jede Zelle 
beinhaltet 500 künstliche Neuronen, als Aktivierungsfunktion der Zellen wird 
der Tangens hyperbolicus genutzt.
\index{Artificial neural network>Long short-term memory}

\medskip
Mittels \texttt{tf.nn.rnn\_cell.MultiRNNCell([cell\_1, cell\_2])} lassen sich 
beide Zellen zusammenfassen. Anschließend wird dieser Zusammenschluss durch 
\texttt{tf.""nn.""rnn\_""cell.""DropoutWrapper} erweitert. Dabei werden 60~\% 
der Neuronen behalten. 

\medskip
Zur Vermeidung von Überanpassungen haben sich einige unterschiedliche 
Möglichkeiten etabliert. Ein Verfahren, dies zu erreichen, wird als Dropout 
bezeichnet. Dabei wird ein vorgegebener Anteil (in diesem Fall 40~\%) an 
künstlichen Neuronen in einem \ac{ANN} für einen Trainingsdurchlauf 
deaktiviert. Die konkreten Neuronen werden zufällig ausgewählt. Somit wird 
verhindert, dass sich das Netzwerk auf einzelne Neuronen stützt und dadurch 
kann einer Überanpassung entgegengewirkt werden 
\QuoteIndirectNoPage{JMLR:v15:srivastava14a}.
\index{Dropout}

\medskip
Um die Daten in den finalen \ac{FCL} leiten zu können, muss die Form des 
Tensors mittels 
\texttt{tf.reshape(val[:, -1, :], [-1, number\_""nodes\_""layer\_2])} erneut 
angepasst werden \QuoteIndirectNoPage{yunjey:LSTM}. Die $-1$ stellt hier einen 
Platzhalter dar, bei dem die Größe des Ausgangstensors so angepasst wird, dass
sie der Größe des Eingangstensors entspricht \QuoteIndirectNoPage{TF:reshape}. 
Diese letzte Schicht besitzt zunächst keine Aktivierungsfunktion, da diese vom 
genutzten Trainingsmodus abhängt. Beim einzeln trainierten Analysator wird 
\texttt{tf.nn.softmax\_""cross\_""en\-tro\-py\_""with\_""logits} zur 
Optimierung genutzt. Im \ac{DAN}-Modus kommt die \texttt{tf.nn.soft\-max} 
Aktivierungsfunktion zum Einsatz.

\subsection{Diskriminator}
\label{subsec:Diskriminator}

Aufgabe des Diskriminators ist es, anhand des gegebenen Tweets und der Klasse 
herauszufinden, ob die Klassifikation vom Analysator oder dem zweiten Korpus 
stammt. Die dazu genutzte Ausgabe besteht aus einer Zahl zwischen 0 und 1, 
welche als Wahrscheinlichkeit interpretiert wird.

\medskip
Die hier beschriebenen Abläufe finden sich in der Datei 
\texttt{discriminator\_LSTM.py} wieder. Einige der dort programmierten 
Verfahren, wie z.B. die Konvertierung des Merkmalsvektors, wurden schon in 
\autoref{subsec:Analysator} beschrieben, sodass an dieser Stelle nicht weiter 
darauf eingegangen wird. Hier vervollständigt \autoref{fig:Diskriminator} die 
andere grau hinterlegte Fläche aus \autoref{fig:DAN}.

\begin{figure}[htb]
	\centering
	\includegraphics[width=0.99\textwidth]
	{Content/50_Implementation/02_Network/figures/Diskriminator_horizontal_2.pdf}
	\caption{Schematischer Aufbau des Diskriminators}
	\label{fig:Diskriminator}
\end{figure}

\bigskip
Der Strang, der die Tweets verarbeitet, entspricht dem Analysator bis auf den 
Umstand, dass im Diskriminator die \ac{LSTM}-Zellen ohne Guckloch zum Einsatz 
kommen. Der Strang, der die Klassen verarbeitet, besteht aus zwei \acp{FCL}, 
jeweils mit Dropout, wobei die Neuronen mit einer 100-prozentigen 
Wahrscheinlichkeit beibehalten werden, somit ist der Dropout an dieser Stelle 
deaktiviert. Beide \acp{FCL} nutzen \texttt{tf.nn.relu} als 
Aktivierungsfunktion und besitzen 500 Neuronen. Zwischen beiden \acp{FCL} 
befindet sich eine \ac{BN}-Ebene.

\smallskip
Während des Trainings verändern sich ständig die Gewichte. Dadurch muss sich 
eine weiter hinten liegende Schicht permanent an seine Vorgänger anpassen. Um 
diese zeitaufwändige Adaption an die Vorgängerschicht zu vermeiden, hat sich 
die \ac{BN} etabliert. Bei diesem Verfahren werden alle Ausgänge einer Schicht 
auf den Mittelwert 0 bei eine Standardabweichung von 1 normalisiert 
\QuoteIndirectNoPage{r2rt:BN}. Dadurch müssen sich die späteren Schichten nicht 
an die schwankenden Eingaben anpassen. Für eine genauere Beschreibung des 
Verfahrens sei auf die Arbeiten von Ioffe et~al. verwiesen 
\QuoteIndirectNoPage{DBLP:journals/corr/IoffeS15}. Da TensorFlow die Funktionen 
\texttt{tf.nn.batch\_normalization} und \texttt{tf.nn.moments} bereitstellt, 
ist die Implementierung dieses Verfahrens sehr einfach.
\index{Batch Normalization}

\medskip
Durch eine einfache Addition werden beide Stränge miteinander verbunden. 
Anschließend werden die zusammengeführten Daten durch eine \ac{MB}-Schicht 
geleitet. 

\smallskip
Um dem in \autoref{sec:GAN} beschriebenen Problem der engen Verteilung der 
Ausgabewerte zu begegnen, hat sich unter anderem das \ac{MB}-Verfahren 
etabliert. Diese enge Verteilung ergibt sich teilweise dadurch, dass der 
Diskriminator jeden Tweet mit seiner Klasse unabhängig von allen anderen 
betrachtet. Um dieses Problem zu lösen, kann dem Diskriminator mehr als ein 
Beispiel gleichzeitig gezeigt werden 
\QuoteIndirect{DBLP:journals/corr/SalimansGZCRC16}{S. 3}. Das \ac{MB}-Verfahren 
versucht dem Diskriminator zusätzlich zu dem Tweet und seiner Klasse einen 
weiteren Wert zu zeigen, der den Abstand zwischen dem konkreten Beispiel und 
allen anderen Tweets aus diesem Batch darstellt 
\QuoteIndirectNoPage{GAN:aylienIntro}. Dadurch kann der Diskriminator auch 
Abweichungen zwischen dem konkreten Fall und dem Durchschnitt erkennen. So 
lässt sich die Verteilung der Ausgabewerte besser streuen. Eine fertige 
Funktion für dieses Verfahren konnte nicht in TensorFlow gefunden werden. In 
dieser Arbeit wurde deswegen die Implementierung von Glover übernommen 
\QuoteIndirectNoPage{GAN:Minibatch}.
\index{Minibatch discrimination}

\bigskip
Der \ac{MB}-Schicht folgt eine weitere vollständige Schicht inklusive 
(deaktiviertem) Dropout. Auch diese Schicht wird normalisiert. Abschließend 
folgt wieder ein \ac{FCL}, der jedoch \texttt{tf.nn.sigmoid} als 
Aktivierungsfunktion nutzt. Dies ist notwendig, damit sich der Ausgabewert des 
Diskriminators zwischen 0 und 1 bewegt \QuoteIndirectNoPage{tf:sigmoid}.
Alle anderen \acp{FCL} nutzen \texttt{tf.nn.relu} als Aktivierungsfunktion. 
