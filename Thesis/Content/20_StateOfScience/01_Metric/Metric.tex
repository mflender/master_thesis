\label{sec:Metriken}

In vielen der in den folgenden Abschnitten beschriebenen Arbeiten wird 
hauptsächlich die Genauigkeit (im Englischen \QuoteM{accuracy}) genutzt.
Dabei ist die Genauigkeit definiert als der Quotient aus richtig 
klassifizierten Fällen und allen Fällen in Prozent. In diesem Unterabschnitt 
soll gezeigt werden, dass diese Metrik zumeist nur unzureichende Aussagen über 
die Qualität der Verfahren zulässt. Weiterhin werden andere übliche Metriken 
vorgestellt. Eine aussagekräftigere Alternative zur Genauigkeit wird in 
\autoref{subsec:GenutzteMetriken} 
\QuoteM{\nameref{subsec:GenutzteMetriken}} auf 
\autopageref{subsec:GenutzteMetriken} vorgestellt.
\index{Genauigkeit>Accuracy} \index{Metrik}

\begin{itemize}
	\setlength\itemsep{\Itemizespace}
	
	\item Da sich die Genauigkeit direkt von dem genutzten Korpus ableitet, 	
	können nur Genauigkeiten von Ergebnissen verglichen werden, die 
	unterschiedliche Verfahren auf dem gleichen Korpus erzielt haben.
	
	\item Die Aufgabe eines fertig trainierten Klassifikators ist es, 
	unbekannte Daten in der realen Welt zu klassifizieren. Die Verteilung 
	dieser Klassen ist nur im Korpus, nicht aber in der Realität bekannt. 
	Weiterhin kann aus der Verteilung der Klassen im Korpus nicht generell die 
	Verteilung in der Realität gefolgert werden, da der Korpus möglicherweise 
	normalisiert wurde oder sich die Verteilung im Lauf der Zeit geändert hat. 
	Jedoch ist ohne dieses Wissen die Angabe einer Genauigkeit irreführend, da 
	möglicherweise ein Klassifikator, der immer die häufigste Klasse wählt 
	(ZeroR), schon extrem hohe Genauigkeiten erzielt 
	\QuoteIndirectNoPage{Provost:1998:CAA:645527.657469}.
	\index{ZeroR}
	
	\item Die Klassifikationsgenauigkeit gewichtet falsch positive und falsch 
	negative Fehler gleich, was in vielen Anwendungen weitreichende Folgen hat, 
	beispielsweise ist eine nicht erkannte Krankheit wesentlich schwerwiegender 
	als eine erkannte Krankheit an einem gesunden Patienten. Auch wenn die 
	Tragweite eines Fehlers bei der \ac{SA} wesentlich geringer ist, so bleibt 
	die grundsätzliche Schwäche der Genauigkeit dennoch erhalten
	\QuoteIndirectNoPage{Provost:1998:CAA:645527.657469}.
	
	\item Werden mehr als nur zwei Klassen genutzt, kommt ein weiteres Problem 
	hinzu: Wie Mozeti{\v{c}} et~al. zeigen konnten, werden die drei in dieser 
	Arbeit genutzten Klassen als geordnet (\QuoteM{Negativ} $\prec$ 
	\QuoteM{Neutral} $\prec$ \QuoteM{Positiv}) empfunden 
	\QuoteIndirect{DBLP:journals/corr/MozeticGS16}{S. 19 f.}. Dieser Umstand 
	wird von der Genauigkeit nicht erkannt, sie betrachtet die drei Klassen als 
	ungeordnet \QuoteIndirect{DBLP:journals/corr/MozeticGS16}{S. 18}.
\end{itemize}

Weitere Metriken, die in vielen der nachfolgenden Arbeiten genutzt werden, sind 
die Präzision (auf Englisch \QuoteM{Precision}), die Trefferquote (auf 
Englisch \QuoteM{Recall}) und das aus ihnen berechnete F-Maß (auf 
Englisch \QuoteM{F-measure} oder \QuoteM{F$_1$ score}). Alle drei Metriken 
werden zunächst für jede Klasse einzeln berechnet, anschließend kann ein 
Durchschnitt ermittelt werden. Dabei beschreibt die Präzision den Quotienten 
aus den richtig klassifizierten und allen klassifizierten Fällen. Die 
Trefferquote stellt das Verhältnis aus richtig klassifizierten Fällen und allen 
Fällen der gleichen tatsächlichen Klasse dar
\QuoteIndirectNoPage{perry1955machine}.
\index{Trefferquote>Recall}
\index{Präzision>Precision}

\medskip
Für mehr als zwei Klassen lassen sich diese Maße am besten anhand der 
Wahrheitsmatrix (auf Englisch \QuoteM{Confusion Matrix}) beschreiben. Für eine 
Klassifikationsaufgabe mit $n$ Klassen hat solch eine Matrix die Form 
$n \times n$, wobei in einer Dimension (im folgenden Spalte) die Anzahl der 
tatsächlichen und in der anderen (im folgenden Zeile) die Anzahl der 
vorhergesagten Klassen dargestellt werden 
\QuoteIndirectNoPage{powers2007evaluation}.
\index{Wahrheitsmatrix>Confusion Matrix}

\smallskip
Die Präzision wird gebildet, indem die Anzahl der richtig klassifizierten 
Fälle (immer auf der Hauptdiagonale der Matrix) durch die Summe der 
betreffenden Zeile geteilt werden. Bei der Trefferquote wird die Anzahl der 
richtig klassifizierten Fälle durch die Summe der betreffenden Spalte geteilt 
\QuoteIndirectNoPage{PrecisionRecall}. 

\medskip
Das harmonische Mittel aus Präzision und Trefferquote bilden das F-Maß
\QuoteIndirect{powers2007evaluation}{S. 4, Gl. 12}:

\begin{equation}
F_1 = 2 \cdot \frac{\text{precision} \cdot \text{recall}}{\text{precision} + 
\text{recall}}
\end{equation}

Das F-Maß beachtet, anders als die Genauigkeit, die Ordnung der Klassen
\QuoteIndirect{DBLP:journals/corr/MozeticGS16}{S. 13}.
\index{F-Maß>F-measure}
