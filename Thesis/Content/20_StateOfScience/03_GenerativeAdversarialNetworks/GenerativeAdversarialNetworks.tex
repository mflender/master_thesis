\label{sec:GAN}

Die Aufgabe der 2014 in der Arbeit von Goodfellow et~al. 
\QuoteIndirectNoPage{goodfellow2014generative} eingeführten \acp{GAN} ist es, 
ein Objekt (z.B. Bilder \QuoteIndirectNoPage{GAN:Face, 
DBLP:journals/corr/RadfordMC15} oder Videos \QuoteIndirectNoPage{
DBLP:journals/corr/LotterKC15, DBLP:journals/corr/VondrickPT16}) zu generieren. 
\acp{GAN} bestehen aus zwei \acp{ANN}, die nach dem Prinzip eines 
Zwei-Spieler-Min-Max-Spiels gegeneinander arbeiten, wobei ein Netzwerk Objekte 
generiert und das andere versucht zu entscheiden, ob das Objekt aus der 
Realität oder vom ersten Netzwerk stammt. Obwohl das Thema \acp{GAN} noch sehr 
jung ist, gibt es bereits eine große Anzahl an diesbezüglichen 
Veröffentlichungen 
\QuoteIndirectNoPage{GAN:List3, GAN:List2, GAN:List1, GAN:List4}.

\medskip
Innerhalb des \ac{ML} gibt es ein anderes Forschungsfeld, das einen ähnlichen 
Namen trägt: Das Training mittels \QuoteM{Adversarial Examples}, welches auch 
als \QuoteM{Adversarial Training} bezeichnet wird. 
\QuoteIndirectNoPage{GAN:AdversarialTraining}. Ziel dieses Training ist es, 
\acp{ANN} gegen manipulierte Eingangsdaten zu härten 
\QuoteIndirectNoPage{DBLP:journals/corr/SzegedyZSBEGF13}. Zusätzlich dazu wird 
das Training von \acp{GAN} in manchen Veröffentlichungen auch als 
\QuoteM{Adversarial Training} bezeichnet. Somit wird ein Begriff für zwei 
unterschiedliche Forschungsfelder genutzt. Diese Arbeit beschäftigt sich mit 
\acp{GAN} und nicht mit \QuoteM{Adversarial Examples}.
\index{Adversarial Example}

\medskip
Um ein \ac{GAN} besser zu verstehen, soll zunächst das \ac{ANN} kurz erläutert 
werden: Ein künstliches neuronales Netz (auf Englisch \QuoteM{\acl{ANN}}) 
versucht die in der Biologie und Neurologie gewonnenen Erkenntnisse über die 
Funktion von Nervenzellen (Neuronen) digital nachzubilden, um so stärkere 
maschinelle Lerner zu erzeugen. Die ersten Ideen in dieser Richtung wurden in 
den 1940ern veröffentlicht \QuoteIndirectNoPage{McCulloch1943}. Da ein 
einzelnes künstliches Neuron nicht ausreicht, um komplexe Probleme zu lösen, 
müssen viele Neuronen vernetzt werden. Der Aufbau von Netzwerken, die groß 
genug sind, um \ac{ML}-Probleme lösen zu können, verbraucht immense 
Rechenkapazitäten. Hinzu kommt, dass solche Netzwerke auch große Mengen an 
Trainingsdaten benötigen. Beide Voraussetzungen können erst seit wenigen Jahren 
erfüllt werden.
\index{Artificial neural network}

\begin{figure}[htb]
	\centering
	\includegraphics[scale=0.53]
	{Content/20_StateOfScience/03_GenerativeAdversarialNetworks/figures/ANN.eps}
	\caption[Schematischer Aufbau eines künstlichen Neurons]
	{Schematischer Aufbau eines künstlichen Neurons, in Anlehnung an: 
	\cite[S. 846, Abbildung 18.19]{aima}}
	\label{fig:ANN}
\end{figure}

Ein einzelnes künstliches Neuron kann in drei Ebenen unterteilt werden:

\smallskip
Im ersten Schritt werden die Eingänge aufsummiert. Ein Eingang besteht aus zwei 
Werten; dem Ausgangswert des Vorgängers $x_i$ (in der ersten Schicht kommen die 
Werte aus den Trainingsdaten) und einem Gewicht $w_i$, beide werden 
multipliziert: $w_i \cdot x_i$. Darüber hinaus gibt es einen Eingang, der nur 
ein Gewicht $b$ oder $w_0$ (auf Englisch \QuoteM{Bias}) besitzt: 
$\sum_{i=1}^{n} w_i \cdot x_i + b$. Üblicherweise werden die Gewichte und der 
Bias mit Zufallszahlen zwischen 0 und 1 initialisiert.

\smallskip
Diese Summe wird in der zweiten Ebene als Eingabe einer Aktivierungsfunktion 
$g(x)$ genutzt $g (\sum_{i=1}^{n} w_i \cdot x_i + b)$. Einige übliche 
Aktivierungsfunktionen sind:
\index{Aktivierungsfunktion}

\begin{description}
	\setlength\itemsep{\Itemizespace}
	
	\item[Tangens hyperbolicus:] $g(x) = \tanh(x)$
	
	\item[\ac{ReLU}:] $g(x)=max(0,x)$
	\QuoteIndirectNoPage{Nair:2010:RLU:3104322.3104425}
	
	\item[Softplus:] $g(x)=\log(1+e^x)$ 
	\QuoteIndirectNoPage{pmlr-v15-glorot11a} 
	
	\item[Sigmoid:] $g(x)=\frac{1}{1+ e^{-x}}$ 
	\QuoteIndirectNoPage{tf:sigmoid}
\end{description}	

Die letzte Ebene gibt den Ausgang des künstlichen Neurons an seine Nachfolger 
weiter, \autoref{fig:ANN} stellt diesen Ablauf grafisch dar. Durch Anwendung 
der Fehlerrückführung (auf Englisch \QuoteM{Backpropagation}) und des 
Gradientenverfahrens lassen sich die Gewichte $w_i$ und der Bias $b$ auf die 
Trainingsdaten optimieren und damit die Fehlerfunktion (auf Englisch 
\QuoteM{Loss}) minimieren \QuoteIndirect{aima}{S. 845 bis 856}. 
\index{Artificial neural network>Neuronen}
\index{Fehlerfunktion>Loss}
\index{Fehlerrückführung>Backpropagation}
\index{Gradientenverfahren}

\bigskip
Die Struktur der innerhalb des \ac{GAN} genutzten \acp{ANN} ist nahezu frei 
wählbar und hängt vom konkreten Anwendungsfall des \acp{GAN} ab, dadurch lassen 
sich \acp{GAN} an viele unterschiedliche Generierungsaufgaben anpassen, 
beispielsweise gibt es Versuche, durch den Einsatz von \ac{RNN} innerhalb von 
\acp{GAN} Musik zu erzeugen \QuoteIndirectNoPage{DBLP:journals/corr/Mogren16}. 
Aus diesem Grund kann hier nur der generelle Aufbau besprochen werden.

\begin{figure}[htb]
	\centering
	\includegraphics[scale=0.53]
	{Content/20_StateOfScience/03_GenerativeAdversarialNetworks/figures/GAN.pdf}
	\caption[Schematischer Aufbau eines \acp{GAN}]
	{Schematischer Aufbau eines \acp{GAN}, in Anlehnung an: \cite{GAN:image}}
	\label{fig:GAN}
\end{figure}

\medskip
\autoref{fig:GAN} stellt den Aufbau eines \acp{GAN} schematisch dar, dabei 
werden die \acp{ANN} durch die grau hinterlegten Flächen symbolisiert. Aus 
einem zufälligen Rauschen $z$ erstellt der Generator zunächst Daten, die wenig 
Bezug zur Realität haben. Zu Beginn erkennt der Diskriminator den Unterschied 
zwischen Daten aus der Realität $x$ und den Daten vom Generator $G(z)$ nicht, 
seine Ausgabe (die Entscheidungsgrenze) liegt bei ca. 0,5. Über die folgenden 
Trainingsepochen erkennt der Diskriminator Unterschiede zwischen echten und 
gefälschten Daten. Die Entscheidungsgrenze verschiebt sich für jeden Datensatz 
in die entsprechende Richtung. Dadurch wird der Generator darauf trainiert, 
realistischere Daten zu erzeugen. So nähern sich mit jeder Trainingsepoche die 
generierten Daten den realen an. Die Fehlerfunktionen, deren Minimierung Ziel 
der beiden \acp{ANN} ist, kann durch  $\text{Loss}_d = -\log(D(x)) - \log(1 - 
D(G(z)))$ für den Diskriminator und $\text{Loss}_g = -\log(D(G(z)))$ für den 
Generator dargestellt werden \QuoteIndirectNoPage{goodfellow2014generative}. Da 
bekannt ist, ob die vom Diskriminator untersuchten Daten aus der Realität oder 
dem Generator stammen, handelt es sich bei \acp{GAN} um ein unüberwachtes 
System.
\index{Generative Adversarial Net}
\index{Entscheidungsgrenze}

\medskip
Die Generierung von Texten unter Zuhilfenahme von \acp{GAN} ist nicht in der 
gleichen Art wie bei Bildern oder Videos möglich. Ein Bild, Musikstück oder 
Video besteht aus quasi kontinuierlichen Zahlen, die auch kleine Änderungen 
zulassen. Z.B. kann ein Pixel in einem generierten Bild den Wert $(128, 128, 
128)$ annehmen, nach einer Trainingsepoche kann sich dieser Wert zu $(127, 125, 
129)$ geändert haben. Texte lassen sich nicht ohne Weiteres durch solche 
kontinuierliche, geordnete Zahlen abbilden, z.B. gibt es kaum eine sinnvolle 
Möglichkeit, das Wort \QuoteM{Größe} durch \QuoteM{Ausdehnung} zu ersetzen, um 
den Sprachstil zu ändern \QuoteIndirectNoPage{DBLP:journals/corr/BergmannJV17, 
GAN:text, GAN:NLP}. 

\smallskip
Vektorraum-Retrieval (auf Englisch \ac{VSM}), angewendet auf Wörter und ihre 
Bestandteile, stellt eine Möglichkeit dar, auch Texte durch kontinuierliche 
Zahlen abzubilden. Dabei werden die Wörter durch ihre Positionen in einem 
kontinuierlichen Vektorraum dargestellt. Semantisch ähnliche Begriffe stehen in 
diesem Raum näher zusammen als semantisch fremde Begriffe. Word2Vec aus der 
Arbeit von Mikolov et~al. stellt ein solches Verfahren dar 
\QuoteIndirectNoPage{Mikolov:2013:DRW:2999792.2999959}.
\index{Vector Space Model}
\index{Vector Space Model>Word2Vec}
\index{Generative Adversarial Net>Text}

\medskip
Durch ihre komplexe Struktur sind \acp{GAN} schwierig zu trainieren, in manchen 
Fällen tritt keine Optimierung der generierten Daten ein, das System 
konvergiert nicht.

\smallskip
Eines der häufigsten Problem beim Training von \acp{GAN} ist die zu enge 
Verteilung der Ausgabewerte. Wird z.B. versucht, eine Gaußsche Normalverteilung 
zu generieren, so erzeugt der Generator nur Werte dicht am Maximum, während 
weiter entfernte Werte nie generiert werden
\QuoteIndirectNoPage{GAN:aylienIntro}. Da dieses Problem bekannt ist, gibt es 
einige Ansätze, das System auf eine breitere Verteilung der Ausgabewerte zu 
optimieren. Salimans, Goodfellow et~al. haben in ihrer Arbeit aus dem Jahre 
2016 fünf Möglichkeiten vorgestellt, die zum einen das oben beschriebene 
Problem adressieren und zum anderen das Training von \acp{GAN} stabilisieren 
sollen \QuoteIndirectNoPage{DBLP:journals/corr/SalimansGZCRC16}:
\index{Generative Adversarial Net>Training}

\begin{itemize}
	\setlength\itemsep{\Itemizespace}
	\item Feature matching
	\item Minibatch discrimination
	\item Historical averaging
	\item One-sided label smoothing
	\item Virtual batch normalization
\end{itemize}

In \autoref{cha:Implementation} \QuoteM{\nameref{cha:Implementation}} auf 
\autopageref{cha:Implementation} werden einige dieser Verfahren genauer 
beschrieben. Zusätzlich dazu gibt es andere Veröffentlichungen, die sich auch 
mit einem optimiertem Training von \acp{GAN} auseinandersetzen
\QuoteIndirectNoPage{GAN:Train3, GAN:Train, GAN:InstanceNoise, GAN:Train2}.
