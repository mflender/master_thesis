\label{sec:SA}

Wie schon in \autoref{sec:Schwächen} \QuoteM{\nameref{sec:Schwächen}} auf 
\autopageref{sec:Schwächen} gezeigt, stellt die \ac{SA} ein gut untersuchtes 
Feld des \ac{NLP} dar. Im Folgenden sollen bestehende Ansätze und Verfahren zur 
\ac{SA} kategorisiert werden, um so einen Überblick über den Stand der 
Forschung zu vermitteln. Aufgrund des Umfangs dieses Themas können nur 
Beispiele gezeigt werden. Die gesamte Bandbreite an bekannten Verfahren kann 
keinesfalls abgedeckt werden.

\medskip
Das vielfältige Thema \ac{SA} lässt sich nach zahlreichen verschiedenen 
Gesichtspunkten kategorisieren. Die in dieser Arbeit genutzte Kategorisierung 
der \ac{SA} wird in \autoref{fig:SAVerfahren} dargestellt und soll unter 
anderem im Folgenden genauer besprochen werden.

\begin{figure}[htb]
	\centering
	\includegraphics[scale=0.5]
	{Content/20_StateOfScience/02_SentimentAnalyse/figures/SA_Verfahren.pdf}
	\caption{Übersicht über die verschiedenen Kategorien der \ac{SA}}
	\label{fig:SAVerfahren}
\end{figure}

\subsection{Erste Versuche und Historie}
\label{subsec:Historie}

Auch wenn die ersten Ansätze einer automatisierten Inhalts-Analyse (im Original 
\QuoteM{content analysis}) bis in die 1960er Jahre zurückreichen 
\QuoteIndirectNoPage{Stone:1963:CAC:1461551.1461583}, wird das Thema \ac{SA} 
erst seit der Jahrtausendwende intensiv untersucht.

\smallskip
Die Arbeiten von Turney \QuoteIndirectNoPage{Turney:2002:TUT:1073083.1073153} 
und Pang et~al. \QuoteIndirectNoPage{pang2002thumbs} gehören zu den ersten, die 
sich direkt mit \ac{SA} beschäftigen. Beide stammen aus dem Jahre 2002, teilen 
die Empfindung in zwei Klassen ein und arbeiten auf Ebene der Dokumente. 
Während Turney sich mit Kundenrückmeldungen auseinandersetzt, nutzen Pang 
et~al. Filmkritiken als Ausgangsmaterial. Insbesondere der \QuoteM{naive} 
Ansatz von Pang et~al. ist erwähnenswert. Dort wurden zwei Listen mit Worten, 
die positive und negative Emotionen darstellen, von zwei Menschen 
zusammengestellt. Verglichen mit üblichen \ac{ML}-Verfahren, war die 
Klassifikationsqualität wesentlich schlechter (64~\% zu 82~\% Genauigkeit). In 
der Arbeit von Pang et~al. wurden \ac{NB} 
\QuoteIndirectNoPage{mccallum1998comparison}, \ac{ME} 
\QuoteIndirectNoPage{nigam1999using} und \ac{SVM} 
\QuoteIndirectNoPage{Cortes1995} als Klassifikatoren genutzt.

\medskip
\autoref{fig:GoogleTrendsSA} \QuoteM{\nameref{fig:GoogleTrendsSA}} auf 
\autopageref{fig:GoogleTrendsSA} zeigt, dass das Interesse und damit auch die 
Zahl der Veröffentlichungen an der \ac{SA} seit 2009 stark ansteigt.

\medskip
Eine weiterführende Übersicht über Veröffentlichungen aus dieser frühen Phase 
der \ac{SA} lassen sich in einer der Arbeiten von Pang und Lee finden
\QuoteIndirect{Pang:2008:OMS:1454711.1454712}{S. 4 f.}.

\subsection{Verschiedene Klassifikationen der Verfahren}
\label{subsec:Klassifikationen}

\autoref{fig:SAVerfahren} zeigt eine mögliche Klassifikation der bestehenden 
Ansätze zur \ac{SA}. Im Folgenden sollen einige dieser Klassen besprochen und 
mit Beispielen belegt werden. Dabei wird die wichtigste und am häufigsten
genutzte Einteilung nach Art des Verfahrens getroffen, dort sind 
vorwissensbasierte, statistische und hybride Verfahren zu unterscheiden
\QuoteIndirect{Dashtipour2016}{S. 2}. Jede dieser Klassen von Verfahren soll im 
Folgenden kurz beschrieben werden.

\subsubsection{Vorwissensbasierte Verfahren}
\label{subsubsec:VorwissensbasierteVerfahren}

In dieser Kategorie befinden sich Verfahren, die vom Menschen erzeugtes 
Vorwissen, i.d.R. in Form eines Lexikons oder Regelsystems, als Basis für die 
Klassifikation nutzen. Da bis auf wenige Ausnahmen (z.B. Emoticons 
\QuoteIndirectNoPage{DBLP:journals/corr/GoncalvesABC14}) in solchen Lexika 
sprachspezifische Begriffe enthalten sind, können mit dieser Art von Verfahren 
zunächst nur Analysen von Text in der Sprache des Lexikons durchgeführt werden. 
Aus diesem Umstand heraus gibt es Bemühungen, solche Lexika automatisiert zu 
generieren \QuoteIndirectNoPage{banea2008bootstrapping, 5349575}. Über die 
genutzte Sprache hinausgehend gibt es auch Versuche, Lexika für Spezialfälle, 
die einen anderen Sprachduktus nutzen (z.B. Twitter), zu generieren
\QuoteIndirectNoPage{DBLP:journals/corr/abs-1103-2903}. Auch im Bereich des 
Emotion Detection gibt es Verfahren, die Vorwissen in Form von Lexika nutzen 
\QuoteIndirectNoPage{aman2008using, FSS102216}. 
Gegenüber statistischen Verfahren haben die vorwissensbasierten Verfahren den 
Vorteil, dass sie ohne Training auskommen und damit einfacher einzusetzen 
sind und keine Trainingsdaten brauchen. Jedoch lassen sich diese Verfahren nur 
einsetzen, wenn ein zum Aufgabenfeld passendes Lexikon vorhanden ist.
\index{Emoticon}
\index{Emotionserkennung>Emotion Detection}

\medskip
Somit kann der in \autoref{subsec:Historie} beschriebene naive Ansatz von Pang 
et~al. dieser Klasse von Verfahren zugeordnet werden. Jedoch erkennt solch ein 
einfacher Ansatz keine Negationen (z.B. \QuoteM{nicht gut}) oder Steigerungen 
(z.B. \QuoteM{sogar besser als}). Dies lässt sich umgehen, wenn man die 
linguistische Struktur der zu analysierenden Sätze beachtet 
\QuoteIndirectNoPage{moilanen2007sentiment, 
Rajagopal:2013:GAC:2487788.2487995}.

\medskip
Bei \textsc{WordNet} \QuoteIndirectNoPage{Miller:1995:WLD:219717.219748} 
handelt es sich um eine Datenbank, in der Worte der englischen Sprache und ihre 
lexikalische Kategorie (z.B. Verb oder Nomen) abgespeichert sind. Wörter, die 
in einer semantischen Beziehung zueinander stehen wie z.B. Synonyme, Antonyme, 
Hyponyme (Unterbegriffe), Meronyme u.s.w. sind auch innerhalb der Datenbank 
miteinander verbunden. Basierend auf dieser Datenbank wurde von Baccianella 
et~al. im Jahre 2010 \textsc{SentiWordNet} entwickelt 
\QuoteIndirectNoPage{conf/lrec/BaccianellaES10}. Dabei wird das von 
\textsc{WordNet} gebildete Netzwerk traversiert und so schrittweise das Lexikon 
erstellt. \textsc{SentiWordNet} ist ein bekanntes Lexikon für englischsprachige 
\ac{SA}. Auch in anderen Arbeiten zur \ac{SA} wird \textsc{WordNet} als 
Ressource genutzt \QuoteIndirectNoPage{kamps2001words}.
\index{WordNet}
\index{SentiWordNet}
\index{lexikalische Kategorie}

\medskip
Während sich die oben aufgeführten Arbeiten mit englischsprachigen Texten 
auseinandersetzen, stellt \ac{SentiWS} ein Lexikon für die deutschsprachige 
\ac{SA} dar \QuoteIndirectNoPage{remus2010sentiws}. Dabei speichert 
\ac{SentiWS} das Wort, seine lexikalische Kategorie, verschiedene Endungen und 
eine Zahl zwischen $-1$ (Negativ) und $+1$ (Positiv). Dieser Aufbau sei 
beispielhaft an einer Zeile aus der Liste für positive Wörter dargestellt:
\texttt{Applaus|NN  0.0871  Applausen,Applauss,Applause,Applauses}
\index{SentiWS}

\medskip
Laut einer umfassenden Metastudie aus dem Jahre 2015, die 24 unterschiedliche 
\ac{SA}-Verfahren mit 18 verschiedenen Korpora getestet hat, gehört \ac{VADER} 
\QuoteIndirectNoPage{conf/icwsm/HuttoG14} zu den besten Verfahren für drei 
Klassen \QuoteIndirect{DBLP:journals/corr/RibeiroAGBG15}{S 13. Tab. 6}. Auch 
\ac{VADER} basiert auf einem Lexikon, das durch Regeln erweitert wird, es ist 
insbesondere für Texte aus dem Bereich der sozialen Medien geeignet.
\index{VADER}

\subsubsection{Statistische Verfahren}
\label{subsubsec:StatistischeVerfahren}

Den vorwissensbasierten Verfahren gegenüber stehen die statistischen Verfahren. 
Sie nutzen \ac{ML}-Techniken, um anhand eines Korpus Verfahren zu trainieren, 
deren Modelle versuchen, einen Teil der Realität abzubilden. Sowohl der zweite 
in \autoref{subsec:Historie} beschriebene Ansatz von Pang et~al. als auch das 
in dieser Ausarbeitung vorgestellte Verfahren fallen in die Klasse der 
statistischen Verfahren. Gegenüber den vorwissensbasierten Verfahren zeichnet 
sich diese Art von Vorgehensweisen durch eine höhere Anpassung an die Domäne 
des Korpus aus. Im Folgenden sollen einige interessante Ansätze einzeln 
besprochen werden. Für einen Überblick über \mbox{eine} Vielzahl von Verfahren 
sei danach jedoch auf verschiedene Meta-Ausarbeitungen verwiesen. 

\smallskip
Der diesen Verfahren zugrundeliegende Arbeitsablauf lässt sich allgemein wie 
folgt beschreiben. Der Korpus wird zunächst in verschiedenen Schritten 
vorverarbeitet:

\begin{itemize}
	\setlength\itemsep{\Itemizespace}
	
	\item Ein Textstück wird in seine Bestandteile (Wörter und Interpunktion) 
	zerteilt (auf Englisch \QuoteM{Tokenizeing}).
	
	\item Oft werden die Wörter alle zu Kleinschreibung umgewandelt.
	
	\item Oft werden häufig genutzte Bindewörter (auf Englisch 
	\QuoteM{Stopwords}, z.B. in, bin, vom) entfernt.
	
	\item Oft werden die Worte auf ihre Stammform reduziert (auf Englisch 
	\QuoteM{Stemming})
	\QuoteIndirectNoPage{porter1980algorithm}.
\end{itemize}

Anschließend wird aus diesen Wörtern ein Merkmalsvektor (auf Englisch 
\QuoteM{Feature vector}) generiert, mit dem das Klassifikationsverfahren 
trainiert wird. Ist das Training abgeschlossen, wird die Kette an 
Arbeitsschritten auch auf unbekannte Daten zur Klassifikation angewendet. 
In diesem Ablauf ist die Erstellung der Merkmalsvektoren ein kritischer 
Schritt, durch den festgelegt wird, welche Merkmale informativ sind und welche 
nicht \QuoteIndirectNoPage{DBLP:conf/lwa/FlenderG17}. Ein übliches Vorgehen 
dabei ist der Bag-of-words-Ansatz, bei dem ein Satz in eine vorgegebene Menge 
($n$-gram) an Wörtern zerteilt wird. Dazu ein Beispiel mit $n=2$ aus der 
Arbeit von Narr et~al. \QuoteIndirectNoPage{dailabor2012twittersentiment}:
\index{Merkmalsvektor>Feature vector}
\index{Bag-of-words}

\begin{quote}
	Hatte einen wunderbaren Abend.. :)\\
	\{Hatte einen\}, \{einen wunderbaren\}, \{wunderbaren Abend\}, u.s.w.
\end{quote}

Einige Klassifikatoren (z.B. \acp{ANN} \QuoteIndirectNoPage{Socher-etal:2013}) 
sind in der Lage, den Informationsgehalt der Merkmale selbständig zu ermitteln 
und damit die nützlichsten Merkmalsvektoren automatisiert zu generieren. Die 
Klassifikatoren lassen sich weiterhin nach Standardverfahren (z.B. \ac{NB}, 
\ac{ME} oder \ac{SVM}) oder speziell für \ac{SA} entwickelt 
\QuoteIndirectNoPage{pang2004sentimental} einteilen.

\medskip
Eine weitere übliche Unterscheidung wird zwischen kaskadierten und direkten 
Verfahren getroffen. Bei den kaskadierten Verfahren wird i.d.R. zunächst ein 
Klassifikator trainiert, der zwischen einem subjektiven und einem objektiven 
Satz unterscheidet. Anschließend werden die subjektiven Sätze in positive und 
negative Stimmung aufgeteilt 
\QuoteIndirectNoPage{AsiaeeT.:2012:YHY:2396761.2398481, 
Barbosa:2010:RSD:1944566.1944571,Boiy:2009:MLA:1612979.1612986, 
pang2004sentimental}.

\bigskip
Das \QuoteM{aktive Lernen} bildet einen weiteren interessanten Ansatz. Dabei 
wird dem Klassifikator ein verhältnismäßig kleiner Korpus zur Initialisierung 
vorgeführt. Anschließend wird dem Klassifikator eine große Menge 
unklassifizierter Daten zur Verfügung gestellt, aus denen er einige Fälle 
auswählt, die anschließend von einem Menschen annotiert werden. Dieser 
Vorgang kann dabei mehrfach wiederholt werden. Damit solch ein aktiv lernendes 
Verfahren mit möglichst wenig klassifizierten Daten eine mit einem überwachten 
Verfahren vergleichbare Klassifikationsqualität erreicht, muss es die Beispiele 
mit dem höchsten Informationsgewinn auswählen 
\QuoteIndirectNoPage{Boiy:2009:MLA:1612979.1612986, ActiveLearningNLP}. Wie die 
Arbeiten von Bondu et~al. \QuoteIndirectNoPage{Bondu:2007:ALS:1770770.1770794} 
und Fu et~al. \QuoteIndirectNoPage{Fu:2015:RAH:2818869.2818908} zeigen, gibt es 
auch für die \ac{SA} Ansätze, die sich mit aktivem Lernen beschäftigen. 
\index{Aktives Lernen}

\medskip
In der Arbeit von Haldenwang und Vornberger werden zusätzlich zu den üblichen 
drei Klassen (\QuoteM{Positiv}, \QuoteM{Neutral}, \QuoteM{Negativ}) noch zwei 
weitere für Spam und Unsicherheit eingeführt 
\QuoteIndirectNoPage{haldenwang2015uncertainty}. Dadurch lassen sich auch Fälle 
abbilden, bei denen keine klar erkennbare Empfindung vorliegt.
\index{Spam}

\medskip
Über den eigentlichen Text hinaus lassen sich Metadaten, wie z.B. die 
lexikalische Kategorie (auf Englisch \QuoteM{part-of-speech}), Hashtags,
eingebettete Links, Emoticons, Dehnung von Wörtern (z.B. Jaaaa) 
\QuoteIndirectNoPage{Brody:2011:CUW:2145432.2145498} und falls vorhanden 
Strukturen der sozialen Medien, aus denen die Texte stammen (z.B. Antworten, 
Kommentare, ReTweets), nutzen. Dieser Ansatz wurde unter anderem von Barbosa 
und Feng verfolgt \QuoteIndirectNoPage{Barbosa:2010:RSD:1944566.1944571}. 
\index{Metadaten}

\bigskip
Eine gute Übersicht über einige der zwischen 1997 und 2010 veröffentlichten 
Verfahren liefern Agarwal et~al. \QuoteIndirect{Agarwal2015}{S. 11, Tab 6}, 
Prabowo et~al. \QuoteIndirect{Prabowo2009SentimentAA}
{S. 4 f., Tab 1 f.} und Tsytsarau et~al. 
\QuoteIndirect{Tsytsarau:2012:SMS:2198180.2198208}
{S. 25 f., Tab. 1}. Daran anschließend liefern Medhat et~al. eine ähnliche 
Zusammenfassung von Verfahren, die in den Jahren von 2010 bis 2012 
veröffentlicht wurden \QuoteIndirect{Medhat2014SentimentAA}{S. 1096, Tab. 1}. 
Die Wiederholbarkeit von 11 \ac{ML}-Verfahren zur \ac{SA} wurde von Dashtipour 
et~al. untersucht \QuoteIndirectNoPage{Dashtipour2016}, dabei ließen sich in 
vielen Fällen die Ergebnisse nur mit schlechterer Klassifikationsgenauigkeit 
als angegeben reproduzieren.

\medskip
Die Genauigkeit der erreichten Klassifikation bewegt sich abhängig von der 
Anzahl der genutzten Klassen im Bereich von ca. 81~\% für 5 Klassen 
\QuoteIndirectNoPage{Socher-etal:2013}), ca. 84~\% für 3 Klassen 
\QuoteIndirectNoPage{DBLP:conf/lwa/FlenderG17} und ca. 90~\% für 2 Klassen 
\QuoteIndirect{DBLP:journals/corr/MesnilMRB14, Agarwal2015}{S. 11, Tab 6}. 
Wobei sich diese Werte, wie in \autoref{sec:Metriken} gezeigt, nur schwer 
miteinander vergleichen lassen.
\index{Genauigkeit>Stand der Forschung}

\subsubsection{Hybride Verfahren}
\label{subsubsec:HybrideVerfahren}

Die beiden vorherigen Ansätze lassen sich zu hybriden Verfahren kombinieren. 
Ein mögliches Vorgehen ergibt sich dabei aus dem Training eines Klassifikators 
mit einem Korpus, der aus einem Lexikon gewonnen wurde \QuoteIndirectNoPage{
He:2011:SLF:1993999.1994073, Sindhwani:2008:DCS:1510528.1511363,
ZHKHAN2015CombiningLA}.

\medskip
In der Arbeit von Xu et~al. wird die Erkennung von linguistischen Strukturen 
über Regeln mit einer \ac{SVM} kombiniert und auf Texte aus Webseiten 
angewendet \QuoteIndirectNoPage{Xu:2008:LKR:1486927.1487049}.

\medskip
Eine andere Möglichkeit besteht darin, erst ein \ac{ML} und anschließend ein 
regelbasiertes Verfahren auf die Daten anzuwenden, dabei kann das regelbasierte 
Verfahren übertragenen Sinn und andere für Maschinen schwer greifbare Konzepte 
abdecken \QuoteIndirectNoPage{Rentoumi2010UnitedWS}.

\medskip
Zhang et~al. gehen den umgekehrten Weg, bei dem die Daten zunächst von einem 
lexikonbasierten Verfahren auf Wortebene und anschließend von einem 
\ac{ML}-Klas\-si\-fi\-ka\-tor untersucht werden. Dabei werden die 
Trainingsdaten für das \ac{ML}-Verfahren durch das vorhergehende 
Lexikon-Verfahren erzeugt \QuoteIndirectNoPage{Zhang2011CombiningLA}.

\subsubsection{Erkannte Komponenten}
\label{subsubsec:ErkannteKomponenten}

Die bis jetzt vorgestellten Vorgehensweisen versuchen innerhalb eines Textes, 
oder einem Teil davon, eine Empfindung zu erkennen, ohne diese Empfindung 
auf ein Objekt oder Aspekte eines Objektes zu beziehen. Zusätzlich zu der 
eigentlichen Empfindung kann noch versucht werden, das Subjekt (z.B. den Autor 
des Textes) \QuoteIndirectNoPage{Kim:2006:IAJ:1220835.1220861} und das Objekt 
(z.B. das Thema des Textes) oder Aspekte des Objektes (z.B. Eigenschaften des 
Themas, wie die Auflösung eines Displays) 
\QuoteIndirectNoPage{Kobayashi07extractingaspect, Zhang2014} zu erkennen. Durch 
dieses Vorgehen lassen sich viele zusätzliche Informationen extrahieren, die 
insbesondere für wirtschaftliche Anwendungen interessant sind. Eine 
Möglichkeit, diese Informationen zu gewinnen, ist ein \ac{SA}-Verfahren, um 
eine \ac{NER}-Komponente zu erweitern \QuoteIndirectNoPage{Cruz2014ImplicitAI}.

\medskip
Werden lexikonbasierte Verfahren zur Empfindungserkennung eingesetzt, kann es 
passieren, dass das zu findende Objekt selbst im Lexikon vorkommt und sich 
dadurch eine Schleife bildet. In der Arbeit von Peleja et~al. wird gezeigt wie 
sich dieses Problem durch die Erstellung eines Graphen umgehen lässt 
\QuoteIndirectNoPage{Peleja:2014:RLS:2682648.2682837}.

\medskip
In der Arbeit von Liu wird, wenn auch in einem anderen Kontext, ein gutes 
Beispiel für eine \ac{SA}, die sich mit Aspekten von Objekten beschäftigt, 
vorgestellt \QuoteIndirect{Liu10sentimentanalysis}{S. 3}. Sollen Empfindungen 
gegenüber mehreren Aspekten eines Objektes erkannt werden, kommen 
spezialisierte Algorithmen, wie z.B. von Snyder und Barzilay entwickelt, zum 
Einsatz \QuoteIndirectNoPage{Snyder07multipleaspect}.

\medskip
Ein Verfahren, das zusätzlich zu der Empfindung auch das Objekt (auf Englisch 
\QuoteM{Target-Dependent}) erkennt, kann zu einer Suchmaschine ausgebaut 
werden, die nach Texten sucht, die eine Empfindung gegenüber dem gesuchten 
Objekt besitzen \QuoteIndirectNoPage{Jiang:2011:TTS:2002472.2002492, 
Vo:2015:TTS:2832415.2832437}.
\index{Sentiment Analysis>Target-Dependent}

\subsection{Genutzte Texte}
\label{subsec:Texte}

Sowohl die Herkunft und Art als auch die Sprache der Texte, deren Stimmung 
untersucht werden soll, haben einen großen Einfluss auf das \ac{SA}-Verfahren 
und seine Ergebnisse, deswegen sollen in diesem Abschnitt einige Quellen für 
gut geeignete Texte und die Sprachen, in denen die Texte verfasst sind, 
besprochen werden.

\subsubsection{Genutzte Quellen}
\label{subsubsec:Quellen}

Generell kann jeder Text, der digital vorliegt, als Un­ter­su­chungs­ob­jekt für die 
\ac{SA} genutzt werden. Da jedoch Texte, die objektiv formuliert werden, 
wie z.B. Bedienungsanleitungen oder Nachrichten, keine Empfindungen enthalten 
sollten, eignen sie sich nur bedingt. Einige Arten von Texten, die sich gut für 
die \ac{SA} eignen und auch zu diesem Zweck genutzt werden, sollen im Folgenden 
näher besprochen werden.

\begin{itemize}
	\setlength\itemsep{\Itemizespace}
	\item Der Begriff \textit{Blog} bezeichnet eine Art Tagebuch, das über das 
	Internet erreichbar ist	(Web-Log). Zu beinah jedem Thema (z.B. Jura und 
	Gesetzgebung \QuoteIndirectNoPage{Conrad:2007:OML:1276318.1276363}) findet 
	sich dort mindestens ein Blog. Zum einen durch diese Vielzahl an möglichen 
	Themen und zum anderen durch die oftmals subjektive Empfindung des Autors 
	(des Bloggers) eignen sich solche Texte gut für die \ac{SA}. Ein weiterer 
	Vorteil von Blogs ist, dass ihr Inhalt einfach zu erreichen ist und so 
	große Mengen an Text gesammelt werden können
	\QuoteIndirectNoPage{Boiy:2009:MLA:1612979.1612986,
	using-verbs-and-adjectives-to-automatically-classify-blog-sentiment}.
	\index{Blog}
	
	\item Obwohl, wie oben beschrieben, \textit{Nachrichten} objektiv 
	formuliert sein sollten, kann es vorkommen, dass sie dennoch eine Stimmung 
	enthalten. Die enorme Menge an Nachrichten-Texten bietet interessante 
	Möglichkeiten, zunächst Texte anhand von Stichworten zu filtern und die 
	Stimmung dieser Texte anschließend zu analysieren, um so der 
	\ac{PR}-Abteilung eines Unternehmens oder einer Hochschule ein Werkzeug an 
	die Hand zu geben, um Rückmeldungen einzuholen 
	\QuoteIndirectNoPage{GerNews}. Nachrichtengruppen (auf Englisch 
	\QuoteM{Newsgroups}) stellen eine ähnliche Quelle dar, welche i.d.R. 
	einen wesentlich subjektiveren Standpunkt widerspiegeln 
	\QuoteIndirectNoPage{Arnold:2014:NVB:2878453.2878455}.
	\index{Nachrichten}
	
	\item Wie schon in \autoref{subsec:Wirtschaft} 
	\QuoteM{\nameref{subsec:Wirtschaft}} auf 
	\autopageref{subsec:Wirtschaft} dargelegt, ist das Einholen der 
	\textit{Kundenrückmeldung} eine der wichtigsten Anwendungen der \ac{SA}. 
	Entsprechend viele Text-Quellen werden zu diesem Zweck herangezogen 
	\QuoteIndirectNoPage{Ng:2006:ERL:1273073.1273152,
	Turney:2002:TUT:1073083.1073153}.
	\index{Kundenrückmeldung}
	\begin{itemize}
		\setlength\itemsep{\Itemizespace}
		
		\item Rückmeldungen von Kunden bezüglich Produkten und Unternehmen auf 
		Plattformen von Dritten, z.B. dem Verbraucherschutz
		\QuoteIndirectNoPage{Dini+Mazzini:02a,
		Durbin2003ASF, Gamon:2004:SCC:1220355.1220476}.	
		
		\item Kundenrezensionen von Produkten im \textit{Onlinehandel}
		\QuoteIndirectNoPage{Dave:2003:MPG:775152.775226,
		Hu:2004:MSC:1014052.1014073,
		Hu:2004:MOF:1597148.1597269,
		Titov:2008:MOR:1367497.1367513,
		Wang:2009:FSM:1695584.1695597}.
		\index{Kundenrezensionen}
		
		\item Diskussionen über Produkte und Dienstleistungen in \textit{Foren}
		\QuoteIndirectNoPage{FLAIRS101323, Glance:2005:DMI:1081870.1081919}.

		\item Kundenrezensionen von online gebuchten \textit{Hotels}.
		\QuoteIndirectNoPage{journals/snam/CataldiBTA13, 
		HotelReviews, HotelReviewsSupervised}
		
		\item \textit{Filmkritiken} von entsprechenden Aggregationswebseiten, 
		z.B. \href{http://www.imdb.com/}{\ac{IMDb}}. Diese Webseiten bieten zu 
		vielen Kritiken als Text auch eine maschinenlesbare Bewertung (z.B. 
		eine Skala von 1 bis 10), die sich gut für das Training entsprechender 
		\ac{ML}-Verfahren nutzen lässt.
		\QuoteIndirectNoPage{pang2004sentimental, pang2002thumbs}.
		\index{Filmkritik}
		
	\end{itemize}
	
	\item Selbst \textit{Musiktexte} lassen sich zur \ac{SA} nutzen 
	\QuoteIndirectNoPage{Xia:2008:SVS:1557690.1557725}. 
	\index{Musiktext}
	
	\item Mikroblogging-Dienste, wie das seit 2006 ins Leben gerufene 
	\textit{Twitter} \QuoteIndirectNoPage{DBLP:journals/corr/abs-0911-1583}, 
	stellen eine Art der sozialen Medien dar, bei denen der Nutzer einen 140 
	Zeichen (um die Kompatibilität zum \ac{SMS} zu erhalten
	\QuoteIndirectNoPage{Brody:2011:CUW:2145432.2145498}) langen Text, der sich 
	Tweet nennt, mit allen Leuten, die ihm folgen, teilen kann. Ein Nutzer kann 
	anderen Leuten folgen und sich so ihre Tweets ansehen. Die Beschränkung auf 
	140 Zeichen, die oftmals informelle Sprache und die häufige Nutzung von 
	Emoticons führen zu einem besonderen Interesse an diesen Texten im Rahmen 
	der \ac{SA} \QuoteIndirectNoPage{Brody:2011:CUW:2145432.2145498, 
	Cui:2011:ETB:2189339.2189365, Davies2011LanguageindependentBS, 
	go2009distant, 2c78550c26834aefa6dae102714cb1dc}. Auch diese Arbeit nutzt 
	Texte von Twitter als Datenquelle. 
	
	Sowohl zu Verfahren, die Twitter als Datenquelle nutzen 
	\QuoteIndirectNoPage{Abbasi2014BenchmarkingTS, SentimentAnalysisInTwitter}, 
	als auch zu den Korpora, die auf Basis von Twitter erstellt wurden, gibt es 
	Metaanalysen \QuoteIndirectNoPage{noauthororeditor2013evaluation}. 
	\ac{SemEval} ist ein jährlich stattfindender Wettbewerb, bei dem sich eine 
	Aufgabe mit der \ac{SA} von Tweets beschäftigt 
	\QuoteIndirect{DBLP:journals/corr/RibeiroAGBG15}
	{S. 3}. Dieser Umstand verdeutlicht, wie weit verbreitet Twitter als 
	Datenquelle ist \QuoteIndirectNoPage{Ghosh2015SemEval2015T1, 
	Rosenthal2015SemEval2015T1}.	
	\index{Twitter} \index{Twitter>Tweet}

\end{itemize}

\subsubsection{Genutzte Sprachen}
\label{subsubsec:Sprachen}

Wie in \autoref{sec:Schwächen} \QuoteM{\nameref{sec:Schwächen}} auf  
\autopageref{sec:Schwächen} gezeigt, beschäftigt sich die überwiegende Anzahl 
der Arbeiten zum Thema \ac{SA} mit englischsprachigen Texten. Jedoch wird in 
den sozialen Medien bei weitem nicht nur Englisch gesprochen, so sind nur ca. 
40~\% bis 50~\% aller Veröffentlichungen auf Twitter und Facebook in Englisch 
formuliert \QuoteIndirectNoPage{Twitter40, Facebook50} und laut den 
Erkenntnissen von Hong et~al. werden 1 bis 2~\% aller Tweets in Deutsch 
abgefasst \QuoteIndirectNoPage{Hong2011LanguageMI}.

\medskip
Die Ansätze, die sich mit nicht englischsprachigen Texten auseinandersetzen, 
lassen sich grob in drei Bereiche einteilen: 

\begin{itemize}
	\setlength\itemsep{\Itemizespace}

	\item Verfahren, die direkt andere Sprachen nutzen.
	
	\begin{itemize}
		\setlength\itemsep{\Itemizespace}
		\item Arabisch \QuoteIndirectNoPage{AlAyyoub2015LexiconbasedSA}
		\item Chinesisch \QuoteIndirectNoPage{fuping2016chinese}
		\item Deutsch \QuoteIndirectNoPage{GerNews, DBLP:conf/lwa/FlenderG17}
		\item Französisch \QuoteIndirectNoPage{DBLP:series/sci/GhorbelJ11}
		\item Japanisch \QuoteIndirectNoPage{Evans2007}
		\item Persisch \QuoteIndirectNoPage{DBLP:journals/corr/BagheriS14a}
		\item Tschechisch 
		\QuoteIndirectNoPage{Habernal:2014:SSA:2657511.2657835}
	\end{itemize}
	
	\item Verfahren, die mit mehreren Sprachen parallel arbeiten können 
	\QuoteIndirectNoPage{MultilingualSubjectivity, 
	dailabor2012twittersentiment}.
	
	\item Verfahren, die die Daten zunächst mittels \ac{MT} ins Englische 
	übersetzen und dort anschließend die Empfindungsklassifikation vornehmen 
	\QuoteIndirectNoPage{Balahur:2012:MSA:2392963.2392976, 
	Balahur2013ImprovingSA, Bautin+Vijayarenu+Skiena:08a, 
	DBLP:journals/corr/DeriuLLSMCHJ17}.
	
\end{itemize}
