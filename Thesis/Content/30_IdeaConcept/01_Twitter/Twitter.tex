\label{sec:Twitter}

Zunächst soll gezeigt werden, wieso in dieser Arbeit die Textform Tweets zur 
\ac{SA} genutzt wird. Nachdem einige Gründe für Twitter als Datenquelle 
aufgezeigt wurden, soll anschließend dargestellt werden, wieso in dieser Arbeit 
deutschsprachige Texte analysiert werden.

\subsection{Tweets}
\label{subsec:Tweets}

Wie in \autoref{subsubsec:Quellen} \QuoteM{\nameref{subsubsec:Quellen}} auf 
\autopageref{subsubsec:Quellen} gezeigt, eignen sich viele unterschiedliche 
Arten an Text für die \ac{SA}. Im Folgenden sollen einige Besonderheiten von 
Twitter als Datenquelle aufgezeigt werden. Diese Besonderheiten und die 
Verfügbarkeit entsprechender Korpora stellen den Grund für die Nutzung von 
Twitter als Textquelle dar.

\medskip
Das in dieser Arbeit zu entwickelnde \ac{SA}-Verfahren soll Texte auf Satzebene 
analysieren, um so Empfindungen möglichst feingranular darstellen zu können. 
Eine Untersuchung von Teilsätzen wird, da dazu die nötigen Korpora fehlen, 
nicht durchgeführt. Dank der maximalen Länge von 140 Zeichen besteht ein Tweet 
i.d.R. nur aus einem Satz, somit kann auf eine Analyse von Teilsätzen 
verzichtet werden.

\medskip
Tweets werden als schwierig zu klassifizieren betrachtet, da sie einige 
sprachliche Besonderheiten besitzen, die bei vielen anderen Arten von Text 
fehlen. So weist die Sprache zumeist eine hohe Informalität auf, die sich oft 
sogar zu Rechtschreib- und Grammatikfehlern steigert. Hinzukommen besondere 
Merkmale wie Twitter-Nutzernamen und Hyperlink-Strukturen wie Hashtags und 
ReTweets. Die Anzahl an besprochenen Themen ist so vielfältig, dass sie einen 
großen Bereich des menschlichen Alltages widerspiegelt. Auch die geringe Länge 
trägt zu der Schwierigkeit des Problems bei.

\medskip
In Tweets werden oftmals Emoticons, die aus verschiedenen \ac{ASCII}-Zeichen 
zusammengesetzt werden (z.B. \mbox{:-)} oder \mbox{(>\_<)}), aber auch 
Smileys, die Teil von \ac{UTF-8} (z.B. \Smiley, \dSadey, \Bratpfanne) sein 
können, genutzt. Viele dieser Emoticons geben direkte Hinweise auf die im Tweet 
enthaltenen Empfindungen. Aus diesem Grund werden sie in manchen Arbeiten als 
verrauschte Klassen (auf Englisch \QuoteM{Noisy Labels}) genutzt 
\QuoteIndirectNoPage{Barbosa:2010:RSD:1944566.1944571, go2009distant, 
dailabor2012twittersentiment}. In solchen Fällen müssen die Emoticons vor dem 
Training aus dem Korpus gefiltert werden, da sonst die Klassifikatoren anhand 
der Emoticons und nicht des Textes lernen. In dieser Arbeit sollen die 
Emoticons nicht zur Klassifikation herangezogen werden, deswegen können sie im 
Text verbleiben und zusätzliche Anhaltspunkte für die Erkennung der Stimmung 
bieten. Dieses Vorgehen ist realistischer, da der fertige Klassifikator auch 
mit Emoticons umgehen kann.
\index{Emoticon}
\index{Smiley}
\index{Noisy Label}

\medskip
Dank der allgemeinen Popularität von Twitter lassen sich große Mengen an 
Rohdaten aus vielen verschiedenen Sprachen erreichen. Die Beschaffung und 
Verarbeitung dieser Daten ist dank der von Twitter bereitgestellten \ac{API} 
sehr einfach.
\index{Twitter>API}

\subsection{Deutsche Sprache}
\label{subsec:Deutsche}

Die meisten der in \autoref{sec:Bedeutung} besprochenen Anwendungen lassen 
sich nicht vollständig durch \ac{SA}-Verfahren, die nur englischsprachige Texte 
analysieren können, abdecken, z.B. werden politische Diskussionen und der 
Wahlkampf in Deutschland i.d.R. auf Deutsch geführt. Auch Kundenkommunikation 
und Mundpropaganda-Marketing sind in Deutschland üblicherweise deutschsprachig. 
Auch für internationale Unternehmen stellt letzteres ein Problem dar, da sich 
der deutschsprachige Markt nicht in jedem Fall analog zum englischsprachigen 
oder internationalen verhalten muss.

\medskip
Jede Sprache ist nach ihren eigenen Regeln und Grammatiken aufgebaut, Groß- und 
Kleinschreibung, Verbkonjugation und Verneinungen sind nur einige Beispiele. 
Die deutsche Grammatik kann an einigen Stellen, wie z.B. bestimmte Artikel 
(\QuoteM{der}, \QuoteM{die}, \QuoteM{das} vs. \QuoteM{the}), sogar als 
komplexer betrachtet werden. Klassifikatoren, die auf englischsprachige Tweets 
trainiert wurden, lassen sich dadurch schlecht für deutschsprachige Daten 
nutzen.

\medskip
Es gibt zwar einige Ansätze, die sich mit multilingualen Korpora 
auseinandersetzen, aber Verfahren, die sich nur auf deutschsprachige Tweets 
beziehen, sind, wie auch in \autoref{sec:Schwächen} dargestellt, selten.
Dieser Umstand, verbunden mit der wirtschaftlichen Notwendigkeit sowie der 
größeren Herausforderung, ist der Grund dafür, dass in dieser Arbeit die 
deutsche Sprache für die Textquellen genutzt wird.
