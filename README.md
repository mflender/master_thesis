## Maltes Master Thesis

2017, FH Bielefeld, computer science

In order to use the index you need to run `makeindex -g -s Index.ist %.idx`

This thesis, a requirement for a master’s degree, deals with the development of a
software for Sentiment Analysis of German Twitter posts. To do so, the Sentiment
Analysis, its applications, and economic necessity will be explained. Furthermore,
the Sentiment Analysis is embedded in the bigger picture of Machine Learning and
artificial intelligence, the general process of Machine Learning is also described.

The extensive state of research is categorized by different criteria and is elaborated.
Possibilities to measure the quality of the Sentiment Analysis will also be shown.

The core of this thesis is the introduction and implementation of a new form of
Artificial Neural Networks, derived from »Generative Adversarial Networks«. To do
so, functional and non-functional requirements of the software will be established.

To implement these requirements, a Deep Learning Framework will be used. Both
the decision to use such a framework and also the choice of the actual framework
are described. TensorFlow is used in this thesis.

Afterwards, the implementation of the Sentiment Analysis software will be shown.
The results are first compared to the prior defined requirements and then to a
classical approach.

Finally, problems that could not be solved with the implementation and further
ideas for the future are described.
