# Copyright (c) 2017 Malte Flender
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of this 
# software and associated documentation files (the "Software"), to deal in the Software 
# without restriction, including without limitation the rights to use, copy, modify, merge, 
# publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons 
# to whom the Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all copies or 
# substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
# INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR 
# PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE 
# FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR 
# OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
# DEALINGS IN THE SOFTWARE.

import tensorflow as tf

#  Lables  Tweets
#    |       |
#   MLP1   LSTM1
#    |       |
#   BN       |
#    |       |
#   MLP2   LSTM2
#      |   |
#        |
#       MB
#        |
#      MLP3
#        |
#       BN
#        |
#      MLP4
class Discriminator():

    # Setup the size of the layers and the number of classes.
    def __init__(self,
                 mean=0.0,           # For the start values
                 stddev=1.0,         # For the start values
                 keep_prob=1.0,      # Dropout keep probabilty
                 lstm_keep_prob=1.0, # Dropout keep probabilty for the LSTM cells
                 activation_function=tf.nn.relu,
                 lstm_activation_function=tf.nn.tanh,
                 use_peepholes=False,
                 number_nodes_layer_1 = 500,
                 number_nodes_layer_2 = 500,
                 number_nodes_layer_3 = 500,
                 number_classes = 1):
        
                # Alternative activation functions:
                #tf.nn.relu
                #tf.nn.softplus
                #tf.nn.elu
                #tf.nn.sigmoid

        self.number_nodes_layer_1 = number_nodes_layer_1
        self.number_nodes_layer_2 = number_nodes_layer_2
        self.number_nodes_layer_3 = number_nodes_layer_3
        
        self.number_classes = number_classes
        
        self.mean = mean
        self.stddev = stddev
        self.keep_prob = keep_prob
        self.activation_function = activation_function
        self.lstm_activation_function = lstm_activation_function
        self.use_peepholes = use_peepholes
        self.lstm_keep_prob = lstm_keep_prob
        
    # From https://r2rt.com/implementing-batch-normalization-in-tensorflow.html
    def _batch_norm_layer(self, dropout_layer_input, name_scope=None):
        with tf.variable_scope(name_scope or "disc_label_batch_normalization"):
            weights_layer_batch_normalization = tf.get_variable("weights_layer_batch_normalization", [dropout_layer_input.get_shape().as_list()[1], self.number_classes], initializer=tf.random_normal_initializer(mean=self.mean, stddev=self.stddev))
            
            scale = tf.get_variable("scale", [dropout_layer_input.get_shape().as_list()[1]], initializer=tf.constant_initializer(1))
            beta = tf.get_variable("beta", [dropout_layer_input.get_shape().as_list()[1]], initializer=tf.constant_initializer(0))
            
            batch_norm = tf.matmul(dropout_layer_input, weights_layer_batch_normalization)
            
            batch_mean, batch_var = tf.nn.moments(batch_norm, [0])
            
            layer_batch_normalization = tf.nn.batch_normalization(batch_norm, batch_mean, batch_var, beta, scale, 1e-3)
            
            out_layer_batch_normalization = tf.nn.sigmoid(layer_batch_normalization)
            
        return out_layer_batch_normalization


    #From https://github.com/AYLIEN/gan-intro/blob/master/gan.py#L85-L92
    def _minibatch(self, both, name_scope=None, num_kernels=500, kernel_dim=3):
        with tf.name_scope(name_scope or "disc_label_layer_minibatch"):

            weights_layer_minibatch = tf.get_variable("weights_layer_minibatch", [both.get_shape()[1], num_kernels * kernel_dim], initializer=tf.random_normal_initializer(mean=0.0, stddev=1.0))
            biases_layer_minibatch = tf.get_variable("biases_layer_minibatch", [num_kernels * kernel_dim], initializer=tf.constant_initializer(0.0))
            
            x = tf.nn.xw_plus_b(both, weights_layer_minibatch, biases_layer_minibatch)
            
            activation = tf.reshape(x, (-1, num_kernels, kernel_dim))
            diffs = tf.expand_dims(activation, 3) - tf.expand_dims(tf.transpose(activation, [1, 2, 0]), 0)
            abs_diffs = tf.reduce_sum(tf.abs(diffs), 2)
            minibatch_features = tf.reduce_sum(tf.exp(-abs_diffs), 2)
            
            out_layer_4 = tf.concat([both, minibatch_features], 1)
        return out_layer_4

    # Defines the tensorflow graph.
    def build_graph(self, input_data_tweet, input_data_lable, reuse=None):

        # Check if word or char encoded input and transform accordingly
        if(len(input_data_tweet.get_shape().as_list()) == 4):
            # Multiply the last two dimensions
            input_data_x_format = tf.reshape(input_data_tweet, [-1, input_data_tweet.get_shape().as_list()[1], input_data_tweet.get_shape().as_list()[2] * input_data_tweet.get_shape().as_list()[3]])
        elif(len(input_data_tweet.get_shape().as_list()) == 2):
            # Add one empty dimension
            input_data_x_format = tf.expand_dims(input_data_tweet, 2)
        else:
            raise ValueError("build_graph ERROR: " + str(input_data_tweet.get_shape().as_list()))

        #-----------------------------------------

        with tf.name_scope("Discriminator"):

            with tf.name_scope("disc_tweet_layer_1"):
                cell_1 = tf.contrib.rnn.LSTMCell(self.number_nodes_layer_1, reuse=reuse, use_peepholes=self.use_peepholes, activation=self.lstm_activation_function)
                
                # Different types of cells:
                #cell_1 = tf.contrib.rnn.GRUCell(self.number_nodes_layer_1, reuse=reuse, activation=self.lstm_activation_function)
                
            with tf.name_scope("disc_tweet_layer_2"):   
                cell_2 = tf.contrib.rnn.LSTMCell(self.number_nodes_layer_2, reuse=reuse, use_peepholes=self.use_peepholes, activation=self.lstm_activation_function)
                
                # Different types of cells:
                #cell_2 = tf.contrib.rnn.GRUCell(self.number_nodes_layer_2, reuse=reuse, activation=tf.nn.sigmoid)
            
            #-----------------------------------------
            
            with tf.name_scope("disc_tweet_layer_together"):
                cell_together = tf.nn.rnn_cell.MultiRNNCell([cell_1, cell_2])
                drop_out = tf.nn.rnn_cell.DropoutWrapper(cell_together, dtype=tf.float32, input_keep_prob=self.lstm_keep_prob, output_keep_prob=self.lstm_keep_prob, state_keep_prob=self.lstm_keep_prob)
                val, _ = tf.nn.dynamic_rnn(drop_out, input_data_x_format, dtype=tf.float32)
                
            with tf.name_scope("disc_transpose"):
                last = tf.reshape(val[:, -1, :], [-1, self.number_nodes_layer_2])
  
            #-----------------------------------------

            with tf.name_scope("disc_label_layer_1"):
                weights_layer_1 = tf.get_variable("disc_weights_layer_1", [input_data_lable.get_shape().as_list()[1], self.number_nodes_layer_1], initializer=tf.random_normal_initializer(mean=self.mean, stddev=self.stddev))
                biases_layer_1 = tf.get_variable("disc_biases_layer_1",[self.number_nodes_layer_1], initializer=tf.constant_initializer(0.0))
                layer_1 = tf.nn.xw_plus_b(input_data_lable, weights_layer_1, biases_layer_1, name="disc_layer_1")
                out_layer_1 = self.activation_function(layer_1, name="disc_activation_layer_1")
                dropout_layer_1 = tf.nn.dropout(out_layer_1, self.keep_prob, name="disc_dropout_layer_1")
            
            out_layer_batch_normalization_1 = self._batch_norm_layer(dropout_layer_1, "disc_label_batch_normalization_1")
            
            with tf.name_scope("disc_label_layer_2"):
                weights_layer_2 = tf.get_variable("disc_weights_layer_2",[out_layer_batch_normalization_1.get_shape().as_list()[1], self.number_nodes_layer_2], initializer=tf.random_normal_initializer(mean=self.mean, stddev=self.stddev))
                biases_layer_2 = tf.get_variable("disc_biases_layer_2", [self.number_nodes_layer_2], initializer=tf.constant_initializer(0.0))
                layer_2 = tf.nn.xw_plus_b(out_layer_batch_normalization_1, weights_layer_2, biases_layer_2, name="disc_layer_2")
                out_layer_2 = self.activation_function(layer_2, name="disc_activation_layer_2")
                dropout_layer_2 = tf.nn.dropout(out_layer_2, self.keep_prob, name="disc_dropout_layer_2")
            
            #-----------------------------------------
    
            together = tf.add(last, dropout_layer_2)
            out_layer_4 = self._minibatch(together)
                
            with tf.name_scope("disc_label_layer_3"):
                weights_layer_3 = tf.get_variable("disc_weights_layer_3", [out_layer_4.get_shape().as_list()[1], self.number_nodes_layer_3], initializer=tf.random_normal_initializer(mean=self.mean, stddev=self.stddev))
                biases_layer_3 = tf.get_variable("disc_biases_layer_3", [self.number_nodes_layer_3], initializer=tf.constant_initializer(0.0))
                layer_3 = tf.nn.xw_plus_b(out_layer_4, weights_layer_3, biases_layer_3, name="disc_layer_3")
                out_layer_3 = self.activation_function(layer_3, name="disc_activation_layer_3")
                dropout_layer_3 = tf.nn.dropout(out_layer_3, self.keep_prob, name="disc_dropout_layer_3")

            out_layer_batch_normalization_2 = self._batch_norm_layer(dropout_layer_3, "disc_label_batch_normalization_2")
            
            with tf.name_scope("disc_layer_out"):
                weights_layer_out = tf.get_variable("disc_weights_layer_out", [out_layer_batch_normalization_2.get_shape().as_list()[1], self.number_classes], initializer=tf.random_normal_initializer(mean=self.mean, stddev=self.stddev))
                biases_layer_out = tf.get_variable("disc_biases_layer_out", [self.number_classes], initializer=tf.constant_initializer(0.0))
                layer_out = tf.nn.xw_plus_b(out_layer_batch_normalization_2, weights_layer_out, biases_layer_out, name="disc_layer_out")
                output_y = tf.nn.sigmoid(layer_out, name="disc_activation_layer_out")
                
        return output_y, dropout_layer_3
