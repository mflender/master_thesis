# Copyright (c) 2017 Malte Flender
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of this 
# software and associated documentation files (the "Software"), to deal in the Software 
# without restriction, including without limitation the rights to use, copy, modify, merge, 
# publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons 
# to whom the Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all copies or 
# substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
# INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR 
# PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE 
# FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR 
# OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
# DEALINGS IN THE SOFTWARE.


#############################################################################################
# https://github.com/grrrr/krippendorff-alpha is needed
#############################################################################################

import argparse
import os

from trainer import Trainer

# Suppress the CUDA loggings
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3'

PATH_NAR = "/data1/Dokumente/Studium/SS2017/Masterarbeit/Korpora/SentimentDataset/Data/de_sentiment_agree3.tsv"
PATH_MOZ = "/data1/Dokumente/Studium/SS2017/Masterarbeit/Korpora/Mozetic/With_Tweets/German_Twitter_sentiment_Tweet_Clean.csv"

def main():
    parser = argparse.ArgumentParser(description="Discriminative Adversarial Networks (DAN) trainer Version 0.1 Copyright (c) 2017 Malte Flender")
    parser.add_argument("-m" , "--path_to_moz",
                        help="The Path to the Mozetic corpus",
                        action="store",
                        default=PATH_MOZ)
    
    parser.add_argument("-n" , "--path_to_nar",
                        help="The Path to the Narr corpus",
                        action="store",
                        default=PATH_NAR) # Baseline @ 0.75531914893617021276 %
    
    parser.add_argument("-w" , "--word_encoding",
                        help="Use the word encoder, else use the char encoder (default: False)",
                        action='store_true',
                        default=False)
    
    parser.add_argument("-c" , "--mozetic_corpus",
                        help="Use Mozetic corpus for the analyzer (default: False (Narr Corpus))",
                        action="store_true",
                        default=False)
    
    parser.add_argument("-r" , "--train_test_ratio",
                        help="The ratio between the train and the test set (default: 0.10)",
                        action="store",
                        default=0.10)
    
    parser.add_argument("-b" , "--batch_size",
                        help="The size of one batch (default: 300)",
                        action="store",
                        default=300)
    
    parser.add_argument("-e" , "--number_epochs",
                        help="The number of epochs to run (default: 1000)",
                        action="store",
                        default=1000)
    
    parser.add_argument("-p" , "--test_print_every",
                        help="Run a test every given epoch (default: 10)",
                        action="store",
                        default=10)
    
    parser.add_argument("-x" , "--max_savepoints_kept",
                        help="The max amount of savepoints (default: 1)",
                        action="store",
                        default=1)
    
    parser.add_argument("-l" , "--learning_rate",
                        help="The learning rate of the regression (default: 0.001)",
                        action="store",
                        default=0.001)
    
    parser.add_argument("-t" , "--comment",
                        help="A comment to describe the actual run, will be stored within the run file",
                        action="store",
                        default=None)
    
    parser.add_argument("-s" , "--resume_training",
                        help="resumes training on the given model either with given or default number_epochs",
                        action="store",
                        default=None)
                        #default="/home/malte/workspace/TensorFlow/DAN/runs/2017-08-08T15:04:12.132399")
    
    parser.add_argument("-a" , "--analyzer_only",
                        help="Use only the analyzer for training/testing (default: False)",
                        action="store_true",
                        default=False)
    
    args = parser.parse_args()
    
    trainer = Trainer(args)
    
    if(args.resume_training):
        trainer.resume_train(args.resume_training)
    else:
        trainer.train()

#     trainer.predict_anal("/home/malte/workspace/TensorFlow/DAN/runs/2017-08-01T12:21:03.675372",
#                          ["Gute nacht ! Schlaft gut ihr alle!", # positiv
#                           "neu bei twitter", # neutral
#                           "Vermisse sie einfach. aber trau mich nicht es ihr mal zu sagen."]) # negativ

if __name__ == '__main__':
    main()
    print("Done")
