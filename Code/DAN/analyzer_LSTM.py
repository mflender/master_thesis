# Copyright (c) 2017 Malte Flender
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of this 
# software and associated documentation files (the "Software"), to deal in the Software 
# without restriction, including without limitation the rights to use, copy, modify, merge, 
# publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons 
# to whom the Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all copies or 
# substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
# INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR 
# PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE 
# FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR 
# OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
# DEALINGS IN THE SOFTWARE.

import tensorflow as tf

# Tweets
#    |
#  LSTM1
#    |
#  LSTM2
#    |
#  MLP3
class Analyzer():

    # Setup the size of the layers, the number of classes and all the other hyperparameters.
    def __init__(self,
                 mean=0.0,
                 stddev=1.0,
                 keep_prob=0.60,
                 use_peepholes=True,
                 activation_function = tf.nn.tanh,
                 number_nodes_layer_1 = 500,
                 number_nodes_layer_2 = 500,
                 number_classes = 3):

        self.number_nodes_layer_1 = number_nodes_layer_1
        self.number_nodes_layer_2 = number_nodes_layer_2

        self.number_classes = number_classes
        
        self.mean = mean
        self.stddev = stddev
        self.keep_prob = keep_prob
        self.use_peepholes = use_peepholes
        self.activation_function = activation_function

    # Defines the tensorflow graph.
    def build_graph(self, input_data_x):
        
        # Check if word or char encoded input and transform accordingly
        if(len(input_data_x.get_shape().as_list()) == 4):
            
            # Multiply the last two dimensions
            input_data_x_format = tf.reshape(input_data_x, [-1, input_data_x.get_shape().as_list()[1], input_data_x.get_shape().as_list()[2] * input_data_x.get_shape().as_list()[3]])
        elif(len(input_data_x.get_shape().as_list()) == 2):
            
            # Add one empty dimension
            input_data_x_format = tf.expand_dims(input_data_x, 2)
        else:
            raise ValueError("build_graph ERROR: " + str(input_data_x.get_shape().as_list()))
        
        
        with tf.name_scope('Analyzer'):
            with tf.name_scope('anal_layer_1'):
                cell_1 = tf.contrib.rnn.LSTMCell(self.number_nodes_layer_1, use_peepholes=self.use_peepholes, activation=self.activation_function)
                
                # Different types of cells:
                #cell_1 = tf.contrib.rnn.LayerNormBasicLSTMCell(self.number_nodes_layer_1, activation=self.activation_function)
                #cell_1 = tf.contrib.rnn.GRUCell(self.number_nodes_layer_1, activation=self.activation_function)
                
            with tf.name_scope('anal_layer_2'):   
                cell_2 = tf.contrib.rnn.LSTMCell(self.number_nodes_layer_2, use_peepholes=self.use_peepholes, activation=self.activation_function)
                
                # Different types of cells:
                #cell_2 = tf.contrib.rnn.LayerNormBasicLSTMCell(self.number_nodes_layer_2, activation=self.activation_function)
                #cell_2 = tf.contrib.rnn.GRUCell(self.number_nodes_layer_2, activation=self.activation_function)
                
            with tf.name_scope('anal_layer_together'):
                cell_together = tf.nn.rnn_cell.MultiRNNCell([cell_1, cell_2])
                drop_out = tf.nn.rnn_cell.DropoutWrapper(cell_together, dtype=tf.float32, input_keep_prob=self.keep_prob, output_keep_prob=self.keep_prob, state_keep_prob=self.keep_prob)
                
                val, _ = tf.nn.dynamic_rnn(drop_out, input_data_x_format, dtype=tf.float32)
                
            with tf.name_scope('anal_transpose'):
                #https://github.com/yunjey/davian-tensorflow/blob/master/notebooks/week2/long_short_term_memory.ipynb
                last = tf.reshape(val[:, -1, :], [-1, self.number_nodes_layer_2])
                
            with tf.name_scope('anal_layer_out'):
                weights_layer_out = tf.Variable(tf.truncated_normal([self.number_nodes_layer_2, self.number_classes], mean=self.mean, stddev=self.stddev), name="anal_weights_layer_out")
                biases_layer_out = tf.Variable(tf.zeros([self.number_classes]), name="anal_biases_layer_out")
                layer_out = tf.nn.xw_plus_b(last, weights_layer_out, biases_layer_out, name="anal_layer_output")

        return layer_out
