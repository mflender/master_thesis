# Copyright (c) 2017 Malte Flender
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of this 
# software and associated documentation files (the "Software"), to deal in the Software 
# without restriction, including without limitation the rights to use, copy, modify, merge, 
# publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons 
# to whom the Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all copies or 
# substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
# INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR 
# PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE 
# FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR 
# OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
# DEALINGS IN THE SOFTWARE.

import collections
import datetime
import itertools
import krippendorff_alpha
import nltk
import numpy as np
import os
import tensorflow as tf
import time
import sklearn.metrics

from datetime import timedelta

from analyzer_LSTM import Analyzer
from discriminator_LSTM import Discriminator
from input_encoder import Input_encoder

NUM_CLASS = 3

# Trains the Analyzer and the DAN network, also performs logging.
class Trainer():
    
    # Sets the command line arguments.
    def __init__(self, args):
        self.args = args

    # Create an unique output directory.
    def _make_out_dir(self):
        timestamp = datetime.datetime.now().isoformat()
        out_dir = os.path.abspath(os.path.join(os.path.curdir, "runs", timestamp))
        print("Writing to {}".format(out_dir))
        if not os.path.exists(out_dir):
            os.makedirs(out_dir)
        return out_dir
    
    # Save the command line arguments to a file so you know what the settings where and can reload them.
    def _save_args(self, out_dir, arguments, make_new_file=False):
        if(make_new_file):
            try:
                os.remove(os.path.join(out_dir, "args.txt"))
            except OSError:
                pass
            with open(os.path.join(out_dir, "args.txt"), "w") as arg_file:
                arg_file.write(arguments)
                arg_file.close()
        else:
            for key, val in vars(self.args).iteritems():
                arguments += str(key) + "=" + str(val) + "\n"
            
            with open(os.path.join(out_dir, "args.txt"), "w") as arg_file:
                arg_file.write(arguments)
                arg_file.close()
        
    # From https://stackoverflow.com/questions/24527006/split-a-generator-into-chunks-without-pre-walking-it
    # Splits the generator into chunks to train batch by batch.
    def _chunks(self, iterable, size):
        iterator = iter(iterable)
        for first in iterator:
            yield itertools.chain([first], itertools.islice(iterator, size - 1))

    # Reads the args file and parses its values into a list.
    def _load_args(self, out_dir):
        args = {}
        with open(os.path.join(out_dir, "args.txt"), "r") as arg_file:
            content = arg_file.readlines()
            clean_content = [x.strip() for x in content]
            
            for i in clean_content:
                try:
                    args[i.split("=")[0]] = i.split("=")[1]
                except IndexError as _:
                    pass
        return args
    
    #From https://github.com/AYLIEN/gan-intro/blob/master/gan.py#L106-L113
    def log(self, x):
        '''
        Sometimes discriminiator outputs can reach values close to
        (or even slightly less than) zero due to numerical rounding.
        This just makes sure that we exclude those values so that we don't
        end up with NaNs during optimisation.
        '''
        return tf.log(tf.maximum(x, 1e-9))

    # Generates, prints and saves some metrics.
    def generate_metrics(self, anal_test_summary_writer, epoch, test_labels_y, eval_accuracy, eval_confusion_matrix, eval_argmax_prediction):
        # Generate base values for Metrics
        eval_argmax_y = np.argmax(test_labels_y, 1)
        
        ref_set = collections.defaultdict(set)
        test_set = collections.defaultdict(set)
        
        for i, (label) in enumerate(eval_argmax_y):
            ref_set[label].add(i)
        
        for i, (label) in enumerate(eval_argmax_prediction):
            test_set[label].add(i)
        
        alpha_input = [eval_argmax_prediction.tolist(), eval_argmax_y.tolist()]
        
        # Generate Metrics
        pos_prec = nltk.metrics.precision(ref_set[0], test_set[0]) or -1
        pos_reca = nltk.metrics.recall(ref_set[0], test_set[0]) or -1
        pos_fsco = nltk.metrics.f_measure(ref_set[0], test_set[0]) or -1
         
        neut_prec = nltk.metrics.precision(ref_set[1], test_set[1]) or -1
        neut_reca = nltk.metrics.recall(ref_set[1], test_set[1]) or -1
        neut_fsco = nltk.metrics.f_measure(ref_set[1], test_set[1]) or -1
         
        nega_prec = nltk.metrics.precision(ref_set[2], test_set[2]) or -1
        nega_reca = nltk.metrics.recall(ref_set[2], test_set[2]) or -1
        nega_fsco = nltk.metrics.f_measure(ref_set[2], test_set[2]) or -1
         
        mean_prec = sklearn.metrics.precision_score(eval_argmax_y, eval_argmax_prediction, average="macro") or -1
        mean_reca = sklearn.metrics.recall_score(eval_argmax_y, eval_argmax_prediction, average="macro") or -1
        mean_fsco = sklearn.metrics.f1_score(eval_argmax_y, eval_argmax_prediction, average="macro") or -1
         
        meaw_prec = sklearn.metrics.precision_score(eval_argmax_y, eval_argmax_prediction, average="weighted") or -1
        meaw_reca = sklearn.metrics.recall_score(eval_argmax_y, eval_argmax_prediction, average="weighted") or -1
        meaw_fsco = sklearn.metrics.f1_score(eval_argmax_y, eval_argmax_prediction, average="weighted") or -1
        
        # interval takes into account the ordering of sentiment values and assigns higher penalty to more extreme disagreements (from moz)
        alpha_int = krippendorff_alpha.krippendorff_alpha(alpha_input, metric=krippendorff_alpha.interval_metric, missing_items="")
        # nominal concidres the classes as unorderd
        alpha_nom = krippendorff_alpha.krippendorff_alpha(alpha_input, metric=krippendorff_alpha.nominal_metric, missing_items="")
         
        # Add Metrics to Tensoeboard
        anal_test_summary_writer.add_summary(tf.Summary(value=[tf.Summary.Value(tag="Analyzer_Metrics/positiv_precision", simple_value=pos_prec)]), epoch)
        anal_test_summary_writer.add_summary(tf.Summary(value=[tf.Summary.Value(tag="Analyzer_Metrics/positiv_recall", simple_value=pos_reca)]), epoch)
        anal_test_summary_writer.add_summary(tf.Summary(value=[tf.Summary.Value(tag="Analyzer_Metrics/positiv_f1", simple_value=pos_fsco)]), epoch)
         
        anal_test_summary_writer.add_summary(tf.Summary(value=[tf.Summary.Value(tag="Analyzer_Metrics/neutral_precision", simple_value=neut_prec)]), epoch)
        anal_test_summary_writer.add_summary(tf.Summary(value=[tf.Summary.Value(tag="Analyzer_Metrics/neutral_recall", simple_value=neut_reca)]), epoch)
        anal_test_summary_writer.add_summary(tf.Summary(value=[tf.Summary.Value(tag="Analyzer_Metrics/neutral_f1", simple_value=neut_fsco)]), epoch)
         
        anal_test_summary_writer.add_summary(tf.Summary(value=[tf.Summary.Value(tag="Analyzer_Metrics/negative_precision", simple_value=nega_prec)]), epoch)
        anal_test_summary_writer.add_summary(tf.Summary(value=[tf.Summary.Value(tag="Analyzer_Metrics/negative_recall", simple_value=nega_reca)]), epoch)
        anal_test_summary_writer.add_summary(tf.Summary(value=[tf.Summary.Value(tag="Analyzer_Metrics/negative_f1", simple_value=nega_fsco)]), epoch)
        
        anal_test_summary_writer.add_summary(tf.Summary(value=[tf.Summary.Value(tag="Analyzer_Metrics/mean_precision", simple_value=mean_prec)]), epoch)
        anal_test_summary_writer.add_summary(tf.Summary(value=[tf.Summary.Value(tag="Analyzer_Metrics/mean_recall", simple_value=mean_reca)]), epoch)
        anal_test_summary_writer.add_summary(tf.Summary(value=[tf.Summary.Value(tag="Analyzer_Metrics/mean_f1", simple_value=mean_fsco)]), epoch)
         
        anal_test_summary_writer.add_summary(tf.Summary(value=[tf.Summary.Value(tag="Analyzer_Metrics/mean_weighted_precision", simple_value=meaw_prec)]), epoch)
        anal_test_summary_writer.add_summary(tf.Summary(value=[tf.Summary.Value(tag="Analyzer_Metrics/mean_weighted_recall", simple_value=meaw_reca)]), epoch)
        anal_test_summary_writer.add_summary(tf.Summary(value=[tf.Summary.Value(tag="Analyzer_Metrics/mean_weighted_f1", simple_value=meaw_fsco)]), epoch)
                
        anal_test_summary_writer.add_summary(tf.Summary(value=[tf.Summary.Value(tag="Analyzer_Metrics/krippendorff_alpha_interval", simple_value=alpha_int)]), epoch)
        anal_test_summary_writer.add_summary(tf.Summary(value=[tf.Summary.Value(tag="Analyzer_Metrics/krippendorff_alpha_nominal", simple_value=alpha_nom)]), epoch)
        
        # Print Metrics
        print("Positiv Precision:           " +  str(pos_prec))
        print("Positiv Recall:              " +  str(pos_reca))
        print("Positiv F1:                  " +  str(pos_fsco))
        print("")
        print("Neutral Precision:           " +  str(neut_prec))
        print("Neutral Recall:              " +  str(neut_reca))
        print("Neutral F1:                  " +  str(neut_fsco))
        print("")
        print("Negative Precision:          " +  str(nega_prec)) 
        print("Negative Recall:             " +  str(nega_reca))
        print("Negative F1:                 " +  str(nega_fsco))
        print("")
        print("Mean Precision:              " + str(mean_prec))
        print("Mean Recall:                 " + str(mean_reca))
        print("Mean F1:                     " + str(mean_fsco))
        print("")
        print("Mean Weighted Precision:     " + str(meaw_prec))
        print("Mean Weighted Recall:        " + str(meaw_reca))
        print("Mean Weighted F1:            " + str(meaw_fsco))
        print("")
        print("Krippendorff Alpha interval: " + str(alpha_int))
        print("Krippendorff Alpha nominal:  " + str(alpha_nom))
        print("")
        print("Accuracy:                    " + str(eval_accuracy))
        print("Confusion Matrix:\n" + str(eval_confusion_matrix))

    # Train a single step of the analyzer.
    def _anal_train_step(self, encoder, sess, x, y, loss, train_op, anal_train_summary_op, anal_train_summary_writer, start_time, epoch, number_epochs, old_epochs=0):
        epoch_loss = 0
        i = 0
        
        train_data_x, train_labels_y, _ = encoder.get_data(test_data = False)
        
        for run, chunk in enumerate(self._chunks(train_data_x, int(self.args.batch_size))):
            start = i
            end = i + int(self.args.batch_size)
            i += int(self.args.batch_size)
                    
            batch_x = np.array(list(chunk))
            batch_y = np.array(train_labels_y[start:end])
            
            assert len(batch_x) == len(batch_y)
            
            _, l = sess.run([train_op, loss], feed_dict={x: batch_x, y: batch_y})
                    
            epoch_loss += l
            print("\tRun: " + str(run+1) + " Anal Loss: " + str(l))
        
        # Generate some metadate for the Tensorboard graph view
        run_options = tf.RunOptions(trace_level=tf.RunOptions.FULL_TRACE)
        run_metadata = tf.RunMetadata()
        
        # Run logging
        summary_train = sess.run(anal_train_summary_op, feed_dict={x: batch_x, y: batch_y}, options=run_options, run_metadata=run_metadata)
        anal_train_summary_writer.add_run_metadata(run_metadata, "train%03d" % epoch)
        anal_train_summary_writer.add_summary(summary_train, epoch)
        
        # Measure the average time per epoch and interpolate the remaining time
        time_done = time.time() - start_time
        time_one_epoch_average = time_done / (float(epoch)-float(old_epochs)+1)
        time_to_do = time_one_epoch_average * int(number_epochs)
        
        # Add the average time to Tensorboard
        summary = tf.Summary(value=[tf.Summary.Value(tag="Analyzer_Metrics/average_time_one_epoch", simple_value=time_one_epoch_average)])
        anal_train_summary_writer.add_summary(summary, epoch)
            
        #Add the average loss for this epoch to Tensorboard
        summary = tf.Summary(value=[tf.Summary.Value(tag="Analyzer_Optimizer/average_epoch_loss", simple_value=(epoch_loss/(run+1)))])
        anal_train_summary_writer.add_summary(summary, epoch)
        
        anal_train_summary_writer.flush()
        
        print(str(((float(epoch)+1)/float(number_epochs))*100)
                + "% (" + str(epoch+1) + "/" 
                + str(number_epochs) 
                + ") completed:\n Average loss:   " 
                + str(epoch_loss/(run+1))
                + "\n Time taken :    " 
                + str(timedelta(seconds=time_done))
                + "\n Time remaining: "
                +str(timedelta(seconds=time_to_do - time_done)))
    
    # Train a single step for both
    def _both_train_step(self, encoder, sess, anal_x, anal_y, anal_loss, anal_train_op, anal_train_summary_op, anal_train_summary_writer,
                         start_time, epoch, number_epochs, disc_loss, disc_train_op, disc_x_1_tweets, disc_x_1_lables, disc_train_summary_op, disc_train_summary_writer, old_epochs=0):
        anal_epoch_loss = 0
        disc_epoch_loss = 0
        i = 0
        
        train_data_x, train_labels_y, max_word_per_line = encoder.get_data(test_data = False, encode_both=True)
        
        # If analyzer uses moz generator must use narr and vice versa
        if (self.args.mozetic_corpus):
            disc_train_data_x, disc_train_labels_y, _ = encoder.get_data(test_data=False, use_mozetic_corpus=False, max_word_per_line=max_word_per_line)
        else:
            disc_train_data_x, disc_train_labels_y, _ = encoder.get_data(test_data=False, use_mozetic_corpus=True, max_word_per_line=max_word_per_line)
        
        run = 0
        for (anal_chunks, disc_chunks) in itertools.izip(self._chunks(train_data_x, int(self.args.batch_size)), self._chunks(disc_train_data_x, int(self.args.batch_size))):
            if(i+int(self.args.batch_size)>len(disc_train_labels_y)):
                break # if the disc_data is smaller then the anal_data stop with a few cases left for the train accuracy evaluation
            
            start = i
            end = i + int(self.args.batch_size)
            i += int(self.args.batch_size)
            run += 1
                    
            anal_batch_x = np.array(list(anal_chunks))
            anal_batch_y = np.array(train_labels_y[start:end])
            
            disc_batch_x = np.array(list(disc_chunks))
            disc_batch_y = np.array(disc_train_labels_y[start:end])
            
            if(len(anal_batch_y) < int(self.args.batch_size)):
                disc_batch_x = disc_batch_x[:len(anal_batch_y)]
                disc_batch_y = disc_batch_y[:len(anal_batch_y)]
            
            assert len(anal_batch_x) == len(anal_batch_y)
            assert len(disc_batch_x) == len(disc_batch_y)
            assert len(disc_batch_x) == len(anal_batch_x)
            
            _, anal_l, _, disc_l = sess.run([anal_train_op, anal_loss, disc_train_op, disc_loss],
                                            feed_dict={anal_x: anal_batch_x, 
                                                       anal_y: anal_batch_y,
                                                       disc_x_1_tweets: disc_batch_x, 
                                                       disc_x_1_lables: disc_batch_y})

            anal_epoch_loss += anal_l
            disc_epoch_loss += disc_l
            
            print("\tRun: " + str(run) + ", Anal Loss: " + str(anal_l) + ", Disc Loss: " + str(disc_l))
  
        # Generate some metadate for the Tensorboard graph vew
        run_options = tf.RunOptions(trace_level=tf.RunOptions.FULL_TRACE)
        run_metadata = tf.RunMetadata()
        
        # Run logging
        anal_summary_train, disc_summary_train = sess.run([anal_train_summary_op, disc_train_summary_op], 
                                 feed_dict={anal_x: anal_batch_x, anal_y: anal_batch_y, disc_x_1_tweets: disc_batch_x, disc_x_1_lables: disc_batch_y},
                                 options=run_options, run_metadata=run_metadata)
        
        anal_train_summary_writer.add_run_metadata(run_metadata, "train%03d" % epoch)
        anal_train_summary_writer.add_summary(anal_summary_train, epoch)
        
        disc_train_summary_writer.add_summary(disc_summary_train, epoch)
        
        # Measure the average time per epoch and interpolate the remeining time
        time_done = time.time() - start_time
        time_one_epoch_average = time_done / (float(epoch)-float(old_epochs)+1)
        time_to_do = time_one_epoch_average * int(number_epochs)
        
        # Add the average time to Tensorboard
        anal_train_summary_writer.add_summary(tf.Summary(value=[tf.Summary.Value(tag="Analyzer_Metrics/average_time_one_epoch", simple_value=time_one_epoch_average)]), epoch)
            
        #Add the average loss for this epoch to Tensorboard
        anal_train_summary_writer.add_summary(tf.Summary(value=[tf.Summary.Value(tag="Analyzer_Optimizer/average_epoch_anal_loss", simple_value=(anal_epoch_loss/run))]), epoch)
        disc_train_summary_writer.add_summary(tf.Summary(value=[tf.Summary.Value(tag="Discriminator_Optimizer/average_epoch_disc_loss", simple_value=(disc_epoch_loss/run))]), epoch)
        
        anal_train_summary_writer.flush()
        disc_train_summary_writer.flush()
        
        print(str(((float(epoch)+1)/float(number_epochs))*100)
                + "% (" + str(epoch+1) + "/" 
                + str(number_epochs) 
                + ") completed:"
                + "\n Average Anal loss:             " 
                + str(anal_epoch_loss/(run))
                + "\n Average Disc loss:             "
                + str(disc_epoch_loss/(run))
                + "\n Time taken:                    " 
                + str(timedelta(seconds=time_done))
                + "\n Time remaining:                "
                +str(timedelta(seconds=time_to_do - time_done)))

    # Perform a single test step
    def _test_step(self, checkpoint_prefix, encoder, sess, x, y, argmax_prediction, accuracy, confusion_matrix, anal_test_summary_op, anal_test_summary_writer, saver, epoch):
        path = saver.save(sess, checkpoint_prefix, global_step=epoch)
        
        print("Saved model checkpoint to {}\n".format(path))
                    
        test_data_x, test_labels_y, _ = encoder.get_data(test_data=True)
        
        run_options = tf.RunOptions(trace_level=tf.RunOptions.FULL_TRACE)
        run_metadata = tf.RunMetadata()
        
        test_data_list = list(test_data_x)
        assert len(test_data_list) == len(test_labels_y)
        
        eval_accuracy, eval_confusion_matrix, eval_summary, eval_argmax_prediction = sess.run(
            [accuracy, confusion_matrix, anal_test_summary_op, argmax_prediction], 
            feed_dict={x:test_data_list, y:test_labels_y}, 
            options=run_options, run_metadata=run_metadata)
        
        anal_test_summary_writer.add_run_metadata(run_metadata, "test%03d" % epoch)
        anal_test_summary_writer.add_summary(eval_summary, epoch)
        
        self.generate_metrics(anal_test_summary_writer, epoch, test_labels_y, eval_accuracy, eval_confusion_matrix, eval_argmax_prediction)
        anal_test_summary_writer.flush()

    # Perform the actual training
    def train(self):
        out_dir = self._make_out_dir()
        
        # Generate all the different path
        anal_train_summary_dir = os.path.join(out_dir, "summaries", "anal", "train")
        anal_test_summary_dir = os.path.join(out_dir, "summaries", "anal", "test")
        
        checkpoint_prefix = os.path.join(out_dir, "checkpoints", "model")
        
        encoder = Input_encoder(self.args.path_to_moz, 
                                self.args.path_to_nar, 
                                out_dir, 
                                float(self.args.train_test_ratio),
                                self.args.mozetic_corpus,
                                self.args.word_encoding)
        
        anal = Analyzer(number_classes=NUM_CLASS)
        
        if(self.args.analyzer_only):
            print("Analyzer Only")
        else:
            print("Analyzer and Discriminator")
            disc_train_summary_dir = os.path.join(out_dir, "summaries", "disc", "train")
            disc = Discriminator()

        with tf.Graph().as_default():
            sess = tf.Session()
            with sess.as_default():
                 
                input_tensor_dimension = encoder.get_tensor_legth()
     
                with tf.name_scope("Analyzer_Input"):
                    anal_x = tf.placeholder(tf.float32, input_tensor_dimension, name="anal_input_data")
                    anal_y = tf.placeholder(tf.float32,[None, NUM_CLASS], name="anal_train_labels")
                     
                anal_prediction = anal.build_graph(anal_x)
                
                if(self.args.analyzer_only):
                    with tf.name_scope("Analyzer_Optimizer"):
                        anal_loss = tf.reduce_mean(tf.nn.softmax_cross_entropy_with_logits(logits=anal_prediction, labels=anal_y, name="anal_softmax"), name="anal_loss")
                        anal_optimizer = tf.train.AdamOptimizer(learning_rate=float(self.args.learning_rate), name="anal_adam_optimizer")
                        anal_train_op = anal_optimizer.minimize(anal_loss, name="anal_adam_optimizer_minimize")
                        anal_loss_sum = tf.summary.scalar("Anal Loss", anal_loss) 
                else:
                    with tf.name_scope("Discriminator_Input"):
                        disc_x_1_tweets = tf.placeholder(tf.float32, input_tensor_dimension, name="disc_1_input_data_tweets") # Real data
                        disc_x_1_lables = tf.placeholder(tf.float32, [None, NUM_CLASS], name="disc_1_input_data_lables")      # Real data
                        
                        disc_x_2_tweets = anal_x # From Anal
                        disc_x_2_lables = tf.nn.softmax(anal_prediction, 1) # From Anal
                
                    with tf.variable_scope("Discriminator"):
                        disc_corpus_prediction, disc_corpus_intermediate  = disc.build_graph(disc_x_1_tweets, disc_x_1_lables, reuse=None) # Real data
                    with tf.variable_scope("Discriminator", reuse=True):
                        disc_from_anal_prediction, disc_from_anal_intermediate = disc.build_graph(disc_x_2_tweets, disc_x_2_lables, reuse=True) # From Anal
                     
                    trainable_vars = tf.trainable_variables()
                    
                    anal_params = [v for v in trainable_vars if v.name.startswith("Analyzer/")]
                    disc_params = [v for v in trainable_vars if v.name.startswith("Discriminator/") or v.name.startswith("rnn/")]
            
                    with tf.name_scope("Analyzer_Optimizer"):
                        
                        # From http://wiseodd.github.io/techblog/2016/09/17/gan-tensorflow/:
                        anal_loss = -tf.reduce_mean(tf.log(disc_from_anal_prediction), name="anal_loss")
                        
                        # Alternative loss from http://wiseodd.github.io/techblog/2016/09/17/gan-tensorflow/:
                        #anal_loss = tf.reduce_mean(tf.nn.sigmoid_cross_entropy_with_logits(labels=disc_from_anal_prediction, logits=tf.ones_like(disc_from_anal_prediction)))
                        
                        # FEATURE MATCHING:
                        #anal_loss = tf.sqrt(tf.reduce_sum(tf.pow(disc_corpus_intermediate - disc_from_anal_intermediate, 2)))
                    
                        anal_optimizer = tf.train.AdamOptimizer(learning_rate=float(self.args.learning_rate), name="anal_adam_optimizer")
                        #anal_optimizer = tf.train.RMSPropOptimizer(learning_rate=float(self.args.learning_rate), name="anal_adam_optimizer")
                        #anal_optimizer = tf.train.ProximalGradientDescentOptimizer(learning_rate=float(self.args.learning_rate), name="anal_adam_optimizer")
                        
                        anal_train_op = anal_optimizer.minimize(anal_loss, var_list=anal_params, name="anal_adam_optimizer_minimize")
                        anal_loss_sum = tf.summary.scalar("anal_loss", anal_loss)
                        
                    with tf.name_scope("Discriminator_Optimizer"):
                        
                        # From http://wiseodd.github.io/techblog/2016/09/17/gan-tensorflow/:
                        disc_loss = -tf.reduce_mean(self.log(disc_corpus_prediction) + self.log(1 - disc_from_anal_prediction), name="disc_loss")
                        
                        # Alternative loss from http://wiseodd.github.io/techblog/2016/09/17/gan-tensorflow/:
                        #disc_loss_1 = tf.reduce_mean(tf.nn.sigmoid_cross_entropy_with_logits(labels=disc_corpus_prediction, logits=tf.ones_like(disc_corpus_prediction)))
                        #disc_loss_2 = tf.reduce_mean(tf.nn.sigmoid_cross_entropy_with_logits(labels=disc_from_anal_prediction, logits=tf.zeros_like(disc_from_anal_prediction)))
                        #disc_loss = tf.add(disc_loss_1, disc_loss_2, name="disc_loss")
                        
                        disc_optimizer = tf.train.AdamOptimizer(learning_rate=float(self.args.learning_rate), name="disc_adam_optimizer")
                        disc_train_op = disc_optimizer.minimize(disc_loss, var_list=disc_params, name="disc_adam_optimizer_minimize")
                        disc_loss_sum = tf.summary.scalar("disc_loss", disc_loss)
                
                with tf.name_scope("Analyzer_Metrics"):
                    anal_argmax_prediction = tf.argmax(anal_prediction, 1, name="anal_argmax_prediction")
                    anal_argmax_y = tf.argmax(anal_y, 1, name="anal_argmax_y")
                     
                    anal_accuracy = tf.contrib.metrics.accuracy(anal_argmax_y, anal_argmax_prediction, name="anal_accuracy")
                    anal_accuracy_sum = tf.summary.scalar("anal_accuracy", anal_accuracy)
                     
                    anal_confusion_matrix = tf.confusion_matrix(anal_argmax_prediction, anal_argmax_y, num_classes=3, name="anal_confusion_matrix")
                    anal_confusion_matrix_sum = tf.summary.histogram("anal_confusion_matrix", anal_confusion_matrix)
                    
                if(not self.args.analyzer_only):
                    with tf.name_scope("Discriminator_Metrics"):
                        disc_from_anal_prediction_boundry = tf.summary.histogram("disc_from_anal_prediction", disc_from_anal_prediction)
                        disc_corpus_prediction_boundry = tf.summary.histogram("disc_corpus_prediction", disc_corpus_prediction)
                        
                        disc_from_anal_prediction_boundry_scal = tf.summary.scalar("disc_from_anal_prediction", tf.reduce_mean(disc_corpus_prediction))
                        disc_corpus_prediction_boundry_scal = tf.summary.scalar("disc_corpus_prediction", tf.reduce_mean(disc_from_anal_prediction))
                        
                        disc_sum_prediction_boundry_scal = tf.summary.scalar("disc_sum_prediction", tf.reduce_mean(disc_from_anal_prediction+disc_corpus_prediction))

                with tf.name_scope("Analyzer_Logging"):
                    # Setup the Tensorboard logging.
                    anal_train_summary_op = tf.summary.merge([anal_accuracy_sum, anal_loss_sum], name="anal_train_summary_op")
                    anal_train_summary_writer = tf.summary.FileWriter(anal_train_summary_dir, sess.graph)
                     
                    anal_test_summary_op = tf.summary.merge([anal_accuracy_sum, anal_confusion_matrix_sum], name="anal_test_summary_op")
                    anal_test_summary_writer = tf.summary.FileWriter(anal_test_summary_dir, sess.graph)
                
                if(not self.args.analyzer_only):
                    with tf.name_scope("Discriminator_Logging"):
                        disc_train_summary_op = tf.summary.merge([disc_sum_prediction_boundry_scal, disc_loss_sum, disc_from_anal_prediction_boundry, disc_corpus_prediction_boundry, disc_from_anal_prediction_boundry_scal, disc_corpus_prediction_boundry_scal], name="disc_train_summary_op")
                        disc_train_summary_writer = tf.summary.FileWriter(disc_train_summary_dir, sess.graph)
                        
                sess.run(tf.global_variables_initializer())
                sess.run(tf.local_variables_initializer())
                 
                saver = tf.train.Saver(tf.global_variables(), max_to_keep=int(self.args.max_savepoints_kept))
                 
                self._save_args(out_dir, "input_tensor_dimension=" + str(input_tensor_dimension[1]) + "\nword_encoding=" + str(self.args.word_encoding)+"\n")
                 
                print("Start Training")
                start_time = time.time()
                 
                for epoch in range(int(self.args.number_epochs)):
                    if(self.args.analyzer_only):
                        self._anal_train_step(encoder, sess, anal_x, anal_y, anal_loss, anal_train_op, anal_train_summary_op, anal_train_summary_writer, start_time, epoch, int(self.args.number_epochs))
                    else:
                        self._both_train_step(encoder, sess, anal_x, anal_y, anal_loss, anal_train_op, anal_train_summary_op, anal_train_summary_writer, start_time, epoch, int(self.args.number_epochs),
                                          disc_loss, disc_train_op, disc_x_1_tweets, disc_x_1_lables, disc_train_summary_op, disc_train_summary_writer)
                    if(epoch % int(self.args.test_print_every) == int(self.args.test_print_every)-1):
                        self._test_step(checkpoint_prefix, encoder, sess, anal_x, anal_y, anal_argmax_prediction, anal_accuracy, anal_confusion_matrix, anal_test_summary_op, anal_test_summary_writer, saver, epoch)

        print("Overall time taken: " + str(timedelta(seconds=(time.time() - start_time))))
         
        anal_train_summary_writer.close()
        anal_test_summary_writer.close()
         
        if(not self.args.analyzer_only):
            disc_train_summary_writer.close()
            
    # Resume an already started training
    def resume_train(self, path_to_model):
        print("Resume Training")
        
        content = self._load_args(path_to_model)
         
        if(content["word_encoding"] == "True"):
            word_encoding = True
        else:
            word_encoding = False
              
        if(content["mozetic_corpus"] == "True"):
            mozetic_corpus = True
        else:
            mozetic_corpus = False
              
        # Generate all the different path
        anal_train_summary_dir = os.path.join(path_to_model, "summaries", "anal", "train")
        anal_test_summary_dir = os.path.join(path_to_model, "summaries", "anal", "test")
        
        disc_train_summary_dir = os.path.join(path_to_model, "summaries", "disc", "train")
        
        checkpoint_prefix = os.path.join(path_to_model, "checkpoints", "model")
        
        checkpoint_file = tf.train.latest_checkpoint(os.path.join(path_to_model, "checkpoints"))
            
        encoder = Input_encoder(content["path_to_moz"], 
                                content["path_to_nar"], 
                                path_to_model, 
                                float(content["train_test_ratio"]),
                                mozetic_corpus,
                                word_encoding,
                                already_fit=True)
        
        graph = tf.Graph()
        with graph.as_default():
            sess = tf.Session()
            with sess.as_default():
                
                saver = tf.train.import_meta_graph("{}.meta".format(checkpoint_file))
                saver.restore(sess, checkpoint_file)

                #print(graph.get_operations())
                #Really helps to find the fitting strings for the get_operation_by_name
                 
                anal_x = graph.get_operation_by_name("Analyzer_Input/anal_input_data").outputs[0]
                anal_y = graph.get_operation_by_name("Analyzer_Input/anal_train_labels").outputs[0]
                
                if(not self.args.analyzer_only):
                    disc_x_1_tweets = graph.get_operation_by_name("Discriminator_Input/disc_1_input_data_tweets").outputs[0]
                    disc_x_1_lables = graph.get_operation_by_name("Discriminator_Input/disc_1_input_data_lables").outputs[0]
                
                anal_loss = graph.get_operation_by_name("Analyzer_Optimizer/anal_loss").outputs[0]
                
                if(not self.args.analyzer_only):
                    disc_loss = graph.get_operation_by_name("Discriminator_Optimizer/disc_loss").outputs[0]
 
                anal_train_op = graph.get_operation_by_name("Analyzer_Optimizer/anal_adam_optimizer_minimize/Assign").outputs[0]
                
                if(not self.args.analyzer_only):
                    disc_train_op = graph.get_operation_by_name("Discriminator_Optimizer/disc_adam_optimizer_minimize/Assign").outputs[0]
                 
                anal_train_summary_op = graph.get_operation_by_name("Analyzer_Logging/anal_train_summary_op/anal_train_summary_op").outputs[0]
                anal_train_summary_writer = tf.summary.FileWriter(anal_train_summary_dir, sess.graph)
                 
                anal_test_summary_op = graph.get_operation_by_name("Analyzer_Logging/anal_test_summary_op/anal_test_summary_op").outputs[0]
                anal_test_summary_writer = tf.summary.FileWriter(anal_test_summary_dir, sess.graph)
                
                if(not self.args.analyzer_only):
                    disc_train_summary_op = graph.get_operation_by_name("Discriminator_Logging/disc_train_summary_op/disc_train_summary_op").outputs[0]
                    disc_train_summary_writer = tf.summary.FileWriter(disc_train_summary_dir, sess.graph)
                 
                anal_argmax_prediction = graph.get_operation_by_name("Analyzer_Metrics/anal_argmax_prediction").outputs[0]
                anal_accuracy = graph.get_operation_by_name("Analyzer_Metrics/anal_accuracy/Mean").outputs[0]
                anal_confusion_matrix = graph.get_operation_by_name("Analyzer_Metrics/anal_confusion_matrix/SparseTensorDenseAdd").outputs[0]
                 
                old_epoch = int(content["number_epochs"])
                new_epoch = int(self.args.number_epochs)
                 
                args = ""
                for key, val in content.iteritems():
                    if(key == "number_epochs"):
                        args += str(key) +"=" + str((old_epoch + new_epoch)) + "\n"
                    else:
                        args += str(key) +"=" + str(val) + "\n"
 
                self._save_args(path_to_model, args, make_new_file=True)
                 
                start_time = time.time()
                for epoch in range(old_epoch ,(old_epoch + new_epoch)):
                    if(self.args.analyzer_only):
                        self._anal_train_step(encoder, sess, anal_x, anal_y, anal_loss, anal_train_op, anal_train_summary_op, anal_train_summary_writer, start_time, epoch, (old_epoch + new_epoch), old_epochs=old_epoch)
                    else:
                        self._both_train_step(encoder, sess, anal_x, anal_y, anal_loss, anal_train_op, anal_train_summary_op, anal_train_summary_writer, start_time, epoch, (old_epoch + new_epoch),
                                          disc_loss, disc_train_op, disc_x_1_tweets, disc_x_1_lables, disc_train_summary_op, disc_train_summary_writer, old_epochs=old_epoch)
                    if(epoch % int(self.args.test_print_every) == int(self.args.test_print_every)-1):
                        self._test_step(checkpoint_prefix, encoder, sess, anal_x, anal_y, anal_argmax_prediction,
                                             anal_accuracy, anal_confusion_matrix, anal_test_summary_op, anal_test_summary_writer, saver, epoch)
        
        print("Overall time taken: " + str(timedelta(seconds=(time.time() - start_time))))
        
        anal_train_summary_writer.close()
        anal_test_summary_writer.close()
        
        if(not self.args.analyzer_only):
            disc_train_summary_writer.close()
        
################################################################################
# Below is used to classify raw tweets
################################################################################

    # performs an actual classification on the given text (as list) and a path to the model
    def predict_anal(self, base_path, text):
        print("Predicting a Tweet")
        content = self._load_args(base_path)
        
        if(content["word_encoding"] == "True"):
            word_encoding = True
        else:
            word_encoding = False

        encoder = Input_encoder(use_word_encoding=word_encoding)
        
        encoded_input = encoder.encode_unclassified(text, os.path.join(base_path, "vocab"), int(content["input_tensor_dimension"]))
        
        checkpoint_file = tf.train.latest_checkpoint(os.path.join(base_path, "checkpoints"))
        
        graph = tf.Graph()
        with graph.as_default():
            sess = tf.Session()
            with sess.as_default():
                
                saver = tf.train.import_meta_graph("{}.meta".format(checkpoint_file))
                saver.restore(sess, checkpoint_file)
                
                input_x = graph.get_operation_by_name("Analyzer_Input/anal_input_data").outputs[0]
                predictions = graph.get_operation_by_name("Analyzer/anal_layer_out/anal_layer_output").outputs[0]
                
                result = tf.argmax(predictions, 1)
                
                actual_result = sess.run(result, {input_x: np.array(list(encoded_input))})
                
                print("---------")
                for i, res in enumerate(text):
                    if(actual_result.item(i) == 0):
                        print("Positive: " + res)
                    elif(actual_result.item(i) == 1):
                        print("Neutral:  " + res)
                    elif(actual_result.item(i) == 2):
                        print("Negative: " + res)
                    else:
                        raise ValueError("predict_anal ERROR: " + actual_result)
                print("---------")
