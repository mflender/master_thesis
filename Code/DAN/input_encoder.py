# coding=utf-8

# Copyright (c) 2017 Malte Flender
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of this 
# software and associated documentation files (the "Software"), to deal in the Software 
# without restriction, including without limitation the rights to use, copy, modify, merge, 
# publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons 
# to whom the Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all copies or 
# substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
# INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR 
# PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE 
# FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR 
# OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
# DEALINGS IN THE SOFTWARE.

import codecs
import numpy as np
import os
import random
import string
import sys

from nltk.tokenize import word_tokenize
from tensorflow.contrib import learn

# To cope with the German umlauts.
reload(sys)
sys.setdefaultencoding("utf8")

SEED = 4711 # To get always the same shuffle

#MAX_CHAR_PER_WORD = 78 # Tested empirical @ Mozetic
MAX_CHAR_PER_WORD = 30 # Faster with same results

emb_alphabet = 'abcdefghijklmnopqrstuvwxyz0123456789-,;.!?:\'"/\\|_@#$%^&*~`+-=<>()[]{} '
emb_alphabet_extra= 'äöüß“„”€❤😂♥😊😍😁😄😉♡☺♫😘☕😜😀😭😏😎😔😆😃😡😱😩😓😅😋😒😴😌😢'

DICT = {ch: ix for ix, ch in enumerate(emb_alphabet + emb_alphabet_extra.decode("utf-8"))}
ALPHABET_SIZE = len(emb_alphabet + emb_alphabet_extra.decode("utf-8"))

# Opens the input files, encodes them and splits them into train and test.
class Input_encoder():
    
    # Set all values.
    def __init__(self,
                 file_path_moz=None, 
                 file_path_nar=None, 
                 out_dir=None, 
                 train_test_ratio=0.1, 
                 use_mozetic_corpus=False, 
                 use_word_encoding=True,
                 already_fit=False):
        
        self.file_path_moz = file_path_moz
        self.file_path_nar = file_path_nar
        
        self.out_dir = out_dir
        
        self.train_test_ratio = train_test_ratio
        self.use_mozetic_corpus = use_mozetic_corpus
        self.use_word_encoding = use_word_encoding
        
        self.already_fit = already_fit
        
        self.max_word_per_line = None
     
        if (self.use_word_encoding):
            self.already_fit = False
            
        if(use_mozetic_corpus):
            print("Using Mozetic Corpus for the Analyzer")
        else:
            print("Using Narr Corpus for the Analyzer")

    # Opens up the file and returns the content of the file.
    def _open_file(self, file_path):
        with codecs.open(file_path, 'r', 'utf-8') as open_file:
            content = open_file.readlines()
        
        clean_content = [x.strip() for x in content] # Clean the trailing newlines.
        
        return clean_content
    
    # Reads the important parts from the file into an array.
    def _parse_narr(self):
        text = []
        label = []
        
        for line in self._open_file(self.file_path_nar):
            parts_of_the_table = line.split("\t")
            if len(parts_of_the_table) == 4:
                
                sentiment = parts_of_the_table[0]
                tweet = parts_of_the_table[-1]
                
                if(sentiment == "positive"):
                    label.append([1, 0, 0])
                    text.append(tweet)
                elif(sentiment == "neutral"):
                    label.append([0, 1, 0])
                    text.append(tweet)
                elif(sentiment == "negative"):
                    label.append([0, 0, 1])
                    text.append(tweet)
                elif(sentiment == "na" or sentiment == "sentiment"):
                    pass
                else:
                    raise ValueError("_parse_narr ERROR: " + sentiment)
                
        return [text, label]

    # Reads the important parts from the file into an array.
    def _parse_moz(self):
        text = []
        label = []
        
        for line in self._open_file(self.file_path_moz): 
            parts_of_the_table = line.split("\t")
            if len(parts_of_the_table) == 4:
                
                sentiment = parts_of_the_table[1]
                tweet = parts_of_the_table[-1]
                
                text.append(tweet)
                
# Test as to add more noise to the labels:
#                 if(sentiment == "Positive"):
#                     label.append([random.uniform(0.9, 1.0), random.uniform(0.0, 0.1), random.uniform(0.0, 0.1)])
#                 elif(sentiment == "Neutral"):
#                     label.append([random.uniform(0.0, 0.1), random.uniform(0.9, 1.0), random.uniform(0.0, 0.1)])
#                 elif(sentiment == "Negative"):
#                     label.append([random.uniform(0.0, 0.1), random.uniform(0.0, 0.1), random.uniform(0.9, 1.0)])
#                 else:
#                     raise ValueError("_parse_moz ERROR: " + sentiment)
                
                if(sentiment == "Positive"):
                    label.append([1., 0., 0.])
                elif(sentiment == "Neutral"):
                    label.append([0., 1., 0.])
                elif(sentiment == "Negative"):
                    label.append([0., 0., 1.])
                else:
                    raise ValueError("_parse_moz ERROR: " + sentiment)
                
        return [text, label]
    
    # From https://github.com/dennybritz/cnn-text-classification-tf/blob/master/train.py#L53
    # Encodes the data on world level base.
    def _generate_word_lookup_table(self, text, max_word_per_line):
        if(not self.already_fit):
            self.already_fit = True
            self.vocab_processor = learn.preprocessing.VocabularyProcessor(max_word_per_line)
            self.vocab_processor.fit(text)
            self.vocab_processor.save(os.path.join(self.out_dir, "vocab"))
            print("Vocabulary Size: {:d} Generated".format(len(self.vocab_processor.vocabulary_))) 
    
    # From https://github.com/charlesashby/CharLSTM/blob/master/lib/data_utils.py
    # Returns an array with one hot encoded chars from emb_alphabet + emb_alphabet_extra
    def _encode_on_char_level(self, text, max_word_per_line):
        for line in text:
            sent = []
            line = line.lower()
            
            encoded_line = filter(lambda x: x in (string.printable + emb_alphabet_extra), line)
            words = word_tokenize(encoded_line)
            for i in range(max_word_per_line):
                try:
                    word = words[i]
                except IndexError as _:
                    word = ""
                word_encoding = np.zeros(shape=(MAX_CHAR_PER_WORD, ALPHABET_SIZE))
                for j, char in enumerate(word.decode("utf-8")):
                    try:
                        char_encoding = DICT[char]
                        one_hot = np.zeros(ALPHABET_SIZE)
                        one_hot[char_encoding] = 1
                        word_encoding[j] = one_hot
                    except Exception as _:
                        pass
                sent.append(np.array(word_encoding))
            yield np.array(sent)
    
    # Returns the length of the first tensor within the analyzer net.
    def get_tensor_legth(self):
        if (self.use_mozetic_corpus):
            text, _ = self._parse_moz()
        else:
            text, _ = self._parse_narr()
        
        max_word_per_line = max([len(word_tokenize(x)) for x in text])
        
        if (self.use_word_encoding):
            print("Using Word encoding")
            return [None, max_word_per_line]
        else:
            print("Using Char encoding")
            return [None, max_word_per_line, MAX_CHAR_PER_WORD, ALPHABET_SIZE]

    # Wrapper to get the encoded data from the corpus.
    def get_data(self, test_data=False, use_mozetic_corpus=None, max_word_per_line=None, encode_both=False):
        if (use_mozetic_corpus == None):
            use_mozetic_corpus = self.use_mozetic_corpus
        
        if (use_mozetic_corpus):
            text, labels = self._parse_moz()
        else:
            text, labels = self._parse_narr()
        
        if (max_word_per_line == None):
            if(self.max_word_per_line == None):
                max_word_per_line = max([len(word_tokenize(x)) for x in text])
                self.max_word_per_line = max_word_per_line
            else:
                max_word_per_line = self.max_word_per_line
            
        length_test = int(len(labels) * self.train_test_ratio)
        
        if (self.use_word_encoding):
            if(encode_both):
                if (not use_mozetic_corpus):
                    text1, _ = self._parse_moz()
                else:
                    text1, _ = self._parse_narr()
                self._generate_word_lookup_table(text+text1, max_word_per_line)
            else:
                self._generate_word_lookup_table(text, max_word_per_line)
        
        #random.seed(SEED)

        if (test_data):
            test_text_x = text[:length_test]
            test_labels_y = labels[:length_test]
             
            test_zipped = zip(test_text_x, test_labels_y)
            random.shuffle(test_zipped)
            test_text_x, test_labels_y = zip(*test_zipped)
            
            if (self.use_word_encoding):
                test_text_x_generator = self.vocab_processor.transform(test_text_x)
            else:
                test_text_x_generator = self._encode_on_char_level(test_text_x, max_word_per_line) 
            
            return [test_text_x_generator, test_labels_y, max_word_per_line]
        else:
            train_text_x = text[length_test:]
            train_labels_y = labels[length_test:]
            
            train_zipped = zip(train_text_x, train_labels_y)
            random.shuffle(train_zipped)
            train_text_x, train_labels_y = zip(*train_zipped)
            
            if (self.use_word_encoding):
                train_text_x_generator = self.vocab_processor.transform(train_text_x)
            else:
                train_text_x_generator = self._encode_on_char_level(train_text_x, max_word_per_line) 
            
            return [train_text_x_generator, train_labels_y, max_word_per_line]

################################################################################
# Below is used to encode a few lines of text for actual classification purpose
################################################################################
    
    # Encodes the data with a given vocab
    def _encode_unclassified_on_word_level(self, text, vocab_path):
        vocab_processor = learn.preprocessing.VocabularyProcessor.restore(vocab_path)
        print("Vocabulary Size: {:d} Loaded".format(len(vocab_processor.vocabulary_)))
        return vocab_processor.transform(text)
    
    # Encodes unclassified data
    def encode_unclassified(self, text, vocab_path, max_word_per_line):
        if (self.use_word_encoding):
            print("Using Word encoding")
            encoded_data_generator = self._encode_unclassified_on_word_level(text, vocab_path)
        else:
            print("Using Char encoding")
            encoded_data_generator = self._encode_on_char_level(text, max_word_per_line)
            
        return encoded_data_generator
