# coding=utf-8

# Copyright (c) 2017 Malte Flender and Christian Carsten Sander
#
# Permission is hereby granted, free of charge, to any person obtaining a copy of this 
# software and associated documentation files (the "Software"), to deal in the Software 
# without restriction, including without limitation the rights to use, copy, modify, merge, 
# publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons 
# to whom the Software is furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all copies or 
# substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, 
# INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR 
# PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE 
# FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR 
# OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER 
# DEALINGS IN THE SOFTWARE.

# The Python-Analyzer for the FH Bielefeld-Newsboard.

import argparse
import datetime
import httplib2
import json
import logging
import nltk
import pytz
import sys

from urlparse import urlparse

# The accepted file type.
# The token for the client authentication.
headers = {
    "Accept": "application/json",
    "Content-Type": "application/json",
    "token": "znryih9xv2hf4x2zchnq"
}

# The URL of the base application.
url = "http://thesisvm2:8080/NewsBoard"

# The URL of the REST-API of the application.
path = "/WebService/analyzer/news/"

# Only for the stub, switches after every sentence so one can easy see
# where a sentence starts/ends.
goodSentence = True

# The logger used to log more detailed behavior.
Logger = None

# Opens a communication path to the FH-Newsboard and parses the resulting
# JSON file into a list of news objects.
def getJson():
    target = urlparse(url + path)
    response, content = httplib2.Http().request(target.geturl(), "GET", "", headers)

    if(int(response["status"]) != 200):
        errorMessage = "Bad Connection: " + str(response)
        Logger.critical(errorMessage)
        raise IOError(errorMessage)
    
    if(content == "" or not bool(content)):
        errorMessage = "No content"
        Logger.critical(errorMessage)
        raise ValueError(errorMessage)
    
    news = json.loads(content)
    Logger.debug("Unanalyzed news: " + str(news))
    
    return news

# Performs the sentiment analysis on the unanalyzed news.
def analyze(content):
    if(content == "" or not bool(content)):
        errorMessage = "No content"
        Logger.critical(errorMessage)
        raise ValueError(errorMessage)
    
    analResArray = []

    tokenizer = nltk.data.load("tokenizers/punkt/german.pickle")
    
    for start, end in tokenizer.span_tokenize(content):
        sentenceResult = {"charStart":start, "charEnd":end}
        sentenceResult = analyzeSentence(sentenceResult, content[start:end])
        analResArray.append(sentenceResult)
        
    date = datetime.datetime.now(pytz.timezone("Europe/Berlin")).replace(microsecond=0).isoformat()
    
    #TODO perform actual sentiment analysis
    analRes = {"value":100, "date":date, "sentenceResults":analResArray}
    
    Logger.debug("Analysis result: " + str(analRes))
    return analRes

# Performs the sentiment analysis at sentence level.
def analyzeSentence(sentenceResult, sentence):
    if(not bool(sentenceResult)):
        errorMessage = "No sentenceResult"
        Logger.critical(errorMessage)
        raise ValueError(errorMessage)
    if(sentence == "" or not bool(sentence)):
        errorMessage = "No sentence"
        Logger.critical(errorMessage)
        raise ValueError(errorMessage)
    
    #TODO perform actual sentiment analysis on sentence
    global goodSentence
    if (goodSentence):
        sentenceResult["value"] = 100
        goodSentence = False
    else:
        sentenceResult["value"] = -100
        goodSentence = True
    
    Logger.debug("Sentence analysis result: " + str(sentenceResult))
    return sentenceResult

#Converts the analyzer results to JSON and POST it to the FH-Newsboard.
def postJson(analRes, newsId):
    if(not bool(analRes)):
        errorMessage = "No analRes"
        Logger.critical(errorMessage)
        raise ValueError(errorMessage)
    if(newsId == "" or not bool(newsId)):
        errorMessage = "No newsId"
        Logger.critical(errorMessage)
        raise ValueError(errorMessage)
    
    target = urlparse(url + path + newsId)
    response, content = httplib2.Http().request(target.geturl(), "POST", json.dumps(analRes), headers)
    if(int(response["status"]) != 200):
        errorMessage = "Bad Connection: " + str(response) + str(content)
        Logger.critical(errorMessage)
        raise IOError(errorMessage)

# Starts the program and handles the command line arguments.
def main():
    parser = argparse.ArgumentParser(description="FH-Newsboard Analyzer Version 0.1 Copyright (c) 2017 Malte Flender")
    parser.add_argument("-v" , "--verbose", help="enables logging", default=False, action="store_true", dest="verbose")
    parser.add_argument("-s", "--single", help="analyze only one batch of news posts", default=False, action="store_true",dest="single")
    args = parser.parse_args()
    
    if(args.verbose):
        print( "Using verbose mode")
        logging.basicConfig(level=logging.DEBUG)
    else:
        logging.basicConfig(level=logging.WARN)
    
    global Logger
    Logger = logging.getLogger("Main")
    
    if (args.single):
        print("Using single mode")
        print("Analyzing")
        for post in getJson():
            postJson(analyze(post["content"]), post["id"])
        print("Done")
    else:
        sys.stdout.write("Analyzing")
        done = False
        while(not done):
            sys.stdout.write(" .")
            posts = getJson()
            if(posts):
                for post in posts:
                    postJson(analyze(post["content"]), post["id"])
            else:
                done = True
        print("\nDone")

if __name__ == "__main__":
    main()