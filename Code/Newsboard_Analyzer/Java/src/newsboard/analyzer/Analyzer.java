package newsboard.analyzer;

import java.text.BreakIterator;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.logging.Level;
import java.util.logging.Logger;

import newsboard.Main;
import newsboard.model.AnalyzeResult;
import newsboard.model.News;
import newsboard.model.SentenceResult;

/*
 *      Copyright (c) 2017 Malte Flender
 *
 *      Permission is hereby granted, free of charge, to any person obtaining a
 *      copy of this software and associated documentation files (the
 *      "Software"), to deal in the Software without restriction, including
 *      without limitation the rights to use, copy, modify, merge, publish,
 *      distribute, sublicense, and/or sell copies of the Software, and to
 *      permit persons to whom the Software is furnished to do so, subject to
 *      the following conditions:
 *
 *      The above copyright notice and this permission notice shall be included
 *      in all copies or substantial portions of the Software.
 *
 *      HE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 *      OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 *      MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 *      IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
 *      CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 *      TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 *      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

/**
 * The actual analyzer. It's only a stub, to show the functionality.
 *
 * @author Malte Flender
 * @version 0.1
 */
public class Analyzer {

    /**
     * The logger, used to log more detailed behavior.
     */
    private static final Logger LOG = Logger.getLogger(Main.class.getName());

    /**
     * Only for the stub, switches after every sentence so one can easy see
     * where a sentence starts/ends.
     */
    private boolean goodSentence = true;

    /**
     * Since it is a stub there is nothing to construct.
     */
    public Analyzer() {

    }

    /**
     * Performs the sentiment analysis on the unanalyzed news.
     *
     * @param unanalyzedNews
     *            the news and Twitter posts, that need to be analyzed
     * @return a List of the analyzed posts, can be null
     */
    public List<AnalyzeResult> analyze(final List<News> unanalyzedNews) {
        if (unanalyzedNews == null || unanalyzedNews.isEmpty()) {
            LOG.log(Level.SEVERE,
                    "Nothing to analyze unanalyzedNews has no content.");
            return null;
        }

        List<AnalyzeResult> results = new ArrayList<AnalyzeResult>();

        for (News news : unanalyzedNews) {
            AnalyzeResult analRes = new AnalyzeResult();

            analRes.setId(news.getId());

            analRes.setValue(100);
            // TODO perform actual sentiment analysis

            analRes.setDate(new Date());

            List<SentenceResult> sentences = splitIntoSentence(
                    news.getContent());
            analRes.setSentenceResults(sentences);
            results.add(analRes);
        }
        return results;
    }

    /**
     * Splits a given string containing a longer text into an ArrayList of
     * sentences and performs the sentiment analysis on every sentence.
     *
     * @param source
     *            the string with the text
     * @return the resulting ArrayList with the sentences, can be null
     */
    private List<SentenceResult> splitIntoSentence(final String source) {
        if (source == null || source == "") {
            LOG.log(Level.SEVERE, "Nothing to analyze: source is empty");
            return null;
        }

        List<SentenceResult> sentences = new ArrayList<SentenceResult>();

        BreakIterator iterator = BreakIterator
                .getSentenceInstance(Locale.GERMANY);
        iterator.setText(source);

        int start = iterator.first();
        for (int end = iterator
                .next(); end != BreakIterator.DONE; start = end, end = iterator
                        .next()) {

            SentenceResult sentRes = new SentenceResult();
            sentRes.setCharStart(start);

            /**
             * Without this the FH-Newsboard either skips the last char of the
             * content or displays all sentences wrong.
             */
            if (source.length() == end) {
                sentRes.setCharEnd(end);
                sentRes.setValue(analyzeSentence(source.substring(start, end)));
            } else {
                sentRes.setCharEnd(end - 1);
                sentRes.setValue(
                        analyzeSentence(source.substring(start, end - 1)));
            }
            sentences.add(sentRes);
        }
        return sentences;
    }

    /**
     * Performs the actual sentiment analysis at sentence level.
     *
     * @param sentence
     *            the sentence to perform the sentiment analysis on
     * @return the score of the analysis (-100 to +100), can be -101 in case of
     *         bad input
     */
    private int analyzeSentence(final String sentence) {
        if (sentence == null || sentence == "") {
            LOG.log(Level.SEVERE, "Nothing to analyze: sentence is empty");
            return -101;
        }

        // TODO perform actual sentiment analysis
        if (goodSentence) {
            goodSentence = false;
            return 100;
        } else {
            goodSentence = true;
            return -100;
        }
    }
}
