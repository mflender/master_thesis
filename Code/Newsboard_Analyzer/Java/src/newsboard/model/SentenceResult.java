package newsboard.model;

/*
 *      Copyright (c) 2017 Malte Flender
 *
 *      Permission is hereby granted, free of charge, to any person obtaining a
 *      copy of this software and associated documentation files (the
 *      "Software"), to deal in the Software without restriction, including
 *      without limitation the rights to use, copy, modify, merge, publish,
 *      distribute, sublicense, and/or sell copies of the Software, and to
 *      permit persons to whom the Software is furnished to do so, subject to
 *      the following conditions:
 *
 *      The above copyright notice and this permission notice shall be included
 *      in all copies or substantial portions of the Software.
 *
 *      HE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 *      OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 *      MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 *      IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
 *      CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 *      TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 *      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

/**
 * POJO to carry all the data, that comes with the JSON-file. This one
 * represents a sentence in the analyzed news and Twitter posts.
 *
 * @author Malte Flender
 * @version 0.1
 */
public class SentenceResult {

    /**
     * The position of the first character of the Sentence within the content.
     */
    private int charStart;

    /**
     * The position of the last character of the Sentence within the content.
     */
    private int charEnd;

    /**
     * The analyzer value of the current Sentence. Range from -100 (max
     * negative) to +100 (max positive).
     */
    private int value;

    /**
     * @return the charStart
     */
    public int getCharStart() {
        return charStart;
    }

    /**
     * @param setCharStart
     *            the charStart to set
     */
    public void setCharStart(final int setCharStart) {
        this.charStart = setCharStart;
    }

    /**
     * @return the charEnd
     */
    public int getCharEnd() {
        return charEnd;
    }

    /**
     * @param setCharEnd
     *            the charEnd to set
     */
    public void setCharEnd(final int setCharEnd) {
        this.charEnd = setCharEnd;
    }

    /**
     * @return the value
     */
    public int getValue() {
        return value;
    }

    /**
     * @param setValue
     *            the value to set
     */
    public void setValue(final int setValue) {
        this.value = setValue;
    }

    /*
     * (non-Javadoc)
     *
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "SentenceResults: charStart=" + charStart + ", charEnd="
                + charEnd + ", value=" + value;
    }

}
