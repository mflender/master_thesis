package newsboard.model;

import java.net.URL;
import java.util.Date;

/*
 *      Copyright (c) 2017 Malte Flender
 *
 *      Permission is hereby granted, free of charge, to any person obtaining a
 *      copy of this software and associated documentation files (the
 *      "Software"), to deal in the Software without restriction, including
 *      without limitation the rights to use, copy, modify, merge, publish,
 *      distribute, sublicense, and/or sell copies of the Software, and to
 *      permit persons to whom the Software is furnished to do so, subject to
 *      the following conditions:
 *
 *      The above copyright notice and this permission notice shall be included
 *      in all copies or substantial portions of the Software.
 *
 *      HE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 *      OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 *      MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 *      IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
 *      CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 *      TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 *      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

/**
 * POJO to carry all the data, that comes with the JSON-file. This one
 * represents the unanalyzed news and Twitter posts.
 *
 * @author Malte Flender
 * @version 0.1
 */
public class News {

    /**
     * The id of the news entry.
     */
    private String id;

    /**
     * The crawler that found the news entry.
     */
    private Crawler crawler;

    /**
     * The title of the news entry.
     */
    private String title;

    /**
     * The image that came with the news entry.
     */
    private URL image;

    /**
     * The content of the news entry.
     */
    private String content;

    /**
     * The source of the news entry.
     */
    private String source;

    /**
     * The URL where the news entry came from.
     */
    private URL url;

    /**
     * The date and time, where the crawler found the news entry.
     */
    private Date date;

    /**
     * @return the id
     */
    public String getId() {
        return id;
    }

    /**
     * @param setId
     *            the id to set
     */
    public void setId(final String setId) {
        this.id = setId;
    }

    /**
     * @return the crawler
     */
    public Crawler getCrawler() {
        return crawler;
    }

    /**
     * @param setCrawler
     *            the crawler to set
     */
    public void setCrawler(final Crawler setCrawler) {
        this.crawler = setCrawler;
    }

    /**
     * @return the title
     */
    public String getTitle() {
        return title;
    }

    /**
     * @param setTitle
     *            the title to set
     */
    public void setTitle(final String setTitle) {
        this.title = setTitle;
    }

    /**
     * @return the image
     */
    public URL getImage() {
        return image;
    }

    /**
     * @param setImage
     *            the image to set
     */
    public void setImage(final URL setImage) {
        this.image = setImage;
    }

    /**
     * @return the content
     */
    public String getContent() {
        return content;
    }

    /**
     * @param setContent
     *            the content to set
     */
    public void setContent(final String setContent) {
        this.content = setContent;
    }

    /**
     * @return the source
     */
    public String getSource() {
        return source;
    }

    /**
     * @param setSource
     *            the source to set
     */
    public void setSource(final String setSource) {
        this.source = setSource;
    }

    /**
     * @return the url
     */
    public URL getUrl() {
        return url;
    }

    /**
     * @param setUrl
     *            the url to set
     */
    public void setUrl(final URL setUrl) {
        this.url = setUrl;
    }

    /**
     * @return the date
     */
    public Date getDate() {
        return date;
    }

    /**
     * @param setDate
     *            the date to set
     */
    public void setDate(final Date setDate) {
        this.date = setDate;
    }

    /*
     * (non-Javadoc)
     *
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "News: \n" + "id=" + id + "\n" + "crawler=" + crawler + "\n"
                + "title=" + title + "\n" + "image=" + image + "\n" + "content="
                + content + "\n" + "source=" + source + "\n" + "url=" + url
                + "\n" + "date=" + date + "\n"
                + "-------------------------------------------\n";
    }

}
