package newsboard.model;

import java.util.Date;
import java.util.List;

/*
 *      Copyright (c) 2017 Malte Flender
 *
 *      Permission is hereby granted, free of charge, to any person obtaining a
 *      copy of this software and associated documentation files (the
 *      "Software"), to deal in the Software without restriction, including
 *      without limitation the rights to use, copy, modify, merge, publish,
 *      distribute, sublicense, and/or sell copies of the Software, and to
 *      permit persons to whom the Software is furnished to do so, subject to
 *      the following conditions:
 *
 *      The above copyright notice and this permission notice shall be included
 *      in all copies or substantial portions of the Software.
 *
 *      HE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 *      OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 *      MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 *      IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
 *      CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 *      TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 *      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

/**
 * POJO to carry all the data, that comes with the JSON-file. This one
 * represents the analyzed news and Twitter posts.
 *
 * @author Malte Flender
 * @version 0.1
 */
public class AnalyzeResult {

    /**
     * The analyzer value of the whole content. Range from -100 (max negative)
     * to +100 (max positive).
     */
    private int value;

    /**
     * The date and time of the analysis.
     */
    private Date date;

    /**
     * A list with all the analysis values for every sentence.
     */
    private List<SentenceResult> sentenceResults;

    /**
     * Only used internal to keep track of the IDs.
     */
    private String id;

    /**
     * @return the value
     */
    public int getValue() {
        return value;
    }

    /**
     * @param setValue
     *            the value to set
     */
    public void setValue(final int setValue) {
        this.value = setValue;
    }

    /**
     * @return the date
     */
    public Date getDate() {
        return date;
    }

    /**
     * @param setDate
     *            the date to set
     */
    public void setDate(final Date setDate) {
        this.date = setDate;
    }

    /**
     * @return the sentenceResults
     */
    public List<SentenceResult> getSentenceResults() {
        return sentenceResults;
    }

    /**
     * @param setSentenceResults
     *            the sentenceResults to set
     */
    public void setSentenceResults(
            final List<SentenceResult> setSentenceResults) {
        this.sentenceResults = setSentenceResults;
    }

    /**
     * @return the id
     */
    public String getId() {
        return id;
    }

    /**
     * @param setId
     *            the id to set
     */
    public void setId(final String setId) {
        this.id = setId;
    }

    /*
     * (non-Javadoc)
     *
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "AnalyzeResult:\n" + "value=" + value + "\n" + "date=" + date
                + "\n" + "id=" + id + "\n" + "sentenceResults="
                + sentenceResults + "\n"
                + "----------------------------------------\n";
    }

}
