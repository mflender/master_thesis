package newsboard;

import java.util.List;
import java.util.logging.ConsoleHandler;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;

import newsboard.analyzer.Analyzer;
import newsboard.model.News;
import newsboard.rest.RestApiCommunication;

/*
 *      Copyright (c) 2017 Malte Flender
 *
 *      Permission is hereby granted, free of charge, to any person obtaining a
 *      copy of this software and associated documentation files (the
 *      "Software"), to deal in the Software without restriction, including
 *      without limitation the rights to use, copy, modify, merge, publish,
 *      distribute, sublicense, and/or sell copies of the Software, and to
 *      permit persons to whom the Software is furnished to do so, subject to
 *      the following conditions:
 *
 *      The above copyright notice and this permission notice shall be included
 *      in all copies or substantial portions of the Software.
 *
 *      HE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 *      OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 *      MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 *      IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
 *      CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 *      TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 *      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

/**
 * Main class of the Java-Analyzer for the FH Bielefeld-Newsboard.
 *
 * @author Malte Flender
 * @version 0.1
 */
public final class Main {

    /**
     * The token for the client authentication.
     */
    private static final String TOKEN = "znryih9xv2hf4x2zchnq";

    /**
     * The URL of the base application.
     */
    private static final String BASE_URL = "http://thesisvm2:8080/NewsBoard";

    /**
     * The logger used to log more detailed behavior.
     */
    private static final Logger LOG = Logger.getLogger(Main.class.getName());

    /**
     * Dead constructor.
     */
    private Main() {

    }

    /**
     * Starts the program and handles the command line arguments.
     *
     * @param args
     *            not used
     */
    public static void main(final String[] args) {
        ConsoleHandler handler = new ConsoleHandler();

        handler.setFormatter(new SimpleFormatter());
        handler.setLevel(Level.INFO);

        LOG.setLevel(Level.INFO);
        LOG.setUseParentHandlers(false);
        LOG.addHandler(handler);

        boolean singleBatch = false;

        for (int argIndex = 0; argIndex < args.length; argIndex++) {
            if (args[argIndex].equalsIgnoreCase("--verbose")) {
                handler.setLevel(Level.ALL);
                LOG.setLevel(Level.ALL);
                System.out.println("Using verbose mode");
            } else if (args[argIndex].equalsIgnoreCase("--single")) {
                singleBatch = true;
                System.out.println("Using single mode");
            } else if (args[argIndex].equalsIgnoreCase("--help")) {
                printHelp();
            } else {
                System.err.println("Unrecognized Option,"
                        + " try \"--help\" for more information");
                return;
            }
        }

        Analyzer anal = new Analyzer();
        RestApiCommunication api = new RestApiCommunication(TOKEN, BASE_URL);

        if (singleBatch) {
            System.out.println("Analyzing");
            api.java2Json(anal.analyze(api.getJSON()));
            System.out.println("Done");
        } else {
            boolean done = false;
            System.out.print("Analyzing");
            while (!done) {
                List<News> unanalyzedNews = api.getJSON();
                if (!unanalyzedNews.isEmpty()) {
                    System.out.print(" .");
                    api.java2Json(anal.analyze(unanalyzedNews));
                } else {
                    System.out.println("\nDone");
                    done = true;
                }
            }
        }
    }

    /**
     * Prints the help message.
     */
    private static void printHelp() {
        System.out.println("FH-Newsboard Analyzer Version"
                + " 0.1 Copyright (c) 2017 Malte Flender");
        System.out.println("Usage:");
        System.out.println("--verbose\tenables logging");
        System.out.println("--single\tanalyze only one batch of news posts");
        System.out.println("--help\t\tprint this message");
    }
}
