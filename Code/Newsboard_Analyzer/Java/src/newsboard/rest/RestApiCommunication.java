package newsboard.rest;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonArrayBuilder;
import javax.json.JsonObject;
import javax.json.JsonObjectBuilder;
import javax.json.JsonReader;
import javax.json.JsonValue;

import newsboard.Main;
import newsboard.model.AnalyzeResult;
import newsboard.model.Crawler;
import newsboard.model.News;
import newsboard.model.SentenceResult;

/*
 *      Copyright (c) 2017 Malte Flender
 *
 *      Permission is hereby granted, free of charge, to any person obtaining a
 *      copy of this software and associated documentation files (the
 *      "Software"), to deal in the Software without restriction, including
 *      without limitation the rights to use, copy, modify, merge, publish,
 *      distribute, sublicense, and/or sell copies of the Software, and to
 *      permit persons to whom the Software is furnished to do so, subject to
 *      the following conditions:
 *
 *      The above copyright notice and this permission notice shall be included
 *      in all copies or substantial portions of the Software.
 *
 *      HE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 *      OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 *      MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 *      IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
 *      CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 *      TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 *      SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

/**
 * This class handles the communication with the FH-Newsboard REST-API and
 * transforms the JSON string into a java usable data structure and back to
 * JSON.
 *
 * @author Malte Flender
 * @version 0.1
 */
public class RestApiCommunication {

    /**
     * The accepted file type.
     */
    private final String accept = "application/json";

    /**
     * The URL of the REST-API of the application.
     */
    private final String restURL = "/WebService/analyzer/news/";

    /**
     * To parse the JSON date into a java date object and back to JSON.
     */
    private final DateFormat dateFormat = new SimpleDateFormat(
            "yyyy-MM-dd'T'HH:mm:ssXXX", Locale.GERMAN);

    /**
     * The token for the client authentication.
     */
    private String token;

    /**
     * The URL of the base application.
     */
    private String baseURL;

    /**
     * The logger, used to log more detailed behavior.
     */
    private static final Logger LOG = Logger.getLogger(Main.class.getName());

    /**
     * Constructor to generate a new API-communication.
     *
     * @param setToken
     *            the security token to access the FH-Newsboard
     * @param setBaseURL
     *            the base URL to the FH-Newsboard e.g.
     *            http://thesisvm2:8080/NewsBoard
     */
    public RestApiCommunication(final String setToken,
            final String setBaseURL) {
        this.token = setToken;
        this.baseURL = setBaseURL;
    }

    /**
     * Opens a communication path to the FH-Newsboard and parses the resulting
     * JSON file into a list of news objects.
     *
     * @return the list of the News objects, can be null
     */
    public List<News> getJSON() {
        List<News> unanalyzedNews = null;
        try {
            URL analyzerURL = new URL(baseURL + restURL);
            HttpURLConnection connection = (HttpURLConnection) analyzerURL
                    .openConnection();

            connection.setRequestMethod("GET");
            connection.setRequestProperty("accept", accept);
            connection.setRequestProperty("token", token);

            if (connection.getResponseCode() != HttpURLConnection.HTTP_OK) {
                String outputError = "";

                BufferedReader br = new BufferedReader(
                        new InputStreamReader((connection.getErrorStream())));

                for (String line; (line = br
                        .readLine()) != null; outputError += line)
                    ;

                String errorMessage = "Failed: " + connection.getResponseCode()
                        + " " + connection.getResponseMessage() + " "
                        + outputError;

                LOG.log(Level.SEVERE, errorMessage);
                throw new RuntimeException(errorMessage);
            }
            unanalyzedNews = json2Java(connection);
        } catch (IOException e) {
            LOG.log(Level.SEVERE,
                    "Error while opeing a connection"
                            + " to the FH-Newsboard or parsing "
                            + "the JSON file into java objects.");
            e.printStackTrace();
        }

        LOG.log(Level.CONFIG, "Java-Sturcture of the news:\n"
                + unanalyzedNews.toString() + "\n");
        return unanalyzedNews;
    }

    /**
     * Receives a JSON file from the FH-Newsboard and parses it into a list of
     * News objects.
     *
     * @param connection
     *            the connection to the FH-Newsboard
     * @return the list of the News objects, can be null
     * @throws IOException
     *             if the URLs can't be parsed or the connection can't be opened
     */
    private List<News> json2Java(final HttpURLConnection connection)
            throws IOException {
        if (connection == null) {
            LOG.log(Level.SEVERE, "Bad http connection.");
            return null;
        }

        JsonReader jsonReader = Json.createReader(connection.getInputStream());
        JsonArray jsonArray = jsonReader.readArray();

        List<News> unanalyzedNews = new ArrayList<News>();

        LOG.log(Level.CONFIG,
                "News from FH-Newsboard:\n" + jsonArray.toString() + "\n");

        if (!jsonArray.isEmpty()) {
            for (JsonValue value : jsonArray) {
                News news = new News();
                JsonObject jsonObject = (JsonObject) value;

                if (jsonObject.containsKey("id") && !jsonObject.isNull("id")) {
                    news.setId(jsonObject.getString("id"));
                }
                if (jsonObject.containsKey("crawler")
                        && !jsonObject.isNull("crawler")) {
                    // This is NOT tested since there is no crawler field in the
                    // actual JSON file. This is only there to be compatible
                    // with the API-description.
                    // --------------------------------------------------
                    JsonObject innerJsonObject = jsonObject
                            .getJsonObject("crawler");
                    Crawler crawler = new Crawler();
                    if (innerJsonObject.containsKey("id")
                            && !innerJsonObject.isNull("id")) {
                        crawler.setId(innerJsonObject.getInt("id"));
                    }
                    if (innerJsonObject.containsKey("name")
                            && !innerJsonObject.isNull("name")) {
                        crawler.setName(innerJsonObject.getString("name"));
                    }
                    news.setCrawler(crawler);
                    // --------------------------------------------------
                }
                if (jsonObject.containsKey("title")
                        && !jsonObject.isNull("title")) {
                    news.setTitle(jsonObject.getString("title"));
                }
                if (jsonObject.containsKey("image")
                        && !jsonObject.isNull("image")) {
                    URL imageURL = new URL(jsonObject.getString("image"));
                    news.setImage(imageURL);
                }
                if (jsonObject.containsKey("content")
                        && !jsonObject.isNull("content")) {
                    news.setContent(jsonObject.getString("content"));
                }
                if (jsonObject.containsKey("source")
                        && !jsonObject.isNull("source")) {
                    news.setSource(jsonObject.getString("source"));
                }
                if (jsonObject.containsKey("url")
                        && !jsonObject.isNull("url")) {
                    URL urlURL = new URL(jsonObject.getString("url"));
                    news.setImage(urlURL);
                }
                if (jsonObject.containsKey("date")
                        && !jsonObject.isNull("date")) {
                    try {
                        news.setDate(
                                dateFormat.parse(jsonObject.getString("date")));
                    } catch (ParseException e) {
                        LOG.log(Level.SEVERE, "Error while parsing the "
                                + "given Date/Time into java object.");
                        e.printStackTrace();
                    }
                }
                unanalyzedNews.add(news);
            }
        }
        return unanalyzedNews;
    }

    /**
     * Converts the analyzer results to JSON and POST it to the FH-Newsboard.
     *
     * @param analyzerResults
     *            the results of the analyzer
     */
    public void java2Json(final List<AnalyzeResult> analyzerResults) {
        if (analyzerResults == null || analyzerResults.isEmpty()) {
            LOG.log(Level.SEVERE, "Nothing to POST: analyzerResults is empty");
            return;
        }

        LOG.log(Level.CONFIG, "Java-Sturcture of the analysis result:\n"
                + analyzerResults.toString() + "\n");

        JsonObjectBuilder analResBuilder;
        JsonObjectBuilder sentResBuilder;
        JsonArrayBuilder sentResArrayBuilder;

        for (AnalyzeResult analRes : analyzerResults) {
            analResBuilder = Json.createObjectBuilder();
            sentResArrayBuilder = Json.createArrayBuilder();

            for (SentenceResult sentRes : analRes.getSentenceResults()) {
                sentResBuilder = Json.createObjectBuilder();

                sentResBuilder.add("charStart", sentRes.getCharStart());
                sentResBuilder.add("charEnd", sentRes.getCharEnd());
                sentResBuilder.add("value", sentRes.getValue());

                sentResArrayBuilder.add(sentResBuilder);
            }

            analResBuilder.add("value", analRes.getValue());
            analResBuilder.add("date", dateFormat.format(analRes.getDate()));
            analResBuilder.add("sentenceResults", sentResArrayBuilder);

            String jsonContent = analResBuilder.build().toString();
            LOG.log(Level.CONFIG,
                    "Analysis result to FH-Newsboard:\n" + jsonContent + "\n");

            postJSON(analRes.getId(), jsonContent);
        }
    }

    /**
     * POSTS the results of the analyzer to the FH-Newsboard.
     *
     * @param postID
     *            the id of the analyzed news entry
     * @param jsonContent
     *            the result of the analyzer as JSON string
     */
    private void postJSON(final String postID, final String jsonContent) {
        if (postID == null || postID == "") {
            String errorMessage = "Failed: postID is empty";
            LOG.log(Level.SEVERE, errorMessage);
            throw new RuntimeException(errorMessage);
        }
        if (jsonContent == null || jsonContent == "") {
            String errorMessage = "Failed: jsonContent is empty";
            LOG.log(Level.SEVERE, errorMessage);
            throw new RuntimeException(errorMessage);
        }
        try {
            URL analyzerURL = new URL(baseURL + restURL + postID);
            HttpURLConnection connection = (HttpURLConnection) analyzerURL
                    .openConnection();

            connection.setDoOutput(true);
            connection.setRequestMethod("POST");
            connection.setRequestProperty("Content-Type", accept);
            connection.setRequestProperty("token", token);

            OutputStream outStream = connection.getOutputStream();
            outStream.write(jsonContent.getBytes());
            outStream.flush();

            if (connection.getResponseCode() != HttpURLConnection.HTTP_OK) {
                String outputError = "";

                BufferedReader br = new BufferedReader(
                        new InputStreamReader((connection.getErrorStream())));
                for (String line; (line = br
                        .readLine()) != null; outputError += line)
                    ;

                String errorMessage = "Failed: HTTP error code : "
                        + connection.getResponseCode() + " "
                        + connection.getResponseMessage() + " " + outputError;

                LOG.log(Level.SEVERE, errorMessage);
                throw new RuntimeException(errorMessage);
            }

        } catch (IOException e) {
            LOG.log(Level.SEVERE, "Error while posting the analyser"
                    + " results to the FH-Newsboard.");
            e.printStackTrace();
        }
    }
}
